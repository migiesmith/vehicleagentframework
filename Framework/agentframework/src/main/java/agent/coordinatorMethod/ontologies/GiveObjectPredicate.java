package agent.coordinatorMethod.ontologies;

import jade.content.Predicate;

/**
 * This is a simple class used for sending objects
 * such as the VisitData and Depot through messages
 * using JADE Ontologies. Using this, objects can
 * be sent in SL format so that they can be encoded
 * and decoded on either end of the communication.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class GiveObjectPredicate implements Predicate{

	private Object data;
	
	public GiveObjectPredicate(){
		setData(null);
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
