package agent.coordinatorMethod.ontologies;

import agent.auctionSolution.dataObjects.Route;
import agent.coordinatorMethod.Depot;
import agent.coordinatorMethod.JourneyData;
import agent.coordinatorMethod.SavingsNode;
import agent.coordinatorMethod.VisitData;
import jade.content.onto.BasicOntology;
import jade.content.onto.BeanOntology;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;

/**
 * This Ontology is used as the language for object
 * encoding and decoding for messages sent that need
 * to contain an object that implements the Concept
 * interface.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class GiveOntology extends BeanOntology {
	
	// Name of the ontology
	public static final String ONTOLOGY_NAME = "Give-Ontology";
		
	
	// The singleton instance of this ontology
	private static Ontology theInstance = new GiveOntology();

	// This is the method to access the singleton music shop ontology object
	public static Ontology getInstance() {
		return theInstance;
	}
	
	GiveOntology(){
		super(ONTOLOGY_NAME, BasicOntology.getInstance());
		
		try {
			add(Route.class);
			add(JourneyData.class);
			add(VisitData.class);
			add(Depot.class);
			add(SavingsNode.class);
			
			add(GiveObjectPredicate.class);
		} catch (BeanOntologyException oe) {
			oe.printStackTrace();
		}
		
	}
	
}
