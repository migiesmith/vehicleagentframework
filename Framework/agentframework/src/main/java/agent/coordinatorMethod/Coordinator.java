package agent.coordinatorMethod;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import agent.coordinatorMethod.ontologies.GiveObjectPredicate;
import agent.coordinatorMethod.ontologies.GiveOntology;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.StaleProxyException;

/**
 * The manager of the agent network.
 * 
 * The Coordinator handles the creation of Employees and Visits
 * and once created, waits for completion inform messages from
 * Employees. Once it receives them all, it creates a solution
 * to the problem from the information within the received messages.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class Coordinator extends Agent {

	// Initialise the language for message contents
	private Codec codec = new SLCodec();
	private Ontology ontologyGive = GiveOntology.getInstance();
	
	// The gui for this Coordinator
	private CoordinatorGui gui;
	// Stores the information about the current problem
	private Problem problem;
	
	// The number of active employees
	private int EMPLOYEE_COUNT = 0;
	
	// The solution to the problem
	private Vector<Route> solution;
	
	// Used to centre the rendering
	private Rectangle2D.Double renderOffset = new Rectangle2D.Double(0,0,0,0);
	
	
	// Initialise this class
	protected void setup() {
		System.out.println(getAID().getLocalName()+" has started.");
		
		// Create the gui for this agent
		gui = new CoordinatorGui(this);
		gui.setStatus("Not Ready.");	

		// Register the Ontology for message content
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontologyGive);
		
		addBehaviour(new EmployeeManager());
	}
	
	public void start(){
		// Create the required agents
		gui.setStatus("Waiting on DataStore Agent.");
		addBehaviour(new OneShotBehaviour(){
			@Override
			public void action() {
				blockingReceive(MessageTemplate.MatchConversationId("data-store-started"));
				gui.setStatus("Creating Agents.");
				
				createAgents();
				
				// Inform the agents to begin
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
				msg.setConversationId("employee-count");
				for(int i = 0; i < EMPLOYEE_COUNT; i++){
					msg.addReceiver(new AID("employee-"+i, AID.ISLOCALNAME));
				}
				msg.setContent(String.valueOf(EMPLOYEE_COUNT));
				send(msg);
				gui.setStatus("Waiting for agents.");	
			}
			
		});
	}

	
	// Returns a copy of the Vector containing the solution
	public Vector<Vector<VisitData>> getSolution(){
		if(solution == null)
			return null;
		Vector<Vector<VisitData>> returnVal = new Vector<Vector<VisitData>>();

        for(Route route : solution){
        	Vector<VisitData> newRoute = new Vector<VisitData>();
            for(VisitData visit : route){
            	VisitData newVisit = new VisitData();
            	newVisit.setAll(visit.name, visit.location, visit.windowStart, visit.windowEnd, visit.visitLength, visit.x, visit.y);
            	newRoute.addElement(newVisit);
            }
            returnVal.addElement(newRoute);
         }        
		return returnVal;
	}
	
	// Returns the depot for the problem
	public Depot getDepot(){
		if(problem == null)
			return null;
		Depot d = new Depot();
		d.setName(problem.depot.getName());
		d.setCommenceTime(problem.depot.getCommenceTime());
		d.setLocation(problem.depot.getLocation());
		d.setX(problem.depot.getX());
		d.setY(problem.depot.getY());
		return d;
	}
	
	// Returns the information required to centre the output
	public Rectangle2D.Double getRenderOffset(){
		return (Rectangle2D.Double) renderOffset.clone();
	}
	
	
	private class EmployeeManager extends Behaviour{

		// Keep track of how many agents have finished
		private int agentsDone = 0;
		// Keep track of what step we are on
		private int step = 0;
		
		// Determine if this behaviour has finished
		private boolean isDone = false;
		
		// The list of savings filled by the employees
		List<SavingsNode> savings = new ArrayList<SavingsNode>();
		
		@Override
		public void action() {
			if(step == 0){ // Check if an agent has finished
				ACLMessage msg = myAgent.receive(MessageTemplate.MatchConversationId("agent-complete"));
				if(msg != null){
					// Read the contents of the message and add it to the savings Vector
					try {
						ContentElement d = getContentManager().extractContent(msg);
						if(d instanceof GiveObjectPredicate){
							@SuppressWarnings("unchecked")
							ArrayList<SavingsNode> savingsNodes = (ArrayList<SavingsNode>) ((GiveObjectPredicate)d).getData();
							savings.addAll(savingsNodes);
						}
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
					// Increment the number of completed agents
					agentsDone++;
					gui.setStatus(agentsDone+" of "+ EMPLOYEE_COUNT +" agents done.");
					if(agentsDone == EMPLOYEE_COUNT) // If they are all done, move to the next step
						step = 1;
				}else{
					// Block this behaviour until an agent is finished
					block();
				}
			}else if(step == 1){
				gui.setStatus("Agents Complete.");

				// For every visit, ask the DataStore for their x and y location
				Point2D.Double visitCoords;
				Iterator<VisitData> it = problem.visits.iterator();
				while(it.hasNext()){
					// Send message for coordinates
					VisitData vd = it.next();
					visitCoords = requestCoordinates(vd.location);
					vd.setX(visitCoords.x);
					vd.setY(visitCoords.y);
				}

				// Send message for coordinates
				Point2D.Double depotCoords = requestCoordinates(problem.depot.location);
				problem.depot.setX(depotCoords.x);
				problem.depot.setY(depotCoords.y);
				
				/* 
				 * Calculate the values required to make the results render nicely
				*/				
				double maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE, minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;
				it = problem.visits.iterator();
				while(it.hasNext()){
					VisitData vd = it.next();
					if(vd.x > maxX)
						maxX = vd.x;
					if(vd.x < minX)
						minX = vd.x;

					if(vd.y > maxY)
						maxY = vd.y;
					if(vd.y < minY)
						minY = vd.y;
				}

				if(problem.depot.x > maxX)
					maxX = problem.depot.x;
				if(problem.depot.x < minX)
					minX = problem.depot.x;

				if(problem.depot.y > maxY)
					maxY = problem.depot.y;
				if(problem.depot.y < minY)
					minY = problem.depot.y;

				double avgX = 0, avgY = 0;
				it = problem.visits.iterator();
				while(it.hasNext()){
					VisitData vd = it.next();
					avgX += vd.getX();
					avgY += vd.getY();
				}
				avgX += problem.depot.x;
				avgY += problem.depot.y;
				
				avgX /= problem.visits.size() + 1;
				avgY /= problem.visits.size() + 1;
				
				double lowerScale = Math.max(maxX - minX, maxY - minY);
				
				renderOffset = new Rectangle2D.Double(avgX, avgY, lowerScale, lowerScale);
				

				// Sort the list of savings supplied by the employees
				Collections.sort(savings);
				
				
				// Use the savings Vector to solve the problem				
				System.out.println("Solving.");
				gui.setStatus("Solving.");
				solve();
				
				// Sum up the total visits
				int totalVisits = 0;
				double totalCost = 0.0d;
				for(Route route : solution){
					totalVisits += route.size();
					totalCost += route.totalEmissions;
				}
				System.out.println("Done.");
				gui.setStatus("Done: Routes="+ solution.size() +" Visits="+ totalVisits +" Cost="+ (int)totalCost );
				System.out.println("Solution:\n"
									+ "- routes = " + solution.size() + "\n"
									+ "- visits = " + totalVisits + "\n"
									+ "- cost = " + totalCost);

				isDone = true;
				gui.readyToSave(true);
				gui.repaint();
			}
		}
		
		// Returns the x and y coordinates of a visit
		private Point2D.Double requestCoordinates(String location){
			// Send message for coordinates for the depot
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			msg.setConversationId("visit-coordinates");
			msg.addReceiver(new AID("data-store", AID.ISLOCALNAME));
			msg.setContent(location);
			myAgent.send(msg);
			// Wait for a response
			ACLMessage response = myAgent.blockingReceive(MessageTemplate.MatchConversationId("visit-coordinates"));
			String[] content = response.getContent().split(",");
			// Store the coordinates
			return new Point2D.Double(Double.parseDouble(content[0]), Double.parseDouble(content[1]));
		}

		@Override
		public boolean done() {
			return isDone;
		}
		
		// Solving code		
		private void solve(){
			solution = new Vector<Route>();
			
			for(VisitData n : problem.visits){
				Route route = new Route();
				route.addElement(n);
				solution.addElement(route);
			}
		
			// Loop through each saving
			for (SavingsNode savingsNode : savings) {
				// Get the route where start is the last customer
				Route route0 = routeWhereCustomerIsFirst(savingsNode.start.getLocalName());
				if(route0 != null){ // check if we found a route for route0
					// Get the route where end is the first customer
					Route route1 = routeWhereCustomerIsLast(savingsNode.end.getLocalName());
					if(route1 != null){ // check if we found a route for route1
						if(route0 == route1){ continue;} // if route0 and route1 are the same, do nothing
						if (withinConstraints(route0, route1)) { // if merge is feasible
							// Merge the two routes
							solution.remove(route1);
							route0.addAll(route1);
							route0.totalEmissions += route1.totalEmissions + savingsNode.emissions;
						}
					}
				}
			}
			
			for(Route route : solution){
				route.totalEmissions += getJourney(new AID(problem.depot.location, AID.ISLOCALNAME),
						new AID(route.get(0).location, AID.ISLOCALNAME)).carEm;
				route.totalEmissions += getJourney(new AID(problem.depot.location, AID.ISLOCALNAME),
						new AID(route.get(route.size()-1).location, AID.ISLOCALNAME)).carEm;
			}
			
		}

		// Returns the JourneyData between from and to
		private JourneyData getJourney(AID from, AID to){
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			msg.addReceiver(from);
			msg.setConversationId("journey-request");
			msg.setLanguage(codec.getName());
			msg.setOntology(ontologyGive.getName());
			try {
				GiveObjectPredicate give = new GiveObjectPredicate();
				give.setData(to);
				getContentManager().fillContent(msg, give);
				send(msg);					
			} catch (CodecException e) {
				e.printStackTrace();
			} catch (OntologyException e) {
				e.printStackTrace();
			}

			// Wait for a response
			ACLMessage response = myAgent.blockingReceive(MessageTemplate.MatchConversationId("journey-request"));
			try {
				ContentElement d = getContentManager().extractContent(response);
				if(d instanceof GiveObjectPredicate){
					Object obj = ((GiveObjectPredicate)d).getData();
					if(obj instanceof JourneyData)
						return (JourneyData)obj;
				}
			} catch (UngroundedException e) {
				e.printStackTrace();
			} catch (CodecException e) {
				e.printStackTrace();
			} catch (OntologyException e) {
				e.printStackTrace();
			}

			return null;
		}

		// returns a route where visitLocation is the last element in a vector
		private Route routeWhereCustomerIsLast(String visitLocation) {
			for (Route r : solution) {
				if (r.firstElement().location.equals(visitLocation))
					return r;
			}
			return null;
		}

		// returns a route where visitLocation is the first element in a vector
		private Route routeWhereCustomerIsFirst(String visitLocation) {
			for (Route r : solution) {
				if (r.firstElement().location.equals(visitLocation)) {
					return r;
				}
			}
			return null;
		}

		// Returns whether or not these routes can be merged
		private boolean withinConstraints(Route route0, Route route1){
			// Create a temporary route that will be 
			Route temp = new Route();
			temp.addAll(route0);
			temp.addAll(route1);
			
			// Create a calendar and set the time to track the time that it would be relative to the visits
			Calendar cal = Calendar.getInstance();
			cal.setTime(problem.depot.commenceTime);
			Iterator<VisitData> it = temp.iterator();
			while(it.hasNext()){
				VisitData vd = it.next();

				// If the windowStart is after the time that the route will be at then this is not a valid merge
				if(vd.windowStart.after(cal.getTime())){
					return false;
				}
				
				// Add the time this visit takes
				cal.add(Calendar.MINUTE, (int)vd.visitLength);

				// If the windowEnd is before the time that the route will be at then this is not a valid merge
				if(vd.windowEnd.before(cal.getTime())){
					return false;
				}
			}
			
			// If we reached here then the merge is valid
			return true;
		}
		
		
	}
	
	// Create the agents required by the system (Employees and Visits)
	private void createAgents(){
		Iterator<VisitData> it = problem.visits.iterator();
		while(it.hasNext()){
			try {
				VisitData visitData = it.next();
				// Create visit agent
				getContainerController().createNewAgent(visitData.location, Visit.class.getCanonicalName(), null)
						.start();
				
				
				// Create employee agent
				getContainerController().createNewAgent("employee-"+EMPLOYEE_COUNT, Employee.class.getCanonicalName(), null)
				.start();
				
				// Send setup messages //
				
				// Visit
				ACLMessage msgV = new ACLMessage(ACLMessage.INFORM);
				msgV.addReceiver(new AID(visitData.location, AID.ISLOCALNAME));
				msgV.setConversationId("visit-set-visitdata");
				msgV.setReplyWith("inf"+System.currentTimeMillis());
				msgV.setLanguage(codec.getName());
				msgV.setOntology(ontologyGive.getName());
				try {
					GiveObjectPredicate give = new GiveObjectPredicate();
					give.setData(visitData);
					getContentManager().fillContent(msgV, give);
					send(msgV);					
				} catch (CodecException e) {
					e.printStackTrace();
				} catch (OntologyException e) {
					e.printStackTrace();
				}
				
				// Employee
				ACLMessage msgE = new ACLMessage(ACLMessage.INFORM);
				msgE.addReceiver(new AID("employee-"+EMPLOYEE_COUNT, AID.ISLOCALNAME));
				msgE.setConversationId("employee-setup-depot");
				msgE.setReplyWith("inf"+System.currentTimeMillis());
				msgE.setLanguage(codec.getName());
				msgE.setOntology(ontologyGive.getName());
				try {
					GiveObjectPredicate give = new GiveObjectPredicate();
					problem.depot.activeEmployees = EMPLOYEE_COUNT;
					give.setData(problem.depot);
					getContentManager().fillContent(msgE, give);
					send(msgE);					
				} catch (CodecException e) {
					e.printStackTrace();
				} catch (OntologyException e) {
					e.printStackTrace();
				}

				msgE.setConversationId("employee-setup-visit");
				msgE.setContent(visitData.location);
				send(msgE);
				
				
				// Increment for next loop
				EMPLOYEE_COUNT++;
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Create visit agent for the depot
		try {
			getContainerController().createNewAgent(problem.depot.location, Visit.class.getCanonicalName(), null)
					.start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
		
		// Visit
		ACLMessage msgV = new ACLMessage(ACLMessage.INFORM);
		msgV.addReceiver(new AID(problem.depot.location, AID.ISLOCALNAME));
		msgV.setConversationId("visit-set-visitdata");
		msgV.setReplyWith("inf"+System.currentTimeMillis());
		msgV.setLanguage(codec.getName());
		msgV.setOntology(ontologyGive.getName());
		try {
			GiveObjectPredicate give = new GiveObjectPredicate();
			give.setData(new VisitData());
			getContentManager().fillContent(msgV, give);
			send(msgV);					
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
		
		System.out.println("Agents Created:\n"
							+ "- " + EMPLOYEE_COUNT + " Employees\n"
							+ "- " + (EMPLOYEE_COUNT+1) + " Visits\n");
	}

	
	void saveResults(String filePath){
		try {
			FileWriter writer = new FileWriter(filePath);

			DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss");

			writer.append("Depot Name,Location,x,y,commenceTime\n");
			writer.append(problem.depot.name +","+ problem.depot.location +","+ problem.depot.x +","
					+ problem.depot.y +","+ df.format(problem.depot.commenceTime)
					+"\n\n\n");
			
			int counter = 0;
			Iterator<Route> vit = solution.iterator();
			while(vit.hasNext()){
				Route route = vit.next();
				writer.append("Visit Name,Location,x,y,windowStart,windowEnd,visitLength\n");
				for(VisitData v : route){
					writer.append(v.name +","+ v.location +","+ v.x +","
									+ v.y +","+ df.format(v.windowStart) +","
									+ df.format(v.windowEnd) +","+ v.visitLength
									+"\n");
				}
				writer.append("Route Name,Route"+ counter +"\n");
				writer.append("Cost,"+ route.totalEmissions+"\n\n");
				counter++;
			}
			
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	// Open a file picker and select a data file to be used
	public void loadFromFile(){
		//Create a file chooser
		final JFileChooser fc = new JFileChooser("Data_files\\");
		fc.setFileFilter(new FileFilter(){
			@Override
			public boolean accept(File f) {
				// Only show .csv files
				if(f.getName().toUpperCase().endsWith(".CSV"))
					return true;				
				return false;
			}
			@Override
			public String getDescription() {
				return null;
			}
			
		});

		//In response to a button click:
		int returnVal = fc.showOpenDialog(null);
		
		// If a file was selected then load it
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			System.out.println("Opening: " + file.getAbsolutePath() + ".");
			
		    try {
				BufferedReader br = new BufferedReader(new FileReader(file));
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();
		        while (line != null) {
		            sb.append(line);
		            sb.append("\n");
		            line = br.readLine();
		        }
		        br.close();
		        problem = new Problem(sb.toString());
				gui.setStatus("Ready.");
		    } catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
}
