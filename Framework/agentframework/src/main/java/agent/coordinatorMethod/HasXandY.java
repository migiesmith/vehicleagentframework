package agent.coordinatorMethod;

/**
 * HasXandY is used so that the CoordinatorGui can
 * get the x and y coordinates from Depot and Visit
 * in the same way.
 * 
 * @author Grant
 */
public interface HasXandY {

	public double getX();
	public void setX(double x);
	
	public double getY();
	public void setY(double y);
	
}
