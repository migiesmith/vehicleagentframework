package agent.coordinatorMethod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import agent.coordinatorMethod.ontologies.GiveObjectPredicate;
import agent.coordinatorMethod.ontologies.GiveOntology;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * This class stores the JourneyData for all of the Visit
 * agents in the system. It acts as a resource server for
 * the Visits and the Coordinator. The Visits can make
 * requests for JourneyData related to them and the
 * Coordinator can make requests for the coordinates of
 * a Visit.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class DataStore extends Agent{

	// Initialise the language for message contents
	private Codec codec = new SLCodec();
	private Ontology ontologyGive = GiveOntology.getInstance();
	
	// The gui for this DataStore
	DataStoreGui gui;
	// The journey data required by the Visit agents
	Map<String, ArrayList<JourneyData>> data;
	
	
	
	// Initialise this class
	protected void setup() {
		System.out.println(getAID().getLocalName()+" has started.");

		// Register the Ontology for message content
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontologyGive);
			
		// Create the gui for this agent
		gui = new DataStoreGui(this);
		gui.setStatus("Not Ready.");		
	}
	
	public void start(){
		// Responds to Visit setup requests
		addBehaviour(new VisitSetupServer());
		// Responds to location requests
		addBehaviour(new LocationServer());
		
		ACLMessage toCoordinator = new ACLMessage(ACLMessage.INFORM);
		toCoordinator.addReceiver(new AID("coordinator", AID.ISLOCALNAME));
		toCoordinator.setConversationId("data-store-started");
		send(toCoordinator);
	}
	
	private class LocationServer extends CyclicBehaviour{
		
		// The template used for incoming messages
		private MessageTemplate template;
		
		LocationServer(){
			template = MessageTemplate.MatchConversationId("visit-coordinates");
		}
		
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive(template);
			if(msg != null){
				// Respond to the request with the x and y coordinates of the requested postcode
				ACLMessage reply = msg.createReply();
				JourneyData jd = data.get(msg.getContent()).get(0);
				reply.setContent(jd.getFromX()+","+jd.getFromY());				
				myAgent.send(reply);
			}else{
				block();
			}
		}
		
	}
	
	private class VisitSetupServer extends CyclicBehaviour{

		// The template used for incoming messages
		private MessageTemplate template;
		
		VisitSetupServer(){
			template = MessageTemplate.MatchConversationId("visit-setup");
		}
		
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive(template);
			if(msg != null){
				// Respond to the request with the Vector containing the JourneyData for sender
				gui.setStatus(msg.getSender().getLocalName() + " requested their data.");
				// Reply with the journey data for the sender
				ACLMessage reply = msg.createReply();
				reply.setLanguage(codec.getName());
				reply.setOntology(ontologyGive.getName());
				try {
					GiveObjectPredicate give = new GiveObjectPredicate();
					ArrayList<JourneyData> content = data.get(msg.getSender().getLocalName());
					if(content == null){
						System.out.println("ERROR: No journey data for agent: " + msg.getSender().getLocalName());
					}
					give.setData(content);
					getContentManager().fillContent(reply, give);
					send(reply);
					gui.visitsReady.add(msg.getSender().getLocalName());
				} catch (CodecException e) {
					e.printStackTrace();
				} catch (OntologyException e) {
					e.printStackTrace();
				}
			}else{
				block();
				gui.setStatus("idle.");
				gui.repaint();
			}
		}
		
	}
	
	private void dataFromString(String content){
		// The regex for a UK postcode
		String postCodeRegex = "((GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2}))";
		String doubleRegex = "[-]{0,1}([0-9]+(\\.[0-9]+)*)";
		String delimeter = ",";
		
		Pattern p = Pattern.compile(// From
									postCodeRegex // Post Code
									+ delimeter
									+ doubleRegex // X Location
									+ delimeter
									+ doubleRegex  // Y Location
									// To
									+ delimeter
									+ postCodeRegex // Post Code
									+ delimeter
									+ doubleRegex // X Location
									+ delimeter
									+ doubleRegex // Y Location
									// Emissions and Time
									+ delimeter
									+ doubleRegex // carEm
									+ delimeter
									+ doubleRegex // carTime
									+ delimeter
									+ doubleRegex // ptEm
									+ delimeter
									+ doubleRegex // ptTime
									); // TODO add the ignored final column

		// Create a matcher to find the matching lines
		Matcher m = p.matcher(content);
		// Initialise data for storing the journeys
		data = new HashMap<String, ArrayList<JourneyData>>();

		JourneyData journey;
		String[] lineContent;
		int sum = 0; // Sum is used to inform the user how many journies were loaded
		while(m.find()){ // Matching line found
			// Parse the line
			lineContent = m.group().split(delimeter);
			journey = new JourneyData();
			journey.setAll(
				lineContent[0], // from
				Double.parseDouble(lineContent[1]), // from x
				Double.parseDouble(lineContent[2]), // from y
				
				lineContent[3], // to
				Double.parseDouble(lineContent[4]), // to x
				Double.parseDouble(lineContent[5]), // to y
				
				Double.parseDouble(lineContent[6]), // carEm
				Double.parseDouble(lineContent[7]), // carTime
				
				Double.parseDouble(lineContent[8]), // ptEm
				Double.parseDouble(lineContent[9]) // ptTime
			);
			
			// If there isn't an entry in the map for this postcode then create one
			if(data.get(journey.from) == null)
				data.put(journey.from, new ArrayList<JourneyData>());
				
			// Store the JourneyData in the entry corresponding to the .from value
			data.get(journey.from).add(journey);
			
			// Increment sum
			sum++;
		}
		System.out.println(sum + " journeys loaded and stored into " + data.size() + " map entries.");
		gui.setStatus(sum + " journeys loaded and stored into " + data.size() + " map entries.");
		
		// Push the data to the gui
		gui.setJourneyData(getData());
		gui.setStatus("Ready. ("+sum+" journeys)");
	}
	
	// Returns the data in a format matching the format used by the gui
	private Object[][] getData(){
		Vector<Object[]> d = new Vector<Object[]>();

		for(ArrayList<JourneyData> v : data.values()){
			Iterator<JourneyData> it = v.iterator();
			while(it.hasNext()){
				JourneyData journey = it.next();
				Object[] displayData = new Object[]{
						journey.from,
						journey.to,
						journey.carEm,
						journey.carTime,
						journey.ptEm,
						journey.ptTime
						};
				d.addElement(displayData);
			}
		}

		Object[][] returnData = new Object[d.size()][6];
		Iterator<Object[]> it = d.iterator();
		int i = 0;
		while(it.hasNext()){
			returnData[i] = it.next();
			i++;
		}
		
		return returnData;
	}


	// Open a file picker and select a data file to be used
	public void loadFromFile(){
		//Create a file chooser
		final JFileChooser fc = new JFileChooser("Data_files\\");
		fc.setFileFilter(new FileFilter(){

			@Override
			public boolean accept(File f) {
				if(f.getName().toUpperCase().endsWith(".CSV"))
					return true;
				
				return false;
			}

			@Override
			public String getDescription() {
				return null;
			}
			
		});

		//In response to a button click:
		int returnVal = fc.showOpenDialog(null);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			System.out.println("Opening: " + file.getAbsolutePath() + ".");
			
		    try {
				BufferedReader br = new BufferedReader(new FileReader(file));
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();
		        while (line != null) {
		            sb.append(line);
		            sb.append("\n");
		            line = br.readLine();
		        }
		        br.close();
		        dataFromString(sb.toString());
		    } catch (IOException e) {
				e.printStackTrace();
			}
		    
		} else {
			System.out.println("No file chosen.");
		}
	}
	
}
