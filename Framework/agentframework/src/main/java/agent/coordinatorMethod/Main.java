package agent.coordinatorMethod;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

/**
 * Simply starts the system by creating a JADE
 * runtime and a Coordinator and DataStore agent.
 * 
 * @author Grant
 */
public class Main {

	public static void main(String[] args) {
		ContainerController myContainer;
		
		Runtime myRuntime = Runtime.instance();

		// prepare the settings for the platform that we're going to start
		Profile myProfile = new ProfileImpl();
		// Increase the maximum results from the DF (yellow-pages)
		myProfile.setParameter("jade_domain_df_maxresult","1000");

		// create the main container
		myContainer = myRuntime.createMainContainer(myProfile);
		
		try {
			//JADE gui
		    AgentController rma = myContainer.createNewAgent("rma", "jade.tools.rma.rma", null);
		    rma.start();
		    
		    // Start the DataStore and Coordinator
		    AgentController dataStore = myContainer.createNewAgent("data-store", DataStore.class.getCanonicalName(), null);
		    dataStore.start();
		    AgentController coordinator = myContainer.createNewAgent("coordinator", Coordinator.class.getCanonicalName(), null);
		    coordinator.start();
		} catch(StaleProxyException e) {
		    e.printStackTrace();
		}
	}
	
}
