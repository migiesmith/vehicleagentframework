package agent.coordinatorMethod;

import jade.content.Concept;

/**
 * Stores the information about a journey between two
 * locations. This is stored in the form: from (location),
 * the x and y coordinates of that location, to (location),
 * the x and y coordinates of that location, the car
 * emissions and time, and the public transport emissions
 * and time. The information is mostly used in the evaluation
 * function to determine which Visits can be merged into
 * one route to make an overall saving.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class JourneyData implements Concept{
	
	public String from;
	public double fromX;
	public double fromY;
	
	public String to;
	public double toX;
	public double toY;
	
	public double carEm; // Car emissions
	public double carTime; // Car travel time
	
	public double ptEm; // Public Transport emissions
	public double ptTime; // Public Transport travel time

	
	// Setter for convenience as Concept is only allowed a default constructor
	public void setAll(String from, double fromX, double fromY, String to, double toX,
		double toY, double carEm, double carTime, double ptEm, double ptTime){

		this.from = from;
		this.fromX = fromX;
		this.fromY = fromY;
		
		this.to = to;
		this.toX = toX;
		this.toY = toY;
		
		this.carEm = carEm;
		this.carTime = carTime;
		
		this.ptEm = ptEm;
		this.ptTime = ptTime;
	}

	// Getters and Setters required by a Concept
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public double getFromX() {
		return fromX;
	}

	public void setFromX(double fromX) {
		this.fromX = fromX;
	}

	public double getFromY() {
		return fromY;
	}

	public void setFromY(double fromY) {
		this.fromY = fromY;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public double getToX() {
		return toX;
	}

	public void setToX(double toX) {
		this.toX = toX;
	}

	public double getToY() {
		return toY;
	}

	public void setToY(double toY) {
		this.toY = toY;
	}

	public double getCarEm() {
		return carEm;
	}

	public void setCarEm(double carEm) {
		this.carEm = carEm;
	}

	public double getCarTime() {
		return carTime;
	}

	public void setCarTime(double carTime) {
		this.carTime = carTime;
	}

	public double getPtEm() {
		return ptEm;
	}

	public void setPtEm(double ptEm) {
		this.ptEm = ptEm;
	}

	public double getPtTime() {
		return ptTime;
	}

	public void setPtTime(double ptTime) {
		this.ptTime = ptTime;
	}
	
	public String toString(){
		return from+","+fromX+","+fromY+","
				+to+","+toX+","+toY+","
				+carEm+","+carTime+","
				+ptEm+","+ptTime;
	}
	
}
