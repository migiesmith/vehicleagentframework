package agent.coordinatorMethod;

import jade.content.Concept;
import jade.core.AID;

/**
 * Stores the AID of the start and end Visit. The start must
 * be found at the start of a route and the end must be found
 * at the end of a route. If one of those is not true then
 * the savings node is skipped during the solving stage. The
 * SavingsNode also stores the saving and travel time. The
 * saving is used to determine which SavingsNodes are the
 * best and the travel time is used to check that time
 * constraints are not broken.
 * 
 * The SavingsNode implements the Comparable interface which
 * allows for the use of Collections.sort to sort a list of
 * SavingsNodes into descending order. This is so that the
 * SavingsNodes with the largest saving are considered first.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class SavingsNode implements Comparable<SavingsNode>, Concept{

	AID start = null; // The Visit that must be at the start of the route
	AID end = null; // The Visit that must be at the end of the route
	double saving = 0.0d; // The saving to be made
	double travelTime = 0.0d; // The time taken to travel between start and end
	double emissions = 0.0d;

	
	// Comparator for sorting
	public int compareTo(SavingsNode sj) {
		return saving < sj.saving ? 1 : saving == sj.saving ? 0 : -1;
	}
	
	
	// Getters and Setters required by a Concept

	public double getEmissions() {
		return emissions;
	}


	public void setEmissions(double emissions) {
		this.emissions = emissions;
	}

	public double getTravelTime() {
		return travelTime;
	}
	public void setTravelTime(double travelTime) {
		this.travelTime = travelTime;
	}
	public AID getStart() {
		return start;
	}
	public void setStart(AID start) {
		this.start = start;
	}
	public AID getEnd() {
		return end;
	}
	public void setEnd(AID end) {
		this.end = end;
	}
	public double getSaving() {
		return saving;
	}
	public void setSaving(double saving) {
		this.saving = saving;
	}	
	
}
