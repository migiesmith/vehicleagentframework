package agent.coordinatorMethod;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 * The interface created by the DataStore to allow the
 * user to selected what journey data file to open and
 * when to start executing. This Gui also contains a
 * table which once a file has been loaded is filled
 * with the contents that were parsed by the DataStore.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class DataStoreGui extends JFrame{

	private JTable table;
	private JLabel lblStatus;
	private DataStore dataStore;
	
	JButton btnLoadFromFile, btnStartSystem;
	
	ArrayList<String> visitsReady = new ArrayList<String>();
	private JLabel lblInfo;
	
	public void setJourneyData(Object[][] data){
		String[] columns = getColumns();
		table.setModel(new DefaultTableModel(data, columns));
		table.invalidate();
		table.repaint();
	}
	
	private String[] getColumns(){
		return new String[]{"From", "To", "carEm", "carTime", "ptEm", "ptTime"};
	}

	/**
	 * Create the application.
	 */
	public DataStoreGui(DataStore dataStore) {
		this.dataStore = dataStore;

		initialize();
		setVisible(true);
	}

	public void setStatus(String msg){
		lblStatus.setText("Status: " + msg);
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("DataStore");
		setBounds(100, 100, 700, 450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		table = new JTable(){
			 @Override
			    public boolean isCellEditable(int row, int column) {
			        return false;
			    }
		};
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			getColumns()
		));
		table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	            final java.awt.Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);


	            Object val = table.getValueAt(row, 0);
	            if (visitsReady.contains(val)) {
	                cellComponent.setForeground(Color.black);
	                cellComponent.setBackground(Color.white);

	            } else {
	                cellComponent.setForeground(Color.black);
	                cellComponent.setBackground(Color.lightGray);
	            }
	            if (isSelected) {
	                cellComponent.setForeground(table.getSelectionForeground());
	                cellComponent.setBackground(table.getSelectionBackground());
	            }

	            return cellComponent;

	        }
		});
		JScrollPane scrollPane = new JScrollPane(table);
		
		JLabel lblDataStore = new JLabel(VisitData.class.getSimpleName()+": ");
		lblDataStore.setHorizontalAlignment(SwingConstants.LEFT);
		
		btnLoadFromFile = new JButton("Load JourneyData from File");
		btnLoadFromFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dataStore.loadFromFile();
				btnStartSystem.setEnabled(true);
			}
		});
		
		lblStatus = new JLabel("Status: Not Ready");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		
		btnStartSystem = new JButton("Start DataStore");
		btnStartSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dataStore.start();
				btnStartSystem.setEnabled(false);
				btnLoadFromFile.setEnabled(false);
			}
		});
		btnStartSystem.setEnabled(false);
		
		lblInfo = new JLabel("*White rows indicate requested data");
		lblInfo.setHorizontalAlignment(SwingConstants.RIGHT);
	    Font italicFont = new Font(lblStatus.getFont().getName(), Font.ITALIC, lblStatus.getFont().getSize());
		lblInfo.setFont(italicFont);
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblDataStore, GroupLayout.PREFERRED_SIZE, 211, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblInfo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(lblStatus, GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnLoadFromFile, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
								.addComponent(btnStartSystem, GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE))))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblDataStore)
						.addComponent(lblInfo))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnLoadFromFile)
							.addGap(11)
							.addComponent(btnStartSystem)
							.addPreferredGap(ComponentPlacement.RELATED, 299, Short.MAX_VALUE)
							.addComponent(lblStatus)))
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);
		
	}
}
