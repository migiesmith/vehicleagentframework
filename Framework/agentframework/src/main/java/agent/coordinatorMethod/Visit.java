package agent.coordinatorMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import agent.coordinatorMethod.ontologies.GiveObjectPredicate;
import agent.coordinatorMethod.ontologies.GiveOntology;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Visit is much like DataStore in the way that it mostly
 * acts as a server for the Employees to request data from.
 * When the Visit is created, it sends a request to the
 * DataStore for all the JourneyData related to that Visit.
 * The DataStore returns a list of all the Journeys that go
 * from that Visit to another (or the depot). Each Visit
 * stores their list as a sort of cache. Having each Visit
 * essentially caching sections of the JourneyData removes
 * the DataStore as a potential bottleneck of the system.
 * This is the case as instead of all agents sending requests
 * for information to the DataStore, the requests a distributed
 * evenly amongst the Visits.
 * 
 * This also has the benefit of knowing where data can be
 * quickly accessed from as an Employee sends a request
 * to the Visit that has the location matching the from
 * location it is looking for. Allowing for requests to
 * go straight to the required component of the system.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class Visit extends Agent {

	// Initialise the language for message contents
	private Codec codec = new SLCodec();
	private Ontology ontologyGive = GiveOntology.getInstance();
	
	private VisitData visitData;
	
	Map<String, JourneyData> journeyData = new HashMap<String, JourneyData>();

	
	
	
	// Initialise this class
	protected void setup() {

		// Register the Ontology for message content
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontologyGive);
		
		addBehaviour(new SetupRequester());
		addBehaviour(new RequestHandler());
		
	}
	
	private class RequestHandler extends CyclicBehaviour{

		// The templates for incoming requests
		MessageTemplate requestVisitDataTemplate, requestScoreTemplate;
		
		RequestHandler(){
			requestVisitDataTemplate = MessageTemplate.MatchConversationId("visit-information-request");
			requestScoreTemplate = MessageTemplate.MatchConversationId("journey-request");
		}
		
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive(MessageTemplate.or(requestVisitDataTemplate, requestScoreTemplate));
			if(msg != null){
				// Return the VisitData associated with this Visit
				if(requestVisitDataTemplate.match(msg)){
					ACLMessage reply = msg.createReply();
					reply.setLanguage(codec.getName());
					reply.setOntology(ontologyGive.getName());
					try {
						GiveObjectPredicate give = new GiveObjectPredicate();
						give.setData(visitData);
						getContentManager().fillContent(reply, give);
						send(reply);					
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
					
				}else{
					// Return the JourneyData from this Visit to another
					try {
						ContentElement d = getContentManager().extractContent(msg);
						if(d instanceof GiveObjectPredicate){
							Object data = ((GiveObjectPredicate)d).getData();
							if(data instanceof AID){
								ACLMessage reply = msg.createReply();
								reply.setLanguage(codec.getName());
								reply.setOntology(ontologyGive.getName());
								try {
									GiveObjectPredicate content = new GiveObjectPredicate();									
									JourneyData jd = journeyData.get(((AID)data).getLocalName());
									content.setData(jd);
									getContentManager().fillContent(reply, content);
									send(reply);
								} catch (CodecException e) {
									e.printStackTrace();
								} catch (OntologyException e) {
									e.printStackTrace();
								}
							}
						}
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
					
				}
			}else{
				block();
			}
		}
		
	}
	
	private class SetupRequester extends Behaviour{

		boolean dataStoreSetup = false, coordinatorSetup = false;
		int step = 0;

		// The templates for incoming responses
		MessageTemplate setupDataStoreTemplate, setupCoordinatorTemplate;
		
		SetupRequester(){
			setupDataStoreTemplate = MessageTemplate.MatchConversationId("visit-setup");
			setupCoordinatorTemplate = MessageTemplate.MatchConversationId("visit-set-visitdata");			
		}
		
		@Override
		public void action() {
			switch(step){
			case 0:{ // Send request
				ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
				msg.addReceiver(new AID("data-store", AID.ISLOCALNAME));
				msg.setConversationId("visit-setup");
				msg.setContent(getAID().getLocalName());
				msg.setReplyWith("req"+System.currentTimeMillis());
				myAgent.send(msg);
				step = 1;
				break;
				}			
			case 1:{ // Get response
				ACLMessage msg = myAgent.receive(MessageTemplate.or(setupDataStoreTemplate, setupCoordinatorTemplate));
				if(msg != null){
					if(setupDataStoreTemplate.match(msg)){
						try { // Data Store
							ContentElement d = getContentManager().extractContent(msg);
							if(d instanceof GiveObjectPredicate){
								Object obj = ((GiveObjectPredicate)d).getData();
								if(obj instanceof ArrayList){
									@SuppressWarnings("unchecked")
									ArrayList<JourneyData> journeyVector = (ArrayList<JourneyData>) obj;
									Iterator<JourneyData> it = journeyVector.iterator();
									while(it.hasNext()){
										JourneyData jd = it.next();
										journeyData.put(jd.to, jd);
									}
								}
							}
						} catch (UngroundedException e) {
							e.printStackTrace();
						} catch (CodecException e) {
							e.printStackTrace();
						} catch (OntologyException e) {
							e.printStackTrace();
						}
						dataStoreSetup = true;
					}else{ // Coordinator
						try {
							ContentElement d = getContentManager().extractContent(msg);
							if(d instanceof GiveObjectPredicate){
								Object obj = ((GiveObjectPredicate)d).getData();
								if(obj instanceof VisitData)
									visitData = (VisitData) obj;
							}
						} catch (UngroundedException e) {
							e.printStackTrace();
						} catch (CodecException e) {
							e.printStackTrace();
						} catch (OntologyException e) {
							e.printStackTrace();
						}
						coordinatorSetup = true;
					}
				}else{
					block();
				}
				break;
				}				
			}
		}

		@Override
		public boolean done() {
			return dataStoreSetup && coordinatorSetup;
		}
		
	}	
	
	
}
