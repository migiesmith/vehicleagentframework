package agent.coordinatorMethod;

import java.util.Vector;

import jade.content.Concept;

@SuppressWarnings("serial")
public class Route extends Vector<VisitData> implements Concept{
	
	public double totalEmissions = 0.0d;

	public double getTotalEmissions() {
		return totalEmissions;
	}

	public void setTotalEmissions(double totalEmissions) {
		this.totalEmissions = totalEmissions;
	}
	
}
