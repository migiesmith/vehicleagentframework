package agent.coordinatorMethod;

import java.util.Date;

import jade.content.Concept;

/**
 * Stores the information about each visit that needs to be
 * made. Each Visit agent has its own VisitData and each
 * Employee has a copy of the VisitData that they have been
 * assigned. VisitData stores the name, location (post code),
 * the start and end of the time window, the visit length, and
 * the x and y coordinates of the location.
 *
 * @author Grant
 */
@SuppressWarnings("serial")
public class VisitData implements Concept, HasXandY{
	
	public String name; // The name of the visit
	public String location; // The post code of the visit
	public Date windowStart; // The start of the time window that this visit can be visited
	public Date windowEnd;// The end of the time window that this visit can be visited
	public double visitLength; // The length of this visit
	public double x, y; // The x and y coordinates of this visit

	
	// Setter for convenience as Concept is only allowed a default constructor
	public void setAll(String name, String location, Date windowStart, Date windowEnd, double visitLength, double x, double y){
		this.name = name;
		this.location = location;
		this.windowStart = windowStart;
		this.windowEnd = windowEnd;
		this.visitLength = visitLength;
		this.x = x;
		this.y = y;
	}

	// Getters and Setters required by a Concept
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getWindowStart() {
		return windowStart;
	}

	public void setWindowStart(Date windowStart) {
		this.windowStart = windowStart;
	}

	public Date getWindowEnd() {
		return windowEnd;
	}

	public void setWindowEnd(Date windowEnd) {
		this.windowEnd = windowEnd;
	}

	public double getVisitLength() {
		return visitLength;
	}

	public void setVisitLength(double visitLength) {
		this.visitLength = visitLength;
	}
}