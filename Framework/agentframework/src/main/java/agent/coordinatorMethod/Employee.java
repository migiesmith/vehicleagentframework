package agent.coordinatorMethod;

import java.util.Vector;

import agent.coordinatorMethod.ontologies.GiveObjectPredicate;
import agent.coordinatorMethod.ontologies.GiveOntology;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SubscriptionInitiator;

/**
 * The Employee is one of the main agents in the system.
 * It stores a copy of the depot and the visit it is
 * assigned. It also creates a list of other Employees
 * using a subscription to the DF Service. The main
 * processing of the Employee is to send out requests to
 * the other Employees to execute evaluations and to
 * complete any evaluation requests it receives. The
 * result of any completed evaluation request is stored
 * within a list of savings. This list is sent to the
 * Coordinator as the content of the inform message to
 * let the Coordinator know that the agent has finished.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class Employee extends Agent {

	// Initialise the language for message contents
	private Codec codec = new SLCodec();
	private Ontology ontologyGive = GiveOntology.getInstance();
	
	// The number of active employee agents (-1 implies that the agent is not ready to start)
	private int employeeCount = -1;
	
	// The depot the problem revolves around
	private Depot depot;

	// The visit associated with this employee
	private VisitData myVisit;
	// The AID of myVisit
	private AID myVisitAID;
	private Vector<AID> otherEmployees = new Vector<AID>();

	
	// The list of savings found by this employee
	private Vector<SavingsNode> savings = new Vector<SavingsNode>();
	
	
	
	// Initialise this class
	protected void setup(){
		// Register the Ontology for message content
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontologyGive);
		
		registerWithDF();
		
		// Add the behaviours
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Employee");
		dfd.addServices(sd);

		addBehaviour(new EmployeeSubscription(this, dfd));
		addBehaviour(new SetupBehaviour());
	}
	
	private class ClarkeWrightBehaviour extends Behaviour{

		// Determine if this behaviour has finished
		private boolean isDone = false;
		// Keep track of what step we are on
		private int step = 0;
		
		// Keep track of how many requests this employee has made
		private int requestsMade = 0;
		// Keep track of how many evaluations this employee has made
		private int evaluationsMade = 0;
		
		// The template for incoming evaluations
		MessageTemplate evalTemplate;
				
		
		ClarkeWrightBehaviour(){
			evalTemplate = MessageTemplate.MatchConversationId("employee-eval");
		}
		
		// Returns the JourneyData between from and to
		private JourneyData getJourney(AID from, AID to){
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			msg.addReceiver(from);
			msg.setConversationId("journey-request");
			msg.setLanguage(codec.getName());
			msg.setOntology(ontologyGive.getName());
			try {
				GiveObjectPredicate give = new GiveObjectPredicate();
				give.setData(to);
				getContentManager().fillContent(msg, give);
				send(msg);					
			} catch (CodecException e) {
				e.printStackTrace();
			} catch (OntologyException e) {
				e.printStackTrace();
			}

			// Wait for a response
			ACLMessage response = myAgent.blockingReceive(MessageTemplate.MatchConversationId("journey-request"));
			try {
				ContentElement d = getContentManager().extractContent(response);
				if(d instanceof GiveObjectPredicate){
					Object obj = ((GiveObjectPredicate)d).getData();
					if(obj instanceof JourneyData)
						return (JourneyData)obj;
				}
			} catch (UngroundedException e) {
				e.printStackTrace();
			} catch (CodecException e) {
				e.printStackTrace();
			} catch (OntologyException e) {
				e.printStackTrace();
			}

			return null;
		}
		
		// Evaluate the savings between start and this employees visit
		private void evaluate(AID start){
			JourneyData depotToEnd = getJourney(myVisitAID, new AID(depot.location, AID.ISLOCALNAME));
			JourneyData depotToStart = getJourney(start, new AID(depot.location, AID.ISLOCALNAME));
			JourneyData endToStart = getJourney(start, myVisitAID);

			// Clarke-Wright
			double saving = (depotToEnd.carEm + depotToStart.carEm) - endToStart.carEm; // Car
			//double saving = (depotToEnd.ptEm + depotToStart.ptEm) - endToStart.ptEm; // Public Transport
			
			// Create the savingsNode for this journey
			SavingsNode savingsNode = new SavingsNode();
			savingsNode.setStart(start);
			savingsNode.setEnd(myVisitAID);
			savingsNode.setSaving(saving);
			savingsNode.setTravelTime(endToStart.carTime);
			savingsNode.setEmissions(endToStart.carEm);
			//savingsNode.setEmissions(endToStart.ptEm);
			
			// Add the savingsNode to the savings Vector
			savings.addElement(savingsNode);
			
			// Increment the number of evaluations
			evaluationsMade++;			
		}
		
		@Override
		public void action() {
			switch(step){
			case 0:{
				// Receive the number of active employees and move to step 1
				ACLMessage msg = myAgent.receive(MessageTemplate.MatchConversationId("employee-count"));
				if(msg != null){
					employeeCount = Integer.parseInt(msg.getContent()) - 1;
					step = 1;
				}else{
					// Block this behaviour until an agent is finished
					block();
				}
				break;
				}
			case 1:{
				// Don't start until I have found all of the other employees
				if(otherEmployees.size() != employeeCount || employeeCount == -1)
					return;

				// If I still have to make a request then do so
				if(requestsMade < otherEmployees.size()){
					ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
					request.addReceiver(otherEmployees.get(requestsMade));
					request.setConversationId("employee-eval");
					request.setLanguage(codec.getName());
					request.setOntology(ontologyGive.getName());
					try {
						GiveObjectPredicate give = new GiveObjectPredicate();
						give.setData(myVisitAID);
						getContentManager().fillContent(request, give);
						send(request);
						requestsMade++;			
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
				}

				// If I have received an evaluation request then execute it
				ACLMessage msg = myAgent.receive(evalTemplate);
				if(msg != null){
					try {
						ContentElement d = getContentManager().extractContent(msg);
						if(d instanceof GiveObjectPredicate){
							evaluate((AID)((GiveObjectPredicate)d).getData());
						}
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
				}else{
					// Block this behaviour until an agent is finished
					block();
				}
				
				// Completed the evaluations and requests? Move onto step 2
				if(evaluationsMade == otherEmployees.size() && requestsMade == otherEmployees.size()){
					step = 2;
				}

				break;
				}
			case 2:{
				// Inform the Coordinator that I have finished (content = savings Vector)
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
				msg.setConversationId("agent-complete");
				msg.addReceiver(new AID("coordinator", AID.ISLOCALNAME));
				msg.setLanguage(codec.getName());
				msg.setOntology(ontologyGive.getName());
				try {
					GiveObjectPredicate give = new GiveObjectPredicate();
					give.setData(savings);
					getContentManager().fillContent(msg, give);
					send(msg);
				} catch (CodecException e) {
					e.printStackTrace();
				} catch (OntologyException e) {
					e.printStackTrace();
				}
				
				// State that the behaviour is done
				isDone = true;
				break;
				}
			}
		}
		
		@Override
		public boolean done() {
			return isDone;
		}
		
	}
	
	private class SetupBehaviour extends Behaviour{

		// Keep track of whether we have received the depot and visit
		private boolean depotReceived = false, visitReceived = false;
		
		// The template for receiving the setup messages
		private MessageTemplate template;
		
		
		SetupBehaviour(){
			template = MessageTemplate.or(MessageTemplate.MatchConversationId("employee-setup-visit"), MessageTemplate.MatchConversationId("employee-setup-depot"));
		}
		
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive(template);
			if(msg != null){ // Received the depot from the Coordinator - store it
				if(msg.getConversationId().equals("employee-setup-depot")){
					try {
						ContentElement d = getContentManager().extractContent(msg);
						if(d instanceof GiveObjectPredicate){
							Object obj = ((GiveObjectPredicate)d).getData();
							if(obj instanceof Depot)
								depot = (Depot) obj;
						}
						depotReceived = true;
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
				}else{
					// Received the Visit from the Coordinator - store it
					myVisitAID = new AID(msg.getContent(), AID.ISLOCALNAME);
					
					ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
					request.addReceiver(new AID(msg.getContent(), AID.ISLOCALNAME));
					request.setConversationId("visit-information-request");
					myAgent.send(request);
					
					msg = myAgent.blockingReceive(MessageTemplate.MatchConversationId("visit-information-request"));
					// Response
					try {
						ContentElement d = getContentManager().extractContent(msg);
						if(d instanceof GiveObjectPredicate){
							Object obj = ((GiveObjectPredicate)d).getData();
							if(obj instanceof VisitData){
								myVisit = ((VisitData) obj);
							}
						}
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
					visitReceived = true;
				}
			}else{
				// Block this behaviour until an agent is finished
				block();
			}
		}

		@Override
		public boolean done() {
			// If this is done then start the ClarkeWrightBehaviour
			if(depotReceived && visitReceived){
				myAgent.addBehaviour(new ClarkeWrightBehaviour());
				return true;
			}
			return false;
		}
		
	}


	// Finds all Employees on the yellow-pages (DF)
	private class EmployeeSubscription extends SubscriptionInitiator {

		/**
		 * @param a The Agent to link to
		 * @param period The time interval, in milliseconds, between each tick
		 */
		public EmployeeSubscription(Agent a, DFAgentDescription dfd) {
			super(a, DFService.createSubscriptionMessage(a,getDefaultDF(),dfd,null));
		}

		protected void handleInform(ACLMessage inform) {
			try {
				DFAgentDescription[] results = DFService.decodeNotification(inform.getContent());
				// If there are results
				if (results.length > 0) {
					for (int i = 0; i < results.length; i++) {
						if (results[i].getAllServices().hasNext()) {
							// Add the new agent to the list (if it isn't this Employee)
							if(!results[i].getName().getLocalName().equals(getAID().getLocalName()))
								otherEmployees.addElement(results[i].getName());
						} else {
							// Remove the agent from the list
							otherEmployees.removeElement(results[i].getName());
						}
					}
				}
			} catch (FIPAException fe) {
				fe.printStackTrace();
			}

		}

	}	

	// Registers this agent with the DF Service
	private void registerWithDF(){
		// Register the RoutingAgent in the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Employee");
		sd.setName(getLocalName() + "-Employee");
		dfd.addServices(sd);
		try{
			DFService.register(this, dfd);
		}catch(FIPAException fe){
			fe.printStackTrace();
		}
	}
	
	@Override
	protected void takeDown(){
		try {
			DFService.deregister(this);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
	}
	
}
