package agent.coordinatorMethod;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

/**
 * The interface created by the coordinator to allow the user to
 * choose what file to use as the problem and when to start
 * executing. This Gui uses a JPanel within the JFrame to supply
 * the user with a graphical representation of the solution.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class CoordinatorGui extends JFrame{
	
	// The panel used to render the solution
	JPanel panel;
	// The label used to show the status of the system
	private JLabel lblStatus;
	// The coordinator that owns this Gui
	private Coordinator coordinator;
	
	JButton btnLoadFromFile, btnStartSystem, btnSaveAscsv;

	private boolean renderLocations = true;
	JCheckBox chckbxRenderLocations;
	private JTable table;
	
	private int selectedRoute = -1;
	
	private int visitRadius = 5;
	

	public CoordinatorGui(Coordinator coordinator) {
		this.coordinator = coordinator;

		initialize();
		setVisible(true);
	}

	public void setStatus(String msg){
		lblStatus.setText("Status: " + msg);
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		lblStatus.setVerticalAlignment(SwingConstants.CENTER);
	}
	

    public void paint(Graphics g) {
    	super.paint(g);

    	
    	BufferedImage b = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_RGB);

    	
        Graphics2D g2 = (Graphics2D) b.getGraphics();
    	FontMetrics fm = g2.getFontMetrics();
        g2.drawRect(0, 0, 640, 480);
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());

        
    	g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    	g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    	g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
    	g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    	g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    	g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);  	
        

        Depot depot = coordinator.getDepot();
        Vector<Vector<VisitData>> solution = coordinator.getSolution();

        if(depot != null && solution != null){
        	
        	Object[][] tableRoutes = new Object[solution.size()][1];
        	for(int i = 0; i < tableRoutes.length; i++)
        		tableRoutes[i][0] = i;

        	table.setModel(new DefaultTableModel(
        			tableRoutes,
        			new String[] {
        				"Route"
        			}
        		));

        	Rectangle2D.Double renderOffset = coordinator.getRenderOffset();
        	double smallerSide = Math.min(panel.getWidth(), panel.getHeight());
        	renderOffset.width = (smallerSide / renderOffset.width) * 0.8d;
        	renderOffset.height = (smallerSide / renderOffset.height) * 0.8d;
            for(Vector<VisitData> route : solution){
                for(VisitData visit : route){
                	visit.x -= renderOffset.x;
                	visit.y -= renderOffset.y;
                	visit.x *= renderOffset.getWidth();
                	visit.y *= renderOffset.getHeight();
                }        	
			}
            depot.x -= renderOffset.x;
            depot.y -= renderOffset.y;
            depot.x *= renderOffset.getWidth();
            depot.y *= renderOffset.getHeight();
            
            g2.translate(panel.getWidth()/2, panel.getHeight()/2);//renderOffset.y * renderOffset.height);
            
        	
	        Iterator<Vector<VisitData>> rit = solution.iterator();
	        HasXandY prev;
	        int routeNo = -1;
	        while(rit.hasNext()){
	        	routeNo++;
	        	if(selectedRoute != routeNo && selectedRoute != -1)
		        	g2.setColor(new Color(255,0,0,10));
	        	else
		        	g2.setColor(Color.red);
	        	
	        	Vector<VisitData> route = rit.next();
	            Iterator<VisitData> nit = route.iterator();
	            prev = depot;
	            while(nit.hasNext()){
	            	VisitData n = nit.next();
	            	g2.drawLine((int)n.x, (int)n.y, (int)prev.getX(), (int)prev.getY());
	            	prev = n;
	            }
	        	g2.drawLine((int)depot.x, (int)depot.y, (int)prev.getX(), (int)prev.getY());
	        }
	        
	
	    	routeNo = -1;
	        rit = solution.iterator();
	        while(rit.hasNext()){
	        	routeNo++;
	        	
	        	Vector<VisitData> route = rit.next();
	            Iterator<VisitData> nit = route.iterator();
	            while(nit.hasNext()){
	            	VisitData n = nit.next();
	            	
		        	if(selectedRoute != routeNo && selectedRoute != -1)
			        	g2.setColor(new Color(255, 221, 84,10));
		        	else
			        	g2.setColor(new Color(255, 221, 84));
		        		
	            	g2.fillOval((int)n.x - visitRadius, (int)n.y - visitRadius, visitRadius*2, visitRadius*2);
	            	g2.setColor(Color.DARK_GRAY);
	            	
	            	
		        	if(selectedRoute != routeNo && selectedRoute != -1)
			        	g2.setColor(new Color(50,50,50,10));
		        	else
			        	g2.setColor(new Color(50,50,50));
		        	
	            	g2.drawOval((int)n.x - visitRadius, (int)n.y - visitRadius, visitRadius*2, visitRadius*2);

	            	if(renderLocations && (selectedRoute == routeNo || selectedRoute == -1)){
		        		g2.setColor(Color.BLACK);
		            	String location = n.location;
		            	Rectangle2D r = fm.getStringBounds(location, g2);
		            	g2.drawString(location, (int)(n.x - r.getWidth()/2), (int)(n.y+r.getHeight()/2));
	            	}
	            }
	        }
	        

	    	g2.setColor(Color.GREEN);
	    	g2.fillOval((int)depot.x - visitRadius, (int)depot.y - visitRadius, visitRadius*2, visitRadius*2);
	    	g2.setColor(Color.DARK_GRAY);
	    	g2.drawOval((int)depot.x - visitRadius, (int)depot.y - visitRadius, visitRadius*2, visitRadius*2);
	    	g2.setColor(Color.BLACK);
	    	Rectangle2D depotRect = fm.getStringBounds(depot.location, g2);
	    	if(renderLocations)g2.drawString(depot.location, (int)(depot.x - depotRect.getWidth()/2), (int)(depot.y+depotRect.getHeight()/2));
	    	depotRect = fm.getStringBounds("Depot", g2);
	    	g2.drawString("Depot", (int)(depot.x - depotRect.getWidth()/2), (int)(depot.y- (renderLocations ? (depotRect.getHeight()/2) : 0)));

        }else{
        	g2.setColor(Color.BLACK);
            g2.translate(panel.getWidth()/2, panel.getHeight()/2);
            String text = this.btnLoadFromFile.isEnabled() ? "Select a file and click 'Start Coordinator'." : "Processing..";
	    	Rectangle2D textRect = fm.getStringBounds(text, g2);
        	g2.drawString(text, (int)-textRect.getWidth()/2, 0);
        	
        }
        
        Graphics gPanel = panel.getGraphics();
        gPanel.drawImage(b, 0, 0, null);
        gPanel.setColor(Color.GRAY);
        gPanel.drawRect(2, 2, panel.getWidth()-5, panel.getHeight()-5);
        gPanel.drawRect(0, 0, panel.getWidth()-1, panel.getHeight()-1);
        gPanel.setColor(Color.BLACK);
        
    }
    
    public void readyToSave(boolean b){
    	btnSaveAscsv.setEnabled(b);
    }
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("Coordinator");
		setBounds(100, 100, 750, 450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		btnLoadFromFile = new JButton("Load Problem from File");
		btnLoadFromFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				coordinator.loadFromFile();
				btnStartSystem.setEnabled(true);
			}
		});
		
		lblStatus = new JLabel("Status: Not Ready");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		
		btnStartSystem = new JButton("Start Coordinator");
		btnStartSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				coordinator.start();
				btnStartSystem.setEnabled(false);
				btnLoadFromFile.setEnabled(false);
				repaint();
			}
		});
		btnStartSystem.setEnabled(false);
		
		panel = new JPanel();
		
		chckbxRenderLocations = new JCheckBox("Render Locations");
		chckbxRenderLocations.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				renderLocations = e.getStateChange() == 1 ? true : false;
				repaint();
			}
		});
		chckbxRenderLocations.setSelected(true);
		
		JScrollPane scrollPane = new JScrollPane((Component) null);
		
		JButton btnNewButton = new JButton("Show All Routes");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedRoute = -1;
				repaint();
			}
		});
		
		final JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(5), new Integer(1), null, new Integer(1)));
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				visitRadius = (Integer)spinner.getValue();
				repaint();
			}
		});
		
		JLabel lblNewLabel = new JLabel("Visit Radius");
		
		btnSaveAscsv = new JButton("Save as .csv");
		btnSaveAscsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser("results/");
				fileChooser.setDialogTitle("Specify a file to save");   
				 
				int userSelection = fileChooser.showSaveDialog(CoordinatorGui.this);
				 
				if (userSelection == JFileChooser.APPROVE_OPTION) {
				    File fileToSave = fileChooser.getSelectedFile();
					coordinator.saveResults(fileToSave.getAbsolutePath());
					Object[] options = {"OK"};
				    JOptionPane.showOptionDialog(CoordinatorGui.this,
				                   "Saved to: "+fileToSave.getAbsolutePath(),"Save Dialog",
				                   JOptionPane.PLAIN_MESSAGE,
				                   JOptionPane.QUESTION_MESSAGE,
				                   null,
				                   options,
				                   options[0]);
				}
			}
		});
		btnSaveAscsv.setEnabled(false);

		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(lblNewLabel)
							.addGap(39)
							.addComponent(btnSaveAscsv, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addComponent(btnStartSystem, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
						.addComponent(btnLoadFromFile, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(lblStatus, GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE))
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
						.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
						.addComponent(chckbxRenderLocations))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnLoadFromFile)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnStartSystem)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(11)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblNewLabel)
										.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(7)
									.addComponent(chckbxRenderLocations))
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnSaveAscsv)))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnNewButton)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblStatus))
						.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE))
					.addContainerGap())
		);
		
		table = new JTable() {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedRoute = table.getSelectedRow();
				repaint();
			}
		});
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Route"
			}
		));

		scrollPane.setViewportView(table);
		getContentPane().setLayout(groupLayout);
		
	}
}
