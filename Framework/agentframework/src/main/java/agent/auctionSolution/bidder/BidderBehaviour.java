package agent.auctionSolution.bidder;

import java.util.Calendar;
import java.util.Iterator;

import agent.auctionSolution.JourneyInfoHelper;
import agent.auctionSolution.dataObjects.BidResponse;
import agent.auctionSolution.dataObjects.CostVariables;
import agent.auctionSolution.dataObjects.Depot;
import agent.auctionSolution.dataObjects.JourneyData;
import agent.auctionSolution.dataObjects.ReturnToCar;
import agent.auctionSolution.dataObjects.Route;
import agent.auctionSolution.dataObjects.VisitData;
import agent.auctionSolution.dataObjects.carShare.CarShare;
import agent.auctionSolution.dataObjects.carShare.CarShareOffer;
import agent.auctionSolution.dataObjects.carShare.CarShareResult;
import agent.auctionSolution.ontologies.GiveObjectPredicate;
import agent.auctionSolution.ontologies.GiveOntology;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * The behaviour that handles the bidding of visits. Communicates with
 * the auctioneer to try and obtain visits that would have the minimal
 * cost increase to the bidder's route.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public abstract class BidderBehaviour extends Behaviour {

	// Initialise the language for message contents
	protected Codec codec = new SLCodec();
	protected Ontology ontologyGive = GiveOntology.getInstance();

	protected MessageTemplate bidTemplate, wonBidTemplate, biddingFinishedTemplate, carShareAcceptTemplate;

	// List of visits to distribute
	public Route route = new Route();
	// The depot for this problem
	public Depot depot;
	
	// Used to check travel times and to check if a route is valid
	public JourneyInfoHelper journeyInfo;
	
	// The visit that the bidder is trying to return
	protected VisitData returningVisit = null;
	protected int indexOfReturningVisit = 0;
	// How many returns this bidder can make
	protected int returnsLeft = 50;
	
	// Toggles what is used to cost the bids
	protected String minimiseFactor = "NOT_SET";
	// Toggles the mode used for transport
	protected String transportMode = "NOT_SET";
	protected int pointOfModeSwapping = -1;
	
	protected ACLMessage carShare = null;
	
	// Whether or not this behaviour has finished
	private boolean isDone = false;

	// Enum for the steps this behaviour can be in
	private enum STEP {
		PreInit, Initialise, HandleBids, FinishUp
	}

	// The current step this behaviour is on
	private STEP currStep = STEP.PreInit;

	protected CostVariables costVars;
	
	protected boolean isCarShareEnabled = true;
	
	// Is this to be reused
	protected boolean isReusable = false;
	
	// The AID for the Car Share Server (for networking)
	protected AID carShareServerAID = null;
	// The AID for the Auctioneer (for networking)
	protected AID auctioneerAID = null;
	
	public BidderBehaviour() {
		// Create the message templates
		bidTemplate = MessageTemplate.MatchConversationId("visit-bid");
		wonBidTemplate = MessageTemplate.MatchConversationId("visit-won");
		biddingFinishedTemplate = MessageTemplate.MatchConversationId("bidding-finished");
		carShareAcceptTemplate = MessageTemplate.MatchConversationId("car-share-accepted");
	}
		
	
	
	// abstract methods that must be implemented //
	
	/**
	 * Execute any inital code that is needed
	 * @return
	 */
	protected abstract boolean initialise();

	/**
	 * @param v The visit that is being bidded for
	 * @return The bid to be made
	 */
	public abstract double getBid(VisitData v);

	/**
	 * Checks if the visit can be added to the route
	 * @param v
	 * @return Whether or not the visit can be added to the route
	 */
	public abstract boolean canAllocateToRoute(VisitData v);

	/**
	 * @param v
	 * @param position
	 * @return The cost of adding a visit at a certain position
	 */
	public abstract double costForAddingAt(VisitData v, int position);
	
	/**
	 * Process the visit that has been won
	 * @param v
	 */
	public abstract void handleWonBid(VisitData v);

	/**
	 * Execute anything before ending the behaviour
	 * @return
	 */
	public abstract boolean finishUp();
	
	/**
	 * Get a visit if there are any that should be returned
	 * 
	 * This is called just before canAllocateToRoute(visitData), 
	 * getBid(visitData), and then the sending of the bid. 
	 * As this is called before canAllocateToRoute, calling canAllocateToRoute 
	 * from within this function will not affect where the visit will be added back 
	 * to thus allowing for the removal of a visit to check if its current allocation 
	 * is optimal (must be returned to the route before returning a value).
	 * 
	 * @param visitData The visit that is being bidded for 
	 * @return VisitData If there is a visit to return otherwise null
	 */
	public abstract VisitData getVisitToReturn(VisitData visitData);
	
	/**
	 * Reset the bidder so that it can be reused. This is only called if the bidder
	 * has been set as reusable. The super of this function should be called if overridden!
	 */
	public void resetBidder(){
		// Reset values so that this bidder can be reused
		route = new Route();
		route.setRouteOwner(myAgent.getAID());
		depot = null;
		returningVisit = null;
		indexOfReturningVisit = 0;
		minimiseFactor = "NOT_SET";
		transportMode = "NOT_SET";
		pointOfModeSwapping = -1;
		carShare = null;
		isDone = false;
		isCarShareEnabled = true;
	}

	// Returns the AID for the map server registered with the DF Service using a service type of "car-share-server"
	public AID getCarShareServerAID(){
		// Setup the search parameters
		DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd  = new ServiceDescription();
        sd.setType("car-share-server");
        dfd.addServices(sd);
        
		try {
			// Search for the Car-Share-Server and return its AID
	        DFAgentDescription[] searchResult = DFService.search(myAgent, dfd);
	        return searchResult[0].getName();
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		// If we couldn't find it, return null
		return null;
	}
	
	/**
	 * Determines from where in the list it is going to be added 
	 * what transport mode should be used. This is done by taking 
	 * into account the position relative to the pointOfModeSwapping 
	 * if 'Mixed' was set for the transport mode, and the cost of 
	 * either using Car or Public Transport.
	 * @param v
	 * @param position
	 * @return
	 */
	public String determineTransportMode(VisitData v, int position){
		// The initial transport mode for the visit
		String startingMode = v.transport;
		// The transport mode that will be return as a result
		String tMode = transportMode;
		// If mode swapping is enabled
		if (pointOfModeSwapping != -1) {
			// If this is the first visit then set up
			if (route.visits.size() == 0) {
				if(minimiseFactor.equals("Cost")){
					tMode = "Car";
					pointOfModeSwapping = 1;
				}else if(minimiseFactor.equals("Emissions")){
					tMode = "Public Transport";
					pointOfModeSwapping = 0;
				}
				
			} else {
				// Find the point of mode swapping
				pointOfModeSwapping = route.visits.size();
				for(int i = 0; i < route.visits.size(); i++){
					if(route.visits.get(i).transport.equals("Public Transport") || route.visits.get(i).transport.equals("Car Share")){
						pointOfModeSwapping = i;
						break;
					}
				}
				// If position is greater or less than pointOfModeSwapping 
				// then we have no choice on what transport mode can be used
				if(position > pointOfModeSwapping){
					tMode = "Public Transport";
				}else if(position < pointOfModeSwapping){
					tMode = "Car";
					
				// If position equals pointOfModeSwapping then we decide depending on what is the cheapest
				}else{
					v.transport = "Car";
					double carCost = costForAddingAt(v, position);
					v.transport = "Public Transport";
					double ptCost = costForAddingAt(v, position);
					if (ptCost < carCost) {
						tMode = "Car";
					}else{
						tMode = "Public Transport";
					}
					v.transport = startingMode;
				}
			}
		}
		return tMode;
	}
	
	// Used to determine if a visit can be added at a specific location within
	// the current route
	public boolean canAddAt(VisitData newVisit, int position) {
		if(transportMode.equals("Mixed")){
			if(pointOfModeSwapping < position){
				newVisit.transport = "Public Transport";
			}else{
				newVisit.transport = "Car";
			}
		}else{
			newVisit.transport = transportMode;
		}
				
		Route tempRoute = new Route();
		Iterator<VisitData> it = route.visits.iterator();
		int pos = 0;
		while (it.hasNext()) {
			tempRoute.visits.add(it.next());
			pos++;
			if (pos == position){
				tempRoute.visits.add(newVisit);
			}
		}
		if (tempRoute.visits.size() == 0) {
			tempRoute.visits.add(newVisit);
		}
		
		
		// Create a calendar and set the time to track the time that it would be
		// relative to the visits
		Calendar cal = Calendar.getInstance();
		cal.setTime(depot.commenceTime);//cal.setTime(tempRoute.visits.firstElement().windowStart);
		cal.add(Calendar.MINUTE, (int) journeyInfo.getTravelTime(myAgent, depot.location, tempRoute.visits.get(0).location, tempRoute.visits.get(0).transport));
		for(int i = 0; i < tempRoute.visits.size(); i++){
			VisitData vd = tempRoute.visits.get(i);

			/*
			 *  Check if we will arrive before the start time.
			 *  If we will but this is the first visit then set
			 *  the time to the start time of that visit
			 */
			if (cal.getTime().before(vd.windowStart)) {
				if(i == 0){
					cal.setTime(tempRoute.visits.get(0).windowStart);
				}else{
					return false;
				}
			}
						
			// If the windowEnd is before the time that the route will be at
			// then this is not a valid merge
			if (cal.getTime().after(vd.windowEnd)) {
				return false;
			}
			
			// Add the time this visit takes
			cal.add(Calendar.MINUTE, (int) vd.visitLength);
			if(vd instanceof CarShare){
				cal.add(Calendar.MINUTE, (int) journeyInfo.getTravelTime(myAgent, ((CarShare) vd).endLocation, (i+1 == tempRoute.visits.size() ? depot.location : tempRoute.visits.get(i+1).location), vd.transport));	
			}else{
				cal.add(Calendar.MINUTE, (int) journeyInfo.getTravelTime(myAgent, vd.location, (i+1 == tempRoute.visits.size() ? depot.location : tempRoute.visits.get(i+1).location), vd.transport));	
			}		
		}
		return true;
	}
	
	
	/**
	 * @param from The location the journey starts at
	 * @param to The location that the journey ends at
	 * @return The cost of travelling between from and to
	 */
	public double getJourneyCost(String from, String to, String minimiseFactor, String transportMode) {
		JourneyData journeyData = this.journeyInfo.getJourneyData(myAgent, from, to);
		if (journeyData != null) {
			if(transportMode.equals("Car")){
				if(minimiseFactor.equals("Emissions")){
					return journeyData.carEm;
				}else if(minimiseFactor.equals("Cost")){
					return (journeyData.carDist * costVars.carCostsPerKM) + ((journeyData.carTime/60.0d) * costVars.staffCostPerHour);
				}
			}else if(transportMode.equals("Public Transport")){
				if(minimiseFactor.equals("Emissions")){
					return journeyData.ptEm;
				}else if(minimiseFactor.equals("Cost")){
					return (journeyData.ptTime/60.0d) * costVars.staffCostPerHour;
				}
			}else if(transportMode.equals("Car Share")){
				if(minimiseFactor.equals("Emissions")){
					return 0;
				}else if(minimiseFactor.equals("Cost")){
					return ((journeyData.carTime/60.0d) * costVars.staffCostPerHour);
				}
			}
		}
		return 0;
	}
	
	/**
	 * Determines if a change can be made without affecting any CarShares
	 * @param pos The position that a visit will be added at
	 * @return Whether or not this change is allowed
	 */
	public boolean canChangeWithoutBreakingCarShare(int pos){
		for(int i = route.visits.size() - 1; i > pos; i--){
			if(route.visits.get(i).sharingWith.size() > 0 || route.visits.get(i) instanceof CarShare){
				return false;
			}
		}
		return true;
	}

	/**
	 * Handles the processing required in making a bid
	 * @param cfp The ACLMessage containing the call for proposal from the auctioneer
	 */
	private void bidForVisit(ACLMessage cfp) {
		try {
			ContentElement d = myAgent.getContentManager().extractContent(cfp);
			if (d instanceof GiveObjectPredicate) {
				VisitData visitData = (VisitData) ((GiveObjectPredicate) d).getData();
				
				// Get bid
				double bid = Double.MIN_VALUE;
				boolean canAllocate = canAllocateToRoute(visitData);
				if (canAllocate) {
						bid = getBid(visitData);
				}
				
				// Send reply
				ACLMessage reply = cfp.createReply();
				if(bid != Double.MIN_VALUE){
					if (canAllocate) {
						reply.setPerformative(ACLMessage.PROPOSE);
					} else {
						// If there were any car shares being setup then remove
						carShare = null;
						// Request a visit for returning
						returningVisit = getVisitToReturn(visitData);
						if(returningVisit != null){
							// If there is one then store where it was in the route
							indexOfReturningVisit = route.visits.indexOf(returningVisit);
							// Then remove it
							route.visits.remove(returningVisit);
							
							// Determine if this is a valid bid
							if(canAllocateToRoute(visitData)){
								reply.setPerformative(ACLMessage.PROPOSE);
							}else{
								reply.setPerformative(ACLMessage.REFUSE);
							}
						}else{
							reply.setPerformative(ACLMessage.REFUSE);
							if(isCarShareEnabled && route.visits.size() > 2){
								offerToCarShare();
							}
						}
						
					}
				}else{
					reply.setPerformative(ACLMessage.REFUSE);
				}
				
				BidResponse replyContent = new BidResponse();
				replyContent.setBid(bid);
				replyContent.setBidder(myAgent.getAID());
				replyContent.setResponsePerformative(reply.getPerformative());
				replyContent.setReturningVisit(returningVisit);
				
				// Add the BidResponse to the message and send it
				cfp.setLanguage(codec.getName());
				cfp.setOntology(ontologyGive.getName());
				try {
					GiveObjectPredicate give = new GiveObjectPredicate();
					give.setData(replyContent);			
					myAgent.getContentManager().fillContent(reply, give);
					myAgent.send(reply);					
				} catch (CodecException e) {
					e.printStackTrace();
				} catch (OntologyException e) {
					e.printStackTrace();
				}
			}
		} catch (UngroundedException e) {
			e.printStackTrace();
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return The results of the carShare message
	 */
	public CarShareResult getCarShareResults(){
		CarShareResult result = null;
		// Read the results from the msg content
		try {
			ContentElement d = myAgent.getContentManager().extractContent(carShare);
			if (d instanceof GiveObjectPredicate) {
				result = (CarShareResult) ((GiveObjectPredicate) d).getData();
			}
		} catch (UngroundedException e) {
			e.printStackTrace();
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Informs the CarShareServer that this bidder is willing to car share
	 */
	protected void offerToCarShare(){
		ACLMessage offer = new ACLMessage(ACLMessage.INFORM);
		offer.addReceiver(carShareServerAID);
		offer.setConversationId("car-share-offer");
		offer.setLanguage(codec.getName());
		offer.setOntology(ontologyGive.getName());
		try {
			GiveObjectPredicate give = new GiveObjectPredicate();
			CarShareOffer carShareOffer = new CarShareOffer();
			carShareOffer.setRoute(route);
			give.setData(carShareOffer);
			myAgent.getContentManager().fillContent(offer, give);
			myAgent.send(offer);
			
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Informs the CarShareServer that this agent wants to stop 
	 * car sharing.
	 */
	public void stopCarSharing(){
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.addReceiver(carShareServerAID);
		msg.setConversationId("car-share-remove");
		myAgent.send(msg);
	}
	
	/**
	 * @return The total time the route should take
	 */
	protected double calcTotalTime(){
		double totalTime = 0.0d;
		VisitData prev = null;
		Iterator<VisitData> it = route.visits.iterator();
		while(it.hasNext()){
			VisitData v = it.next();
			
			if (prev == null) {
				totalTime += journeyInfo.getTravelTime(myAgent, depot.location, v.location, v.transport);
			}else{
				totalTime += journeyInfo.getTravelTime(myAgent, (prev instanceof CarShare ? ((CarShare)prev).endLocation : prev.location), v.location, v.transport);
			}
			
			if (!it.hasNext()) {
				totalTime += journeyInfo.getTravelTime(myAgent, (v instanceof CarShare ? ((CarShare)v).endLocation : v.location), depot.location, v.transport);
			}
			totalTime += v.visitLength;
			prev = v;
		}
		return totalTime;
	}
	
	/**
	 * @return The total emissions of the route
	 */
	protected double calcTotalEmissions(){
		double totalEmissions = 0.0d;
		int i = 0;
		Iterator<VisitData> it = route.visits.iterator();
		while(it.hasNext()){
			VisitData v = it.next();
			
			if(i == 0){
				totalEmissions += getJourneyCost(depot.location, v.location, "Emissions", v.transport);
				
			}else{
				if(route.visits.get(i-1) instanceof CarShare){
					totalEmissions += getJourneyCost(((CarShare)route.visits.get(i-1)).endLocation, v.location, "Emissions", v.transport);
				}else{
					totalEmissions += getJourneyCost(route.visits.get(i-1).location, v.location, "Emissions", v.transport);
				}
				
			}
			if(i == route.visits.size()){
				if(route.visits.get(i-1) instanceof CarShare){
					totalEmissions += getJourneyCost(((CarShare)v).endLocation, depot.location, "Emissions", v.transport);
				}else{
					totalEmissions += getJourneyCost(v.location, depot.location, "Emissions", v.transport);
				}
			}
			i++;
		}
		return totalEmissions;
	}
	
	/**
	 * @return The total distance of the route
	 */
	protected double calcTotalDist(){
		double totalDist = 0.0d;
		VisitData prev = null;
		Iterator<VisitData> it = route.visits.iterator();
		while(it.hasNext()){
			VisitData v = it.next();
						
			if(prev == null){
				totalDist += journeyInfo.getDistance(myAgent, depot.location, v.location, v.transport);				
			}else{
				totalDist += journeyInfo.getDistance(myAgent, (prev instanceof CarShare ? ((CarShare)prev).endLocation : prev.location), v.location, v.transport);				
			}
			
			if(!it.hasNext()){
				totalDist += journeyInfo.getDistance(myAgent, (v instanceof CarShare ? ((CarShare)v).endLocation : v.location), depot.location, v.transport);
			}
			prev = v;
		}
		return totalDist;
	}

	/**
	 * @return The total cost of the route
	 */
	protected double calcTotalCost(){
		double moneyCost = 0.0d;
		
		boolean usedCar = false;
		boolean usedPT = false;
		
		for(int i = 0; i < route.visits.size(); i++){
			VisitData v = route.visits.get(i);
			
			double time = 0.0d, dist = 0.0d;
			
			// Keep track of car and pt usage
			if(v.transport.equals("Car")){
				usedCar = true;
			}else if(v.transport.equals("Public Transport")){
				usedPT = true;
			}
			
			if (i == 0) {
				time += journeyInfo.getTravelTime(myAgent, depot.location, v.location, v.transport);

			} else {
				if(route.visits.get(i-1) instanceof CarShare){
					time += journeyInfo.getTravelTime(myAgent, ((CarShare)route.visits.get(i - 1)).endLocation, v.location, v.transport);
				}else{
					time += journeyInfo.getTravelTime(myAgent, route.visits.get(i - 1).location, v.location, v.transport);
				}

			}
			if (i == route.visits.size()) {
				if(route.visits.get(i-1) instanceof CarShare){
					time += journeyInfo.getTravelTime(myAgent, ((CarShare)v).endLocation, depot.location, v.transport);
				}else{
					time += journeyInfo.getTravelTime(myAgent, v.location, depot.location, v.transport);
				}
			}
			time += v.visitLength;
			
			// Get the distance for this visit
			if(i == 0){
				dist += journeyInfo.getDistance(myAgent, depot.location, v.location, v.transport);
				
			}else{
				if(route.visits.get(i-1) instanceof CarShare){
					dist += journeyInfo.getDistance(myAgent, ((CarShare)route.visits.get(i - 1)).endLocation, v.location, v.transport);
				}else{
					dist += journeyInfo.getDistance(myAgent, route.visits.get(i-1).location, v.location, v.transport);
				}
				
			}
			if(i == route.visits.size()){
				if(route.visits.get(i-1) instanceof CarShare){
					dist += journeyInfo.getDistance(myAgent, ((CarShare)v).endLocation, depot.location, v.transport);
				}else{
					dist += journeyInfo.getDistance(myAgent, v.location, depot.location, v.transport);
				}
			}
			
			// Calc the cost
			moneyCost +=  (costVars.staffCostPerHour * (time/60.0d))
						+ (v.transport.equals("Car") ? costVars.carCostsPerKM * dist : 0);
		}
		
		// If a car was used, add the fixed cost
		if(usedCar)
			moneyCost += costVars.fixedCostsCar;

		// If pt was used, add the fixed cost
		if(usedPT)
			moneyCost += costVars.fixedCostsPt;
		
		// Return the cost
		return moneyCost;
	}	
	
	// Allows for a custom class for checking time constraints and sending requests to the MapServer
	public void setJourneyInfoHelper(JourneyInfoHelper newHelper){
		this.journeyInfo = newHelper;
	}
	
	
	// Handle the process at each step of its execution
	@Override
	public void action() {
		switch (currStep) {
		case PreInit:
			// Register the Ontology for message content
			myAgent.getContentManager().registerLanguage(codec);
			myAgent.getContentManager().registerOntology(ontologyGive);
			
			// Check for an argument stating whether or not this agent will be reusable
            if(myAgent.getArguments() != null && myAgent.getArguments().length >= 1){
            	isReusable = (Boolean) myAgent.getArguments()[0];                
            }

			ACLMessage argMsg = myAgent.blockingReceive(MessageTemplate.MatchConversationId("bidder-arguments"));
			
			// Store the Auctioneer's AID
			auctioneerAID = argMsg.getSender();
			
			String[] args = argMsg.getContent().split(",");			
			if(args.length >= 4){
				returnsLeft = Integer.valueOf(args[0]);
				minimiseFactor = args[1];
				transportMode = args[2];
				isCarShareEnabled = Boolean.valueOf(args[3]);
				if(transportMode == "Mixed"){
					pointOfModeSwapping = route.visits.size();
				}else{
					isCarShareEnabled = false;
				}
			}
			
			currStep = STEP.Initialise;
			break;
		case Initialise:
			if (initialise()) {
				ACLMessage depotMsg = myAgent.blockingReceive(MessageTemplate.MatchConversationId("depot-setup"));
				try {
					ContentElement d = myAgent.getContentManager().extractContent(depotMsg);
					if (d instanceof GiveObjectPredicate) {
						depot = (Depot) ((GiveObjectPredicate) d).getData();
					}
				} catch (UngroundedException e) {
					e.printStackTrace();
				} catch (CodecException e) {
					e.printStackTrace();
				} catch (OntologyException e) {
					e.printStackTrace();
				}
				
				costVars = this.journeyInfo.getCostVariables(myAgent);

				if(journeyInfo == null)
					journeyInfo = new JourneyInfoHelper();

				carShareServerAID = getCarShareServerAID();
				
				currStep = STEP.HandleBids;

				// Set my route's owner to me
				route.setRouteOwner(myAgent.getAID());
			}
			break;

		case HandleBids:
			ACLMessage msg = myAgent.receive(
					MessageTemplate.or(MessageTemplate.or(bidTemplate, carShareAcceptTemplate), MessageTemplate.or(wonBidTemplate, biddingFinishedTemplate)));
			if (msg != null) {
				
				// Bid for the VisitData
				if (bidTemplate.match(msg)) {
					// If returningVisit isn't null then we didn't win, add it back to the route
					if(returningVisit != null){
						route.visits.add(indexOfReturningVisit, returningVisit);
					}
					returningVisit = null;
					carShare = null;
					bidForVisit(msg);

				// Handle a winning bid
				} else if (wonBidTemplate.match(msg)) {
					try {
						ContentElement d = myAgent.getContentManager().extractContent(msg);
						if (d instanceof GiveObjectPredicate) {
							VisitData visitData = (VisitData) ((GiveObjectPredicate) d).getData();
							if(returningVisit != null){
								// Set the returningVisit to null
								returningVisit = null;
								// Decrement how many visits we can still return
								returnsLeft--;
							}
							handleWonBid(visitData);
							if(isCarShareEnabled)
								offerToCarShare();
						}
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}

					// The auctioneer has closed the bidding, show your winnings
				} else if (biddingFinishedTemplate.match(msg)) {
					// If returningVisit isn't null we need to add it back before finishing
					if(returningVisit != null){
						route.visits.add(indexOfReturningVisit, returningVisit);
						returningVisit = null;
					}
					
					// Stop offering to Car Share
					if(isCarShareEnabled){
						stopCarSharing();
					}

					// Return to the car if we need to
					if(route.visits.size() > 0 && route.visits.get(route.visits.size()-1).transport.equals("Public Transport")){
						ReturnToCar returnToCar = new ReturnToCar();
						returnToCar.location = null;
						// Find the car
						for(int i = 0; i < route.visits.size(); i++){
							VisitData v = route.visits.get(i);
							if(v.transport.equals("Car")){
								returnToCar.location = v.location;
							}else{
								break;
							}
						}
						if(returnToCar.location != null){
							returnToCar.name = "Returning to Car";
							returnToCar.visitLength = 0;
							returnToCar.transport = "Public Transport";
							
							returnToCar.windowStart = depot.commenceTime;
							// Change windowEnd to a date really far in the future (one that doesn't crash JADE's parser)
							returnToCar.windowEnd = depot.commenceTime;
							
							// Add returning to the car to the list
							route.visits.add(returnToCar);
						}
					}
					
					ACLMessage completionMsg = msg.createReply();
					completionMsg.setLanguage(codec.getName());
					completionMsg.setOntology(ontologyGive.getName());
					try {
						GiveObjectPredicate give = new GiveObjectPredicate();
						route.setTotalEmissions(calcTotalEmissions());
						
						// Round the time up to the nearest hour
						double totalTime = calcTotalTime();
						//totalTime = (int)(totalTime - totalTime % 60.0d + 60.0d);
						route.setTotalTime(totalTime);
						
						route.setTotalDist(calcTotalDist());						
						route.setTotalCost(calcTotalCost());

						give.setData(route);
						myAgent.getContentManager().fillContent(completionMsg, give);
						myAgent.send(completionMsg);
						currStep = STEP.FinishUp;
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}

				}else if(carShareAcceptTemplate.match(msg)){
					// Read the Car Share info from the msg
					try {
						ContentElement d = myAgent.getContentManager().extractContent(msg);
						if (d instanceof GiveObjectPredicate) {
							CarShareResult results = (CarShareResult) ((GiveObjectPredicate) d).getData();
							for(VisitData resV : results.carShare.visits){
								for(VisitData v : route.visits){
									if(v.name.equals(resV.name)){
										v.sharingWith.add(results.shareWith);
									}
								}
							}
						}
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
					
				}

			} else {
				block();
			}
			break;

		case FinishUp:
			if (finishUp()){
				stopCarSharing();
				if(isReusable){
					currStep = STEP.PreInit;
					resetBidder();
				}else{
					myAgent.doDelete();
					isDone = true;
				}
			}
			break;
		}
	}

	@Override
	public boolean done() {
		return isDone;
	}
	
}