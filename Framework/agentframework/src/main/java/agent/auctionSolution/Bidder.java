package agent.auctionSolution;

import java.util.Calendar;
import java.util.Iterator;

import agent.auctionSolution.bidder.BidderBehaviour;
import agent.auctionSolution.dataObjects.VisitData;
import agent.auctionSolution.dataObjects.carShare.CarShare;
import agent.auctionSolution.dataObjects.carShare.CarShareRequest;
import agent.auctionSolution.dataObjects.carShare.CarShareResult;
import agent.auctionSolution.ontologies.GiveObjectPredicate;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Executes the BidderBehaviour inner class that handles
 * the bidding of visits.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class Bidder extends Agent{

	
	protected void setup(){
		addBehaviour(new MyBidderBehaviour());
	}

	private class MyBidderBehaviour extends BidderBehaviour{

		private int addAt = -1;

		@Override
		protected boolean initialise() {
			setJourneyInfoHelper(new JourneyInfoHelper());
			return true;
		}

		@Override
		public double getBid(VisitData v) {
			// Determine what transport mode should be used for v
			v.transport = determineTransportMode(v, addAt);
			// Store the value of the bid
			double bid = costForAddingAt(v, addAt);
			
			// If car sharing is enabled
			if(isCarShareEnabled){
				// Store the car share bid
				double carShareBid = getCarShareBid(v, addAt);
				
				// If it is better than the current bid then use the CarShare bid
				if(carShareBid >= bid && carShare != null){
					CarShare cs = getCarShareResults().carShare;
					route.visits.add(addAt, cs);
					if(this.canAddAt(v, addAt+1)){
						bid = carShareBid;
					}else{
						carShare = null;
					}
					route.visits.remove(cs);
				}else{
					carShare = null;
				}
			}
			
			return bid;
		}

		/**
		 * Send a request to the CarShareServer for a CarShare
		 * @param v
		 * @param pos
		 * @return
		 */
		private double getCarShareBid(VisitData v, int pos){
			this.carShare = null;
			
			// Create the request
			CarShareRequest request = new CarShareRequest();
			request.from = (pos-1 <= 0) ? depot.location : route.visits.get(pos-1).location;
			request.to = v.location;
			request.transportMode = v.transport;
			
			if(pos-1 > 0 && route.visits.get(pos-1) instanceof CarShare)
				return Double.MIN_VALUE;
			
			request.route = route;

			// Send the request
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			msg.addReceiver(carShareServerAID);
			msg.setPerformative(ACLMessage.PROPOSE);
			msg.setConversationId("car-share-request");
			msg.setLanguage(codec.getName());
			msg.setOntology(ontologyGive.getName());
			try {
				GiveObjectPredicate give = new GiveObjectPredicate();
				give.setData(request);
				myAgent.getContentManager().fillContent(msg, give);
				myAgent.send(msg);
			} catch (CodecException e) {
				e.printStackTrace();
			} catch (OntologyException e) {
				e.printStackTrace();
			}
			
			// Wait for the response
			ACLMessage response = myAgent.blockingReceive(MessageTemplate.MatchConversationId("car-share-proposal"));
			carShare = response;
			CarShareResult result = getCarShareResults();
			
			// Determine the cost of adding the CarShare if there is one
			if(result != null){
				double currentCost = getJourneyCost(pos-1 < 0 ? depot.location : route.visits.get(pos-1).location, (pos == route.visits.size() ? depot.location : route.visits.get(pos).location), minimiseFactor, v.transport);
				double cost = (minimiseFactor.equals("Emissions") ? 0 : (costVars.staffCostPerHour * (result.carShare.visitLength / 60.0d)));
				cost += getJourneyCost((pos-1 < 0) ? depot.location : route.visits.get(pos-1).location, result.carShare.location, minimiseFactor, v.transport);
				cost += getJourneyCost(result.carShare.endLocation, v.location, minimiseFactor, "Public Transport");
				
				return (currentCost - (cost) + (minimiseFactor.equals("Emissions") ? 0 : (costVars.staffCostPerHour * (v.visitLength / 60.0d))) + (result.carShare.visitLength/60.0d));
			}else{
				carShare = null;
				return Double.MIN_VALUE;
			}			
		}
		
		/**
		 * @param v The visit being added
		 * @param position The position in the route to add it
		 * @return The cost associated with adding v at position in route.visits
		 */
		@Override
		public double costForAddingAt(VisitData v, int position){
			if(route.visits.size() == 0){
				return getJourneyCost(depot.location, v.location, minimiseFactor, v.transport) + getJourneyCost(v.location, depot.location, minimiseFactor, v.transport);
				
			}else if(position == route.visits.size()){
				double cost0 = getJourneyCost(route.visits.get(position-1).location, v.location, minimiseFactor, v.transport);
				double cost1 = getJourneyCost(v.location, depot.location, minimiseFactor, v.transport);
				double currentCost = getJourneyCost(route.visits.get(position-1).location, depot.location, minimiseFactor, v.transport);
				
				return -((cost0 + cost1) - currentCost);
				
			}else{
				double cost0 = getJourneyCost(position-1 < 0 ? depot.location : route.visits.get(position-1).location, v.location, minimiseFactor, v.transport);
				double cost1 = getJourneyCost(v.location, route.visits.get(position).location, minimiseFactor, v.transport);
				double currentCost = getJourneyCost(position-1 < 0 ? depot.location : route.visits.get(position-1).location, route.visits.get(position).location, minimiseFactor, v.transport);
				
				return -((cost0 + cost1) - currentCost);
			}
		}		
		
		@Override
		public boolean canAllocateToRoute(VisitData v) {
			double bestCost = Double.MAX_VALUE;
			addAt = -1;
			int i = 0;
			do{
				v.transport = determineTransportMode(v, i);
				if(canAddAt(v, i)){
					double cost = costForAddingAt(v, i);
					if(addAt == -1 || -cost < bestCost){
						addAt = i;
						bestCost = -cost;
					}
				}
				i++;
			}while(i <= route.visits.size());
			
			// If CarSharing is enabled then check if it is possible to add
			if(isCarShareEnabled){
				return (addAt != -1 && canChangeWithoutBreakingCarShare(addAt));
			}else{
				return addAt != -1;
			}
		}

		@Override
		public void handleWonBid(VisitData v) {			
			if(carShare != null){
				// Inform the CarShareServer that you accept the CarShare
				CarShareResult result = getCarShareResults();
				ACLMessage accept = carShare.createReply();
				accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
				accept.setLanguage(codec.getName());
				accept.setOntology(ontologyGive.getName());
				try {
					GiveObjectPredicate give = new GiveObjectPredicate();
					give.setData(result);
					myAgent.getContentManager().fillContent(accept, give);
					send(accept);

				} catch (CodecException e) {
					e.printStackTrace();
				} catch (OntologyException e) {
					e.printStackTrace();
				}
				// Add the CarShare and the visit won
				result.carShare.transport = determineTransportMode(result.carShare, addAt);
				carShare = null;
				route.visits.add(addAt, result.carShare);
				v.transport = "Public Transport";
				route.visits.add(addAt + 1, v);
				System.out.println("CARSHARE");
			}else{
				// Determine the transport mode and add the visit
				v.transport = determineTransportMode(v, addAt);
				route.visits.add(addAt, v);
			}			
		}

		@Override
		public boolean finishUp() {
			return true;
		}

		@Override
		public VisitData getVisitToReturn(VisitData visitData) {
			if (returnsLeft > 0) {
				double deltaEm = 0.0d;
				double deltaTime = 0.0d;
				VisitData bestVisit = null;
				
				// Store the current emission and time cost
				double currentEmissions = calcTotalEmissions();
				double currentTime = calcTotalTime();
				
				// Loop through every visit and check if an improvement can
				// be made by returning a visit and replacing it with visitData
				for(int i = 0; i < route.visits.size(); i++){
					if(route.visits.get(i) instanceof CarShare || route.visits.get(i).sharingWith.size() > 0 || !canChangeWithoutBreakingCarShare(i))
						continue;
					
					VisitData v = route.visits.remove(i);

					if(canAddAt(visitData, i)){
						route.visits.add(i, visitData);
						double newEmissions = calcTotalEmissions();
						double newTime = calcTotalTime();
						route.visits.remove(visitData);
						
						if(currentEmissions - newEmissions >= deltaEm && currentTime - newTime >= deltaTime){
							deltaEm = currentEmissions - newEmissions;
							deltaTime = currentTime - newTime;
							bestVisit = v;
						}
					}
					
					route.visits.add(i, v);
				}
				
				// If we have a valid visit to return then return it
				int index = route.visits.indexOf(bestVisit);
				if(!isCarShareEnabled || (index != -1 && canChangeWithoutBreakingCarShare(index))){
					return bestVisit;
				}else{
					return null;
				}
				
			}
			return null;
		}
		
	
	}
}
