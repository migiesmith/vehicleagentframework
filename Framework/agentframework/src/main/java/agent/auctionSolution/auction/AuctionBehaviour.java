package agent.auctionSolution.auction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import agent.auctionSolution.dataObjects.BidResponse;
import agent.auctionSolution.dataObjects.Depot;
import agent.auctionSolution.dataObjects.Route;
import agent.auctionSolution.dataObjects.Transaction;
import agent.auctionSolution.dataObjects.VisitData;
import agent.auctionSolution.ontologies.GiveObjectPredicate;
import agent.auctionSolution.ontologies.GiveOntology;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * The behaviour that distributes a list of visits between
 * bidders. Once they have all been distributed, a request
 * is sent to the bidders for their resulting routes.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public abstract class AuctionBehaviour extends Behaviour{

	// Initialise the language for message contents
	private Codec codec = new SLCodec();
	private Ontology ontologyGive = GiveOntology.getInstance();
	
	// Templates for the incoming messages
	protected MessageTemplate bidTemplate, biddingFinishedTemplate;
	
	private List<Transaction> transactions = new ArrayList<Transaction>();
	
	// Vector of visits to distribute
	private List<VisitData> visits;
	// The current visit that is being auctioned
	private VisitData upForAuction = null;
	
	// The depot of the problem
	private Depot depot;
	
	// The solution to the bidding
	private List<Route> solution;
	
	// Vector of bidders AIDs
	private List<AID> bidders;
	protected List<BidResponse> responses = new ArrayList<BidResponse>();
	
	// Whether or not this behaviour has finished
	private boolean isDone = false;
	
	// Enum for the steps this behaviour can be in
	private enum STEP{
		AddLanguages,
		Initialise,
		AuctionVisit,
		WaitForResponse,
		FinishUp
	}
	
	// The current step this behaviour is on
	private STEP currStep = STEP.AddLanguages;
	
	private int resellCount;
	
	
	protected AuctionBehaviour(List<VisitData> visits, List<AID> bidders, Depot depot){
		this.visits = copyConentsOfVisitList(visits);
		this.bidders = bidders;
		this.depot = depot;
		
		// Set the limit to reselling visits
		resellCount = this.visits.size();
		
		// Create the message templates
		bidTemplate = MessageTemplate.MatchConversationId("visit-bid");
		biddingFinishedTemplate = MessageTemplate.MatchConversationId("bidding-finished");
	}

	// Creates a new Vector<VisitData> with contents matching the one passed in (prevents modification to the original)
	protected List<VisitData> copyConentsOfVisitList(List<VisitData> visits){
		List<VisitData> returnVal = new ArrayList<VisitData>();

		for (VisitData visit : visits) {
			VisitData newVisit = new VisitData();
			newVisit.setAll(visit.name, visit.location, visit.windowStart, visit.windowEnd, visit.visitLength, visit.x,
					visit.y, visit.transport);
			returnVal.add(newVisit);
		}
		return returnVal;
	}
	
	// Returns a reference to the visit that is up for auction
	protected VisitData getUpForAuction(){
		return upForAuction;
	}

	// abstract methods that must be implemented //	
	/**
	 * Execute any inital code that is needed
	 * @return
	 */
	protected abstract boolean initialise();

	/**
	 * Execute anything before ending the behaviour
	 * @return
	 */
	protected abstract boolean finishUp();
	
	protected List<VisitData> getVisits(){
		return visits;
	}
	
	// Returns the solution to the problem
	public List<Route> getSolution(){
		return solution;
	}
	
	// Returns the transactions made during the problem
	public List<Transaction> getTransactions(){
		return transactions;
	}
	
	// Try to auction off a visit
	protected boolean auctionVisit(){
		if(visits.isEmpty())
			return false;
		
		// Get a new visit to auction
		upForAuction = visits.remove(0);
		
		// Create a Call For Proposal
		ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
		// Add all of the receivers
		for(AID bidder : bidders){
			cfp.addReceiver(bidder);
		}
		cfp.setConversationId("visit-bid");
		
		// Add the visit to the message and send it
		cfp.setLanguage(codec.getName());
		cfp.setOntology(ontologyGive.getName());
		try {
			GiveObjectPredicate give = new GiveObjectPredicate();
			give.setData(upForAuction);			
			myAgent.getContentManager().fillContent(cfp, give);
			myAgent.send(cfp);					
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}

		// Clear the responses list
		responses.clear();
		return true;
	}
	
	// Wait and process any received bids
	protected boolean waitForBids(){
		ACLMessage bid = myAgent.receive(bidTemplate);
		if(bid != null){ // Read in the bid
			try {
				ContentElement d = myAgent.getContentManager().extractContent(bid);
				if (d instanceof GiveObjectPredicate) {
					// Add the bid to the list of responses
					responses.add((BidResponse) ((GiveObjectPredicate) d).getData());
				}
			} catch (UngroundedException e) {
				e.printStackTrace();
			} catch (CodecException e) {
				e.printStackTrace();
			} catch (OntologyException e) {
				e.printStackTrace();
			}
		}else{
			block();
		}
		
		// If all of the responses have been received then send to the winner
		if(responses.size() == bidders.size()){
			handleCompletedBid();
			return true;
		}
		
		return false;
	}
	
	// Go through the list of responses and return the AID of the highest bidder
	protected AID getHighestBidder(){
		AID highestBidder = null;
		double highestBid = Double.MIN_VALUE;
		VisitData returningVisit = null;
		Iterator<BidResponse> it = responses.iterator();
		while(it.hasNext()){
			BidResponse response = it.next();
			// Only process Proposals
			if(response.getResponsePerformative() == ACLMessage.PROPOSE){
				if(response.getBid() >= highestBid || highestBidder == null){
					highestBid = response.getBid();
					highestBidder = response.getBidder();
					returningVisit = response.getReturningVisit();
				}					
			}
		}
		if(returningVisit != null){
			visits.add(returningVisit);			
			// Store the transaction for the transaction log
			Transaction t = new Transaction();
			t.setSender(highestBidder);
			t.setReceiver(myAgent.getAID());
			t.setVisitData(returningVisit);
			transactions.add(t);
		}
		return highestBidder;
	}
	
	// Send upForAuction to the highest bidder
	protected void handleCompletedBid(){
		ACLMessage msg = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
		msg.setConversationId("visit-won");
		AID winner = getHighestBidder();
		msg.addReceiver(winner);
		
		// Add the visit to the message and send it
		msg.setLanguage(codec.getName());
		msg.setOntology(ontologyGive.getName());
		try {
			GiveObjectPredicate give = new GiveObjectPredicate();
			give.setData(upForAuction);
			myAgent.getContentManager().fillContent(msg, give);
			myAgent.send(msg);					
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
		
		if(winner == null && resellCount >= 0){
			visits.add(upForAuction);
			resellCount--;
		}

		// Store the transaction for the transaction log
		Transaction t = new Transaction();
		t.setSender(myAgent.getAID());
		t.setReceiver(winner);
		t.setVisitData(upForAuction);
		transactions.add(t);
		
		upForAuction = null;
	}
	
	/* 
	 * Send a message to inform each bidder that the
	 * bidding is over requesting them for their routes.
	 */
	protected void biddingFinished(){
		// Create a Request for each bidder's route
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		// Add all of the receivers
		for(AID bidder : bidders){
			msg.addReceiver(bidder);
		}
		msg.setConversationId("bidding-finished");
		myAgent.send(msg);
		
		solution = new ArrayList<Route>();
	}
	
	// Handle the process at each step of its execution
	@Override
	public void action() {
		switch(currStep){
		case AddLanguages:
			// Register the Ontology for message content
			myAgent.getContentManager().registerLanguage(codec);
			myAgent.getContentManager().registerOntology(ontologyGive);
			currStep = STEP.Initialise;
			break;
			
		case Initialise:
			if(initialise()){
				// Send the depot information to each bidder
				ACLMessage depotMsg = new ACLMessage(ACLMessage.INFORM);
				// Add all of the receivers
				for(AID bidder : bidders){
					depotMsg.addReceiver(bidder);
				}
				depotMsg.addReceiver(new AID("car-share-server", AID.ISLOCALNAME));
				depotMsg.setConversationId("depot-setup");
				depotMsg.setLanguage(codec.getName());
				depotMsg.setOntology(ontologyGive.getName());
				try {
					GiveObjectPredicate give = new GiveObjectPredicate();
					give.setData(depot);
					myAgent.getContentManager().fillContent(depotMsg, give);
					myAgent.send(depotMsg);
				} catch (CodecException e) {
					e.printStackTrace();
				} catch (OntologyException e) {
					e.printStackTrace();
				}
				currStep = STEP.AuctionVisit;
			}
			break;
			
		case AuctionVisit:
			// Auction off a visit
			if(auctionVisit())
				currStep = STEP.WaitForResponse;
			break;

		case WaitForResponse:
			// Wait for responses from the bidders
			if(waitForBids()){
				if(visits.size() == 0){
					// If there are no more visits to send then finish up
					biddingFinished();
					currStep = STEP.FinishUp;
				}else{
					currStep = STEP.AuctionVisit;
				}
			}
			break;
			
		case FinishUp:
			// All routes have been received, try to finish
			if(solution.size() == bidders.size()){	
					isDone = finishUp();
					
			}else{ // Accept the results from the bidders
				ACLMessage msg = myAgent.receive(biddingFinishedTemplate);
				if (msg != null) {
					// Read the Route from the msg content
					try {
						ContentElement d = myAgent.getContentManager().extractContent(msg);
						if (d instanceof GiveObjectPredicate) {
							Route r = (Route) ((GiveObjectPredicate) d).getData();
							solution.add(r);
						}
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
				} else {
					block();
				}
			}
			
			break;			
		}		
	}
	

	@Override
	public boolean done() {
		return isDone;
	}
}
