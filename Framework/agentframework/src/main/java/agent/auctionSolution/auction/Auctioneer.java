package agent.auctionSolution.auction;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import agent.auctionSolution.Bidder;
import agent.auctionSolution.carShare.CarShareServer;
import agent.auctionSolution.dataObjects.Depot;
import agent.auctionSolution.dataObjects.Problem;
import agent.auctionSolution.dataObjects.ReturnToCar;
import agent.auctionSolution.dataObjects.Route;
import agent.auctionSolution.dataObjects.Transaction;
import agent.auctionSolution.dataObjects.VisitData;
import agent.auctionSolution.dataObjects.carShare.CarShare;
import agent.auctionSolution.gui.AuctioneerGui;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.StaleProxyException;

/**
 * Handles the aution side of the system. Most of the
 * processing is done through the AuctionBehaviour inner
 * class. This class at its core supplies the information
 * required to run the inner class.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public abstract class Auctioneer extends Agent {

	// The AID of the bidder agents
	protected List<AID> bidders;

	// The gui for this Auctioneer
	protected AuctioneerGui gui;

	// Used to centre the rendering; x and y for centring, width and height for scaling
	protected Rectangle2D.Double renderOffset = new Rectangle2D.Double(0, 0, 0, 0);

	// The problem files to be loaded
	protected List<String> files = new ArrayList<String>();
	// The number of bidders to be used for each problem
	protected List<Integer> noBidders = new ArrayList<Integer>();
	// Stores the information about the current problem
	protected Problem problem;
	// The index of the current problem
	protected int currentProblem = 0;
	
	// The AuctionBehaviour of this auctioneer
	protected AuctionBehaviour auction;
	
	// The costing value to be kept lowest
	public String minimiseFactor = "NOT_SET";
	// The transport mode to be used
	public String transportMode = "NOT_SET";
	
	protected void setup() {
		System.out.println(getAID().getLocalName() + " has started.");
		
		gui = new AuctioneerGui(this);
		
		try {
			getContainerController().createNewAgent("car-share-server", CarShareServer.class.getCanonicalName(), null).start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}

	public void start() {
		// Return values to default
		auction = null;
		bidders = new ArrayList<AID>();
		renderOffset = new Rectangle2D.Double(0, 0, 0, 0);
			
		createAgents(Bidder.class, 0, noBidders.get(currentProblem));
		
		// If there is already an auction behaviour, remove it
		if(auction != null)
			removeBehaviour(auction);
		// Add a new auction behaviour
		addBehaviour(getAuctionBehaviour());
	}

	// abstract methods that must be implemented //
	
	/**
	 * @return The problem being processed
	 */
	protected abstract Problem getProblem();
	
	/**
	 * @return The AuctionBehaviour to be used by this class
	 */
	protected abstract AuctionBehaviour getAuctionBehaviour();
	
	/**
	 * Loads the next problem from a file
	 */
	public abstract void loadFromFile();
	/**
	 * Sets up a list of problems to be loaded
	 * @param files The list of files paths for the problems
	 * @param noBidders The number of bidders used for each problem 
	 */
	public abstract void loadFromFiles(List<String> files, List<Integer> noBidders);
	
	// Create the specified number of bidders of type bidderClass
	protected void createAgents(Class bidderClass, int startIndex, int noOfAgents) {
		try {
			ACLMessage bidderArgs = new ACLMessage(ACLMessage.INFORM);
			bidderArgs.setConversationId("bidder-arguments");
			for (int i = 0; i < noOfAgents; i++) {
				getContainerController().createNewAgent("Bidder-" + (startIndex+i), bidderClass.getCanonicalName(), new Object[]{false}).start();
				AID bidderAID = new AID("Bidder-" + (startIndex+i), AID.ISLOCALNAME);
				bidders.add(bidderAID);
				bidderArgs.addReceiver(bidderAID);
			}
			// Return limit, minimiseFactor, transportMode, isCarShareEnabled
			bidderArgs.setContent(gui.getReturnableVisits() +","+ minimiseFactor +","+ transportMode +","+ gui.getAllowCarSharing());
			send(bidderArgs);
			
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}
	
	// Returns the x and y coordinates of a visit
	private Point2D.Double requestCoordinates(String location) {
		// Send message for coordinates for the depot
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.setConversationId("visit-coordinates");
		msg.addReceiver(new AID("Map-Server", AID.ISLOCALNAME));
		msg.setContent(location);
		send(msg);
		// Wait for a response
		ACLMessage response = blockingReceive(MessageTemplate.MatchConversationId("visit-coordinates"));
		String[] content = response.getContent().split(",");
		// Store the coordinates
		return new Point2D.Double(Double.parseDouble(content[0]), Double.parseDouble(content[1]));
	}

	// Calculate the render offsets for centring the rendering
	protected void calcRenderOffsets(){
		// For every visit, ask the DataStore for their x and y location
		Point2D.Double visitCoords;
		Iterator<Route> it = getAuctionBehaviour().getSolution().iterator();
		while(it.hasNext()){
			Route route = it.next();
			for(VisitData vd : route.visits){
				// Send message for coordinates
				visitCoords = requestCoordinates(vd.location);
				vd.setX(visitCoords.x);
				vd.setY(visitCoords.y);
				if(vd.transport.equals("Car Share")){
					for(VisitData v : ((CarShare)vd).visits){
						visitCoords = requestCoordinates(v.location);
						v.setX(visitCoords.x);
						v.setY(visitCoords.y);
					}
				}
			}
		}

		// Send message for coordinates
		Point2D.Double depotCoords = requestCoordinates(getProblem().depot.location);
		getProblem().depot.setX(depotCoords.x);
		getProblem().depot.setY(depotCoords.y);
		
		/* 
		 * Calculate the values required to make the results render nicely
		*/				
		double avgX = 0, avgY = 0;
		double maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE, minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;
		it = getAuctionBehaviour().getSolution().iterator();
		int visitCount = 0;
		while(it.hasNext()){
			Route route = it.next();
			for (VisitData vd : route.visits) {
				if (vd.x > maxX)
					maxX = vd.x;
				if (vd.x < minX)
					minX = vd.x;

				if (vd.y > maxY)
					maxY = vd.y;
				if (vd.y < minY)
					minY = vd.y;
				
				avgX += vd.getX();
				avgY += vd.getY();
				visitCount++;
			}
		}

		// Add the depot's values
		if(getProblem().depot.x > maxX)
			maxX = getProblem().depot.x;
		if(getProblem().depot.x < minX)
			minX = getProblem().depot.x;

		if(getProblem().depot.y > maxY)
			maxY = getProblem().depot.y;
		if(getProblem().depot.y < minY)
			minY = getProblem().depot.y;

		
		avgX += getProblem().depot.x;
		avgY += getProblem().depot.y;
		
		// Average results (+1 for the depot)
		avgX /= visitCount+1;
		avgY /= visitCount+1;
		
		// Calc the scaling factor
		double lowerScale = Math.max(maxX - minX, maxY - minY);
		
		// x and y for centring, width and height for scaling
		renderOffset = new Rectangle2D.Double(avgX, avgY, lowerScale, lowerScale);
	}
	
	/* 
	 * Returns the information required to centre the output;
	 * x and y for centring, width and height for scaling
	 */
	public Rectangle2D.Double getRenderOffset(){
		return (Rectangle2D.Double) renderOffset.clone();
	}

	// Returns a copy of the List containing the solution
	public List<List<VisitData>> getSolution() {
		if (getAuctionBehaviour() == null || getAuctionBehaviour().getSolution() == null)
			return null;
		
		List<List<VisitData>> returnVal = new ArrayList<List<VisitData>>();
		
		// Loop through every route in the solution
		for (Route route : getAuctionBehaviour().getSolution()) {
			List<VisitData> newRoute = new ArrayList<VisitData>();
			// Loop through every visit in each route
			for (VisitData visit : route.visits) {
				VisitData newVisit = null;
				// If the visit is a ReturnToCar then create a new ReturnToCar
				if(visit instanceof ReturnToCar){
					newVisit = new ReturnToCar();
				
				// If the visit is a CarShare then create a CarShare
				}else if(visit instanceof CarShare){
					newVisit = new CarShare();

					List<VisitData> newCSRoute = new ArrayList<VisitData>();
					// Loop through each visit in the CarShare and create it
					for (VisitData csVisit : ((CarShare) visit).getVisits()) {
						VisitData newCSVisit = new VisitData();
						newCSVisit.setAll(csVisit.name, csVisit.location, csVisit.windowStart, csVisit.windowEnd, csVisit.visitLength,
								csVisit.x, csVisit.y, csVisit.transport);
						newCSRoute.add(newCSVisit);
					}
					// Set the CarShare's values
					((CarShare)newVisit).setVisits(newCSRoute);
					((CarShare)newVisit).setEndLocation(((CarShare) visit).getEndLocation());
				}else{
					newVisit = new VisitData();
				}
				
				// Set the general VisitData values
				newVisit.setAll(visit.name, visit.location, visit.windowStart, visit.windowEnd, visit.visitLength,
						visit.x, visit.y, visit.transport);
				newRoute.add(newVisit);
			}
			// Add the new route to a list that will be returned
			returnVal.add(newRoute);
		}
		return returnVal;
	}
	
	// Returns a copy of the depot for the problem
	public Depot getDepot() {
		if (getProblem() == null)
			return null;
		Depot d = new Depot();
		d.setName(getProblem().depot.getName());
		d.setCommenceTime(getProblem().depot.getCommenceTime());
		d.setLocation(getProblem().depot.getLocation());
		d.setX(getProblem().depot.getX());
		d.setY(getProblem().depot.getY());
		return d;
	}

	// Save the transactions made during the processing
	public void saveTransactions(String filePath){
		try {
			File f = new File(filePath.substring(0, filePath.lastIndexOf("\\")));
			f.mkdirs();
			f = new File(filePath);
			f.createNewFile();
			
			FileWriter writer = new FileWriter(filePath);

			writer.append("Visit,Sender,Receiver\n");
			
			Iterator<Transaction> it = auction.getTransactions().iterator();
			while(it.hasNext()){
				writer.append(it.next().toCSV() +"\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Save the results to the file specified
	public void saveResults(String filePath) {
		try {
			File f = new File(filePath.substring(0, filePath.lastIndexOf("\\")));
			f.mkdirs();
			f = new File(filePath);
			f.createNewFile();
			
			FileWriter writer = new FileWriter(filePath);

			DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss");

			writer.append("Depot Name,Location,x,y,commenceTime\n");
			writer.append(getProblem().depot.name +","+ getProblem().depot.location +","+ getProblem().depot.x + ","
					+ getProblem().depot.y +","+ df.format(getProblem().depot.commenceTime) + "\n\n\n");

			int counter = 0;
			Iterator<Route> vit = getAuctionBehaviour().getSolution().iterator();
			while (vit.hasNext()) {
				Route route = vit.next();
				writer.append("Visit Name,Location,x,y,windowStart,windowEnd,visitLength,Transport Mode\n");
				for (VisitData v : route.visits) {
					if(v instanceof CarShare){
						int i = 0;
						for (VisitData csVisit : ((CarShare)v).visits) {
							writer.append(csVisit.name +","+ csVisit.location +","+ csVisit.x +","+ csVisit.y +","+ df.format(csVisit.windowStart)
							+","+ df.format(csVisit.windowEnd) +","+ (i==0 ? csVisit.visitLength : "") +",Car Share"+ (i == 0 ? " Start" : (i == ((CarShare)v).visits.size()-1 ? " End" : "")) +"\n");
							i++;
						}
					}else{
						writer.append(v.name +","+ v.location +","+ v.x +","+ v.y +","+ df.format(v.windowStart)
						+","+ df.format(v.windowEnd) +","+ v.visitLength +","+ v.transport +"\n");						
					}
				}
				writer.append("Route Name,Route" + counter + "\n");
				writer.append("Time," + route.totalTime + "\n");
				writer.append("Emissions," + route.totalEmissions + "\n");
				writer.append("Distance," + route.totalDist + "\n");
				writer.append("Cost," + route.totalCost + "\n\n");
				counter++;
			}

			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}
