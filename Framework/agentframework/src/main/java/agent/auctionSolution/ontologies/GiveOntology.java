package agent.auctionSolution.ontologies;

import agent.auctionSolution.dataObjects.BidResponse;
import agent.auctionSolution.dataObjects.CostVariables;
import agent.auctionSolution.dataObjects.Depot;
import agent.auctionSolution.dataObjects.JourneyData;
import agent.auctionSolution.dataObjects.ReturnToCar;
import agent.auctionSolution.dataObjects.Route;
import agent.auctionSolution.dataObjects.VisitData;
import agent.auctionSolution.dataObjects.carShare.CarShare;
import agent.auctionSolution.dataObjects.carShare.CarShareOffer;
import agent.auctionSolution.dataObjects.carShare.CarShareRequest;
import agent.auctionSolution.dataObjects.carShare.CarShareResult;
import jade.content.onto.BasicOntology;
import jade.content.onto.BeanOntology;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;
import jade.core.AID;

/**
 * This Ontology is used as the language for object
 * encoding and decoding for messages sent that need
 * to contain an object that implements the Concept
 * interface.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class GiveOntology extends BeanOntology {
	
	// Name of the ontology
	public static final String ONTOLOGY_NAME = "Give-Ontology";
		
	
	// The singleton instance of this ontology
	private static Ontology theInstance = new GiveOntology();

	// This is the method to access the singleton music shop ontology object
	public static Ontology getInstance() {
		return theInstance;
	}
	
	public GiveOntology(){
		super(ONTOLOGY_NAME, BasicOntology.getInstance());
		
		try {
			add(Route.class);
			add(JourneyData.class);
			add(VisitData.class);
			add(ReturnToCar.class);
			add(Depot.class);
			add(BidResponse.class);
			add(CostVariables.class);

			
			add(CarShare.class);
			add(CarShareOffer.class);
			add(CarShareRequest.class);
			add(CarShareResult.class);
						
			
			add(GiveObjectPredicate.class);
		} catch (BeanOntologyException oe) {
			oe.printStackTrace();
		}
		
	}
	
}
