package agent.auctionSolution.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import agent.auctionSolution.MapServer;

/**
 * The gui for starting the MapServer agent and selecting
 * the data to be used
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class MapServerGui extends JFrame {

	private JPanel contentPane;

	// Reference to the map server
	private MapServer mapServer;
	
	// Gui buttons
	JButton btnLoadFromFile, btnStart;
	
	// The label used to show the status of the system
	private JLabel lblStatus;
	
	public MapServerGui(MapServer mapServer){
		this.mapServer = mapServer;
		initialise();
		setVisible(true);
	}

	public void setStatus(String msg){
		lblStatus.setText("Status: " + msg);
	}

	/**
	 * Create the frame.
	 */
	private void initialise() {
		setTitle("MapServer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		btnLoadFromFile = new JButton("Load JourneyData from File");
		btnLoadFromFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mapServer.loadFromFile();
				btnStart.setEnabled(true);
			}
		});
		
		btnStart = new JButton("Start MapServer");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLoadFromFile.setEnabled(false);
				btnStart.setEnabled(false);
				mapServer.start();
			}
		});
		btnStart.setEnabled(false);
		
		lblStatus = new JLabel("Status: Not Ready");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);

		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnStart, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
						.addComponent(btnLoadFromFile, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
						.addComponent(lblStatus, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnLoadFromFile)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnStart)
					.addPreferredGap(ComponentPlacement.RELATED, 170, Short.MAX_VALUE)
					.addComponent(lblStatus))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
