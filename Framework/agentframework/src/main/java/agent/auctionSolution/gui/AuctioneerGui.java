package agent.auctionSolution.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import agent.auctionSolution.auction.Auctioneer;
import agent.auctionSolution.dataObjects.Depot;
import agent.auctionSolution.dataObjects.HasXandY;
import agent.auctionSolution.dataObjects.ReturnToCar;
import agent.auctionSolution.dataObjects.VisitData;
import agent.auctionSolution.dataObjects.carShare.CarShare;

/**
 * The interface created by the Auctioneer to allow the user to choose what file
 * to use as the problem and when to start executing. This Gui uses a JPanel
 * within the JFrame to supply the user with a graphical representation of the
 * solution.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class AuctioneerGui extends JFrame implements WindowListener {

	// The panel used to render the solution
	JPanel panel;
	// The label used to show the status of the system
	private JLabel lblStatus;
	// The auctioneer that owns this Gui
	private Auctioneer auctioneer;

	// Buttons for loading problems and starting the system
	JButton btnLoadFromFile, btnStartSystem;

	// The reference to the gui for selecting what problem(s) to run
	private ProblemSelectionGui problemSelectionGui = null;
	
	// Used to determine if the the locations should be rendered
	private boolean renderLocations = true;
	// Changes the value of renderLocations
	JCheckBox chckbxRenderLocations;
	// Table to show routes for selection
	private JTable table;

	// The route to render (all if -1)
	private int selectedRoute = -1;
	// The radius of the rendered visits
	private int visitRadius = 5;

	public AuctioneerGui(Auctioneer auctioneer) {
		this.auctioneer = auctioneer;

		initialize();
		setVisible(true);
	}

	// Sets the text of the Status label to: "Status: " + msg
	public void setStatus(String msg) {
		lblStatus.setText("Status: " + msg);
	}

	public int getReturnableVisits(){
		return problemSelectionGui.getReturnableVisits();
	}
	
	public boolean getAllowCarSharing(){
		return problemSelectionGui.getAllowCarSharing();
	}
	
	public void paint(Graphics g) {
		super.paint(g);

		BufferedImage b = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_RGB);

		Graphics2D g2 = (Graphics2D) b.getGraphics();
		FontMetrics fm = g2.getFontMetrics();
		g2.drawRect(0, 0, 640, 480);
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());

		g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
		g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		Depot depot = auctioneer.getDepot();
		List<List<VisitData>> solution = auctioneer.getSolution();

		if (depot != null && solution != null) {

			// Update the route selection table
			Object[][] tableRoutes = new Object[solution.size()][1];
			for (int i = 0; i < tableRoutes.length; i++)
				tableRoutes[i][0] = i;

			table.setModel(new DefaultTableModel(tableRoutes, new String[] { "Route" }));

			// Apply the render offsets
			Rectangle2D.Double renderOffset = auctioneer.getRenderOffset();
			double smallerSide = Math.min(panel.getWidth(), panel.getHeight());
			renderOffset.width = (smallerSide / renderOffset.width) * 0.8d;
			renderOffset.height = (smallerSide / renderOffset.height) * 0.8d;
			for (List<VisitData> route : solution) {
				for (VisitData visit : route) {
					visit.x -= renderOffset.x;
					visit.y -= renderOffset.y;
					visit.x *= renderOffset.getWidth();
					visit.y *= renderOffset.getHeight();

					if(visit.transport.equals("Car Share")){
						for(VisitData csVisit : ((CarShare)visit).visits){
							csVisit.x -= renderOffset.x;
							csVisit.y -= renderOffset.y;
							csVisit.x *= renderOffset.getWidth();
							csVisit.y *= renderOffset.getHeight();
						}
					}
				}
			}
			depot.x -= renderOffset.x;
			depot.y -= renderOffset.y;
			depot.x *= renderOffset.getWidth();
			depot.y *= renderOffset.getHeight();

			g2.translate(panel.getWidth() / 2, panel.getHeight() / 2);// renderOffset.y
																		// *
																		// renderOffset.height);

			Iterator<List<VisitData>> rit = solution.iterator();
			HasXandY prev;
			int routeNo = -1;
			// Render the connections between visits
			while (rit.hasNext()) {
				routeNo++;
				int opacity = 255;
				if (selectedRoute != routeNo && selectedRoute != -1){
					opacity = 10;
				}

				List<VisitData> route = rit.next();
				Iterator<VisitData> nit = route.iterator();
				prev = depot;
				while (nit.hasNext()) {
					VisitData n = nit.next();
					if(n.transport.equals("Car")){
						g2.setColor(new Color(255, 0, 0, opacity));
					}else if(n.transport.equals("Public Transport")){
						g2.setColor(new Color(0, 0, 255, opacity));
					}else if(n.transport.equals("Car Share")){
						if(prev instanceof VisitData){
							if(((VisitData)prev).transport.equals("Car")){
								g2.setColor(new Color(200, 50, 0, opacity));
							}else if(((VisitData)prev).transport.equals("Public Transport")){
								g2.setColor(new Color(0, 50, 200, opacity));
							}
						}else{
							g2.setColor(new Color(90, 0, 255, opacity));
						}
					}else{
						g2.setColor(new Color(0,0,0, opacity));
					}
					g2.drawLine((int) n.x, (int) n.y, (int) prev.getX(), (int) prev.getY());
					
					prev = n;
					if (n instanceof CarShare) {
						g2.setColor(new Color(0,255,0,opacity));
						CarShare cs = (CarShare) n;
						int i = -1;
						for (VisitData csVisit : cs.visits) {
							i++;
							if(i == 0)
								continue;

							g2.drawLine((int) csVisit.x, (int) csVisit.y, (int) prev.getX(), (int) prev.getY());							
							
							prev = csVisit;
						}
					}					
				}
				if(prev instanceof ReturnToCar){
					g2.setColor(new Color(255, 0, 0, opacity));
				}else if(prev instanceof CarShare){
					g2.setColor(new Color(0,255,0, opacity));
					// TODO draw more lines
				}
				g2.drawLine((int) depot.x, (int) depot.y, (int) prev.getX(), (int) prev.getY());
			}

			routeNo = -1;
			rit = solution.iterator();
			// Render the visits
			while (rit.hasNext()) {
				routeNo++;

				List<VisitData> route = rit.next();
				Iterator<VisitData> nit = route.iterator();
				while (nit.hasNext()) {
					VisitData n = nit.next();

					int opacity = 255;
					if (selectedRoute != routeNo && selectedRoute != -1){
						opacity = 10;
					}
					g2.setColor(new Color(255, 221, 84, opacity));
					

					if (n instanceof CarShare) {
						CarShare cs = (CarShare) n;
						for (VisitData csVisit : cs.visits) {					
							g2.setColor(new Color(0, 255 ,0, opacity));
							g2.fillOval((int) csVisit.x - visitRadius, (int) csVisit.y - visitRadius, visitRadius * 2, visitRadius * 2);
						
							g2.setColor(new Color(60, 60, 60, opacity));
							g2.drawOval((int) csVisit.x - visitRadius, (int) csVisit.y - visitRadius, visitRadius * 2, visitRadius * 2);						
						}		
						g2.setColor(new Color(0, 255 ,0, opacity));
					}
					
					g2.fillOval((int) n.x - visitRadius, (int) n.y - visitRadius, visitRadius * 2, visitRadius * 2);
					g2.setColor(new Color(60, 60, 60, opacity));

					g2.drawOval((int) n.x - visitRadius, (int) n.y - visitRadius, visitRadius * 2, visitRadius * 2);

					if (renderLocations && (selectedRoute == routeNo || selectedRoute == -1)) {
						g2.setColor(Color.BLACK);
						String location = n.location;
						Rectangle2D r = fm.getStringBounds(location, g2);
						g2.drawString(location, (int) (n.x - r.getWidth() / 2), (int) (n.y + r.getHeight() / 2));
						
						if(n instanceof CarShare){
							for(VisitData csVisit : ((CarShare)n).visits){
								location = csVisit.location;
								r = fm.getStringBounds(location, g2);
								g2.drawString(location, (int) (csVisit.x - r.getWidth() / 2), (int) (csVisit.y + r.getHeight() / 2));
							}
						}
						
					}
				}
			}

			// Render the Depot
			g2.setColor(Color.GREEN);
			g2.fillOval((int) depot.x - visitRadius, (int) depot.y - visitRadius, visitRadius * 2, visitRadius * 2);
			g2.setColor(Color.DARK_GRAY);
			g2.drawOval((int) depot.x - visitRadius, (int) depot.y - visitRadius, visitRadius * 2, visitRadius * 2);
			g2.setColor(Color.BLACK);
			Rectangle2D depotRect = fm.getStringBounds(depot.location, g2);
			if (renderLocations)
				g2.drawString(depot.location, (int) (depot.x - depotRect.getWidth() / 2),
						(int) (depot.y + depotRect.getHeight() / 2));
			depotRect = fm.getStringBounds("Depot", g2);
			g2.drawString("Depot", (int) (depot.x - depotRect.getWidth() / 2),
					(int) (depot.y - (renderLocations ? (depotRect.getHeight() / 2) : 0)));

			String text = "Showing " + (selectedRoute == -1 ? "all routes." : "route " + selectedRoute + ".");
			Rectangle2D textRect = fm.getStringBounds(text, g2);
			g2.drawString(text, (int) (-panel.getWidth() * 0.49d),
					(int) (panel.getHeight() * 0.5d - textRect.getHeight() * 0.5d));

		} else {
			g2.setColor(Color.BLACK);
			g2.translate(panel.getWidth() / 2, panel.getHeight() / 2);
			String text = this.btnLoadFromFile.isEnabled() ? "Select a file and click 'Start Auctioneer'."
					: "Processing..";
			Rectangle2D textRect = fm.getStringBounds(text, g2);
			g2.drawString(text, (int) -textRect.getWidth() / 2, 0);

		}

		Graphics gPanel = panel.getGraphics();
		gPanel.drawImage(b, 0, 0, null);
		gPanel.setColor(Color.GRAY);
		gPanel.drawRect(2, 2, panel.getWidth() - 5, panel.getHeight() - 5);
		gPanel.drawRect(0, 0, panel.getWidth() - 1, panel.getHeight() - 1);
		gPanel.setColor(Color.BLACK);

	}

	public void canLoadFile(boolean b) {
		btnLoadFromFile.setEnabled(b);
	}

	// Called when the ProblemSelectionGui closes
	public void windowClosed(WindowEvent e) {
		// Pass the files to the auctioneer
		auctioneer.loadFromFiles(problemSelectionGui.getFiles(), problemSelectionGui.getNoBidders());
		if(problemSelectionGui.getFiles().size() > 0){
			auctioneer.loadFromFile();
			btnStartSystem.setEnabled(true);
		}
		toFront();
		requestFocus();
		setEnabled(true);
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("Auctioneer");
		setBounds(100, 100, 750, 450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		btnLoadFromFile = new JButton("Load Problem(s) from File(s)");
		btnLoadFromFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Open the problem selection gui
				problemSelectionGui = new ProblemSelectionGui();
				problemSelectionGui.addWindowListener(AuctioneerGui.this);
				setEnabled(false);
			}
		});

		lblStatus = new JLabel("Status: Not Ready");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);

		btnStartSystem = new JButton("Start Auctioneer");
		btnStartSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Start the Auctioneer
				auctioneer.minimiseFactor = problemSelectionGui.getMinimiseFactor();
				auctioneer.transportMode = problemSelectionGui.getTransportMode();
				auctioneer.start();
				btnStartSystem.setEnabled(false);
				btnLoadFromFile.setEnabled(false);
				repaint();
			}
		});
		btnStartSystem.setEnabled(false);

		panel = new JPanel();

		chckbxRenderLocations = new JCheckBox("Render Locations");
		chckbxRenderLocations.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				renderLocations = e.getStateChange() == 1 ? true : false;
				repaint();
			}
		});
		chckbxRenderLocations.setSelected(true);

		JScrollPane scrollPane = new JScrollPane((Component) null);

		JButton btnNewButton = new JButton("Show All Routes");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedRoute = -1;
				repaint();
			}
		});

		final JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(5), new Integer(1), null, new Integer(1)));
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				visitRadius = (Integer) spinner.getValue();
				repaint();
			}
		});

		JLabel lblNewLabel = new JLabel("Visit Radius");
		
		JButton btnCycleRoutes = new JButton("Cycle Routes");
		btnCycleRoutes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final List<List<VisitData>> solution = auctioneer.getSolution();
				if(solution == null)
					return;
				
				new Thread(new Runnable() {
				    public void run() {
				    	selectedRoute = 0;
						for(int i = 0; i < solution.size(); i++){
							if(selectedRoute == -1)
								break;
							selectedRoute = i;
							repaint();
							try {
								Thread.sleep(250);
							} catch (InterruptedException e1) {
								e1.printStackTrace();
							}
						}
				    }
				}).start();
			}
		});

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(lblNewLabel))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(lblStatus, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
						.addComponent(btnLoadFromFile, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnStartSystem, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnCycleRoutes, GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
							.addGap(4))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(chckbxRenderLocations)
							.addGap(14)))
					.addGap(11))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnLoadFromFile)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnStartSystem)
							.addGap(11)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel)
								.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(7)
							.addComponent(chckbxRenderLocations)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton)
								.addComponent(btnCycleRoutes))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblStatus))
						.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE))
					.addContainerGap())
		);

		table = new JTable() {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedRoute = table.getSelectedRow();
				repaint();
			}
		});

		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Route" }));

		scrollPane.setViewportView(table);
		getContentPane().setLayout(groupLayout);

	}

	// Unused window listeners that are required by the WindowListener Interface
	public void windowActivated(WindowEvent e) {}
	public void windowDeactivated(WindowEvent e) {}
	public void windowDeiconified(WindowEvent e) {}
	public void windowIconified(WindowEvent e) {}
	public void windowOpened(WindowEvent e) {}
	public void windowClosing(WindowEvent e) {}
}
