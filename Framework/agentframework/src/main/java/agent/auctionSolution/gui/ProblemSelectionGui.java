package agent.auctionSolution.gui;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

/**
 * The interface created by the Auctioneer to allow the user to choose what file
 * to use as the problem and when to start executing. This Gui uses a JPanel
 * within the JFrame to supply the user with a graphical representation of the
 * solution.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class ProblemSelectionGui extends JFrame {

	// The problem files to be loaded
	private List<String> files = new ArrayList<String>();
	// The number of bidders to be used for each problem
	private List<Integer> noBidders = new ArrayList<Integer>();

	JComboBox<String> comboBoxMinimise;	
	JComboBox<String> comboBoxTransport;
	
	JSpinner spinner;
	private JTable table;
	
	JCheckBox chckbxAllowCarSharing;
	
	

	public ProblemSelectionGui() {
		initialize();
		setVisible(true);
	}

	public int getReturnableVisits(){
		return (Integer) spinner.getValue();
	}
	
	public String getMinimiseFactor(){
		return (String) comboBoxMinimise.getSelectedItem();
	}
	
	public String getTransportMode(){
		return (String) comboBoxTransport.getSelectedItem();
	}
	
	public boolean getAllowCarSharing(){
		return chckbxAllowCarSharing.isSelected();
	}
	
	public List<String> getFiles(){
		return this.files;
	}
	
	public List<Integer> getNoBidders(){
		return this.noBidders;
	}

	private void updateTable(){
		Object[][] data = new Object[files.size()][2];
		for(int i = 0; i < data.length; i++){
			data[i][0] = files.get(i);
			data[i][1] = noBidders.get(i);
		}

		table.setModel(new DefaultTableModel(data, new String[] { "File", "Bidders" }));
	}


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("Select Problems");
		setBounds(100, 100, 478, 282);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JScrollPane scrollPane = new JScrollPane((Component) null);
		
		JButton btnAddProblemFile = new JButton("Add Problem File");
		btnAddProblemFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Create a file chooser
				final JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
				// Set the title of the file chooser
				fc.setDialogTitle("Select the problem CSV file");
				// In response to a button click:
				int returnVal = fc.showOpenDialog(null);

				// If a file was selected then load it
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();

					String line = "";
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(file));
						line = br.readLine();
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					// If the first line contains 'batch' then load multiple problems
					if(line.toUpperCase().contains("BATCH")){
						try{
							String fileDir = file.getParent();
							line = br.readLine();
							while (line != null) {
								String[] content = line.split(",");
								if(content.length != 2)
									content = line.split("\t");

								if(content.length == 2){
									noBidders.add(Integer.parseInt(content[1]));
									String f = fileDir +"\\"+ content[0];
									if(!f.toUpperCase().endsWith(".CSV"))
										f += ".csv";
									files.add(f);
								}
								
								line = br.readLine();
							}
						} catch (IOException e1) {
							e1.printStackTrace();
						}catch(NumberFormatException e2){
							e2.printStackTrace();
						}
					}else{					
						// Request the user for the bidder quantity
						String input = JOptionPane.showInputDialog("How many bidders?\n(comma seperated for multiple runs with a different number of bidders)");
						try{
							if(input != null){
								if(input.contains(",")){
									String[] numbers = input.split(",");
									for(String s : numbers){
										noBidders.add(Integer.parseInt(s));
										files.add(file.getAbsolutePath());
									}
								}else{
									noBidders.add(Integer.parseInt(input));
									files.add(file.getAbsolutePath());
								}
							}
						}catch(NumberFormatException e2){}
					}
				}

		        updateTable();

			}
		});
		
		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JLabel lblNewLabel = new JLabel("Double click to remove an entry");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);


		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(5), new Integer(0), null, new Integer(1)));
		
		JLabel lblReturnableVisits = new JLabel("Returnable Visits");
		
		comboBoxMinimise = new JComboBox<String>();
		comboBoxMinimise.setModel(new DefaultComboBoxModel<String>(new String[] {"Emissions", "Cost"}));
		
		comboBoxTransport = new JComboBox<String>();
		comboBoxTransport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chckbxAllowCarSharing.setEnabled(comboBoxTransport.getSelectedItem().equals("Mixed"));
			}
		});
		comboBoxTransport.setModel(new DefaultComboBoxModel<String>(new String[] {"Car", "Public Transport", "Mixed"}));
		
		
		JLabel lblMinimise = new JLabel("Minimise:");
		
		JLabel lblTransportMode = new JLabel("Transport:");
		
		chckbxAllowCarSharing = new JCheckBox("Allow Car Sharing");
		chckbxAllowCarSharing.setEnabled(false);

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnAddProblemFile, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
						.addComponent(lblNewLabel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblTransportMode)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(comboBoxTransport, 0, 132, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(spinner, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblReturnableVisits, GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblMinimise, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(comboBoxMinimise, 0, 137, Short.MAX_VALUE))
						.addComponent(btnDone, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
						.addComponent(chckbxAllowCarSharing))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnAddProblemFile)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblNewLabel)
							.addGap(3)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblMinimise)
								.addComponent(comboBoxMinimise, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(3)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(comboBoxTransport, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblTransportMode))
							.addGap(9)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblReturnableVisits))
							.addGap(7)
							.addComponent(chckbxAllowCarSharing)
							.addPreferredGap(ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
							.addComponent(btnDone)))
					.addContainerGap())
		);

		table = new JTable() {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//handle double click event.
				if (e.getClickCount() == 2) {
			        JTable table =(JTable) e.getSource();
			        Point p = e.getPoint();
			        int row = table.rowAtPoint(p);
			        files.remove(row);
			        noBidders.remove(row);
			        updateTable();
				}
			}
		});

		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"File", "Bidders"
			}
		));

		scrollPane.setViewportView(table);
		getContentPane().setLayout(groupLayout);

	}
}
