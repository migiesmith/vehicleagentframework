package agent.auctionSolution.dataObjects;

import jade.content.Concept;
import jade.core.AID;

/**
 * Used by the auctioneer to keep track of the
 * bids that have been made.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class BidResponse implements Concept{

	// The owner of the bid
	private AID bidder = null;
	// The value of the bid
	private double bid = 0.0d;
	// The performative of the bid (Proposal/Refuse)
	private int responsePerformative;
	// The visit being returned (null if there isn't one)
	private VisitData returningVisit = null;
	

	// Getters and Setters required by a Concept
	
	public AID getBidder() {
		return bidder;
	}
	public void setBidder(AID bidder) {
		this.bidder = bidder;
	}
	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public int getResponsePerformative() {
		return responsePerformative;
	}
	public void setResponsePerformative(int responsePerformative) {
		this.responsePerformative = responsePerformative;
	}
	public VisitData getReturningVisit() {
		return returningVisit;
	}
	public void setReturningVisit(VisitData returningVisit) {
		this.returningVisit = returningVisit;
	}


	
}
