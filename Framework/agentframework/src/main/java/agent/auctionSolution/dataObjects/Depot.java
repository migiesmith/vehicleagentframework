package agent.auctionSolution.dataObjects;

import java.util.Date;

import jade.content.Concept;

/**
 *  The depot each employee must start and end at.
 *  
 *  Stores the information about the depot of the
 *  problem. This includes the location (post code),
 *  the data and time that the problem starts at, and
 *  the x and y coordinates of the depot. This
 *  implements Concept and HasXandY. Concept allows
 *  the depot to be sent within a messages using Ontologies.
 *  
 * @author Grant
 */
@SuppressWarnings("serial")
public class Depot implements Concept, HasXandY{
	public String name = "DEPOT";
	public String location;
	public Date commenceTime;
	public double x, y;

	int activeEmployees = -1;
	
	// Getters and Setters required by a Concept
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public int getActiveEmployees() {
		return activeEmployees;
	}

	public void setActiveEmployees(int activeEmployees) {
		this.activeEmployees = activeEmployees;
	}	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public Date getCommenceTime() {
		return commenceTime;
	}
	
	public void setCommenceTime(Date commenceTime) {
		this.commenceTime = commenceTime;
	}
	
}