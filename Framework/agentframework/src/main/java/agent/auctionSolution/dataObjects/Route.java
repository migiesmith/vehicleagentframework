package agent.auctionSolution.dataObjects;

import java.util.ArrayList;
import java.util.List;

import agent.auctionSolution.dataObjects.carShare.CarShare;
import jade.content.Concept;
import jade.core.AID;

/**
 * A container for the resulting routes of each bidder.
 * This stores the list of visits and the total emissions
 * relating to that list. This class implements the Concept
 * interface to allow for the information to be send within
 * messages to agents.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class Route implements Concept{

	// total cost in emissions for this route
	public double totalEmissions = 0.0d;
	// total cost in time for this route
	public double totalTime = 0.0d;
	// total cost in distance for this route
	public double totalDist = 0.0d;
	// total cost in money for this route
	public double totalCost = 0.0d;
	// Vector containing the visits in this route
	public List<VisitData> visits = new ArrayList<VisitData>();
	// The AID of the bidder who made the route
	private AID routeOwner = null;
	
	
	// Used to determine if all of the visits have been visited (ignoring visits that are not part of the problem)
	public int getNoOfVisitsMade(){
		int count = 0;
		for(int i = 0; i < visits.size(); i++){
			if(!(visits.get(i) instanceof ReturnToCar || visits.get(i) instanceof CarShare))
				count++;
		}
		return count;
	}
	

	// Getters and Setters required by a Concept
	
	public double getTotalEmissions() {
		return totalEmissions;
	}

	public void setTotalEmissions(double totalEmissions) {
		this.totalEmissions = totalEmissions;
	}
	
	public double getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(double totalTime) {
		this.totalTime = totalTime;
	}

	public double getTotalDist() {
		return totalDist;
	}

	public void setTotalDist(double totalDist) {
		this.totalDist = totalDist;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	
	public List<VisitData> getVisits() {
		return visits;
	}

	public void setVisits(List<VisitData> visits) {
		this.visits = visits;
	}

	public AID getRouteOwner() {
		return routeOwner;
	}

	public void setRouteOwner(AID routeOwner) {
		this.routeOwner = routeOwner;
	}
	
}
