package agent.auctionSolution.dataObjects;

/**
 * Used to identify visits that are only used to return a route to it's car
 * @author Grant
 */
public class ReturnToCar extends VisitData {  }
