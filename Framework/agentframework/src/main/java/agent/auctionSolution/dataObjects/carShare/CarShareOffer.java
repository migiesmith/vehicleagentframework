package agent.auctionSolution.dataObjects.carShare;

import agent.auctionSolution.dataObjects.Route;
import jade.core.AID;

/**
 * Stores the information about a CarShare offer
 * 
 * @author Grant
 */
public class CarShareOffer {

	// The senderof the offer
	public AID sender;
	// The route of the sender
	public Route route;

	
	// Getters and Setters required by a Concept

	public AID getSender() {
		return sender;
	}

	public void setSender(AID sender) {
		this.sender = sender;
	}
	
	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}	
	
}
