package agent.auctionSolution.dataObjects;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Parses the content of a problem file and stores
 * the VisitData and Depot for that problem.
 * 
 * @author Grant
 */
public class Problem {

	// The name of the problem
	public String name = "";
	// The depot to start and end at
	public Depot depot;
	// The visits to be made
	public List<VisitData> visits;
	
	// Create the problem from a String
	public Problem(String name, String fileContents){
		dataFromString(name, fileContents);
		this.name = name;
	}
	
	// Parse the string
	private void dataFromString(String name, String content){
		
		// Create a scanner to read line by line
		Scanner scanner = new Scanner(content);
		
		// Instantiate the visits List
		visits = new ArrayList<VisitData>();

		// Initialise the depot
		depot = new Depot();
		
		// Create the format to parse the dates
		DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
		
		String line;
		String[] lineContent;
		int sum = 0;
		while(scanner.hasNextLine()){
			line = scanner.nextLine();
			if(line.toUpperCase().startsWith("VISIT,")){
				lineContent = line.split(",");
				try {
					VisitData visitData = new VisitData();
					visitData.setAll(lineContent[1], lineContent[2], df.parse(lineContent[3]), df.parse(lineContent[4]), Double.parseDouble(lineContent[5]), 0, 0, null);
					visits.add(visitData);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				sum++;				
				
			}else if(line.toUpperCase().startsWith("DEPOT,")){
				depot.location = line.split(",")[1];
				
			}else if(line.toUpperCase().startsWith("COMMENCETIME,")){
				try {
					depot.commenceTime = df.parse(line.split(",")[1]);
				} catch (ParseException e) {
					e.printStackTrace();
				}				
				
			}
			
		}
		scanner.close();
		System.out.println(sum + " visits loaded.");
		
	}
	
}
