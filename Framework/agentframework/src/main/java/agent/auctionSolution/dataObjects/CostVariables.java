package agent.auctionSolution.dataObjects;

import jade.content.Concept;

/**
 * Stores the variables used to determine real world costs for the solutions
 * 
 * @author Grant
 */
public class CostVariables implements Concept {
	
	// The cost per hour for a staff member
	public double staffCostPerHour;
	// The cost applied per staff member if they use car
	public double fixedCostsCar;
	// The cost applied per staff member if they use public transport
	public double fixedCostsPt;
	// The additional cost per KM for using a car
	public double carCostsPerKM;
	

	// Getters and Setters required by a Concept
	
	public double getStaffCostPerHour() {
		return staffCostPerHour;
	}
	public void setStaffCostPerHour(double staffCostPerHour) {
		this.staffCostPerHour = staffCostPerHour;
	}
	public double getFixedCostsCar() {
		return fixedCostsCar;
	}
	public void setFixedCostsCar(double fixedCostsCar) {
		this.fixedCostsCar = fixedCostsCar;
	}
	public double getFixedCostsPt() {
		return fixedCostsPt;
	}
	public void setFixedCostsPt(double fixedCostsPt) {
		this.fixedCostsPt = fixedCostsPt;
	}
	public double getCarCostsPerKM() {
		return carCostsPerKM;
	}
	public void setCarCostsPerKM(double carCostsPerKM) {
		this.carCostsPerKM = carCostsPerKM;
	}

}
