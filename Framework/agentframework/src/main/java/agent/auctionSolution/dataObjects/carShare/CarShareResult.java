package agent.auctionSolution.dataObjects.carShare;

import jade.core.AID;

/**
 * Stores the results of looking for a potential CarShare
 * 
 * @author Grant
 */
public class CarShareResult implements Comparable<CarShareResult>{

	// The related agents
	public AID carSharer, shareWith;
	// The resulting CarShare
	public CarShare carShare = new CarShare();

	// The emission cost to the start of the CarShare
	public double emToStart;
	// The emission cost from the end of the CarShare
	public double emFromEnd;

	// Used for checking if one CarShareResult is better than another
	public int compareTo(CarShareResult otherResults) {
		return Double.compare(otherResults.emToStart + otherResults.emFromEnd, emToStart + emFromEnd);
	}
	
	
	// Getters and Setters required by a Concept

	public AID getCarSharer() {
		return carSharer;
	}

	public void setCarSharer(AID carSharer) {
		this.carSharer = carSharer;
	}


	public AID getShareWith() {
		return shareWith;
	}

	public void setShareWith(AID shareWith) {
		this.shareWith = shareWith;
	}

	public CarShare getCarShareVisits() {
		return carShare;
	}

	public void setCarShareVisits(CarShare carShareVisits) {
		this.carShare = carShareVisits;
	}
	
	public double getEmToStart() {
		return emToStart;
	}

	public void setEmToStart(double emToStart) {
		this.emToStart = emToStart;
	}

	public double getEmFromEnd() {
		return emFromEnd;
	}

	public void setEmFromEnd(double emFromEnd) {
		this.emFromEnd = emFromEnd;
	}
	
}
