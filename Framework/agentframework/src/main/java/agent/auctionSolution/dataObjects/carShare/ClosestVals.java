package agent.auctionSolution.dataObjects.carShare;

import agent.auctionSolution.dataObjects.VisitData;

/**
 * Used by the CarShareServer to determine which visits 
 * are closest to the from and to of a request
 * 
 * @author Grant
 */
public class ClosestVals implements Comparable<ClosestVals>{

	// The visit in question
	public VisitData visit;
	// The cost to/from the visit
	public double cost;
	
	public ClosestVals(VisitData visit, double cost){
		this.visit = visit;
		this.cost = cost;
	}
	
	// Used to check which ClosestVal is closer to the from/to of a request
	public int compareTo(ClosestVals other) {
		return Double.compare(cost, other.cost);
	}
		
}
