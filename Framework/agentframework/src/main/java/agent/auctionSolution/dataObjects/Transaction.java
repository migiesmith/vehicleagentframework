package agent.auctionSolution.dataObjects;

import jade.core.AID;

public class Transaction {

	// The AID of the Sender
	private AID sender;
	// The AID of the receiver
	private AID receiver;
	// The VisitData that was sent
	private VisitData visitData;
	
	// Convert the Transaction into CSV format (visitData, sender, receiver)
	public String toCSV(){
		return visitData.name +"," + sender.getLocalName() +","+ (receiver == null ? "NO_RECEIVER" : receiver.getLocalName());
	}
	
	// Converts the Transaction into a readable format (visitData: sender -> receiver)
	public String toString(){
		return visitData.name +": " + sender.getLocalName() +" -> "+ (receiver == null ? "NO_RECEIVER" : receiver.getLocalName());
	}
	

	// Getters and Setters required by a Concept
	
	public AID getSender() {
		return sender;
	}
	public void setSender(AID sender) {
		this.sender = sender;
	}
	public AID getReceiver() {
		return receiver;
	}
	public void setReceiver(AID receiver) {
		this.receiver = receiver;
	}
	public VisitData getVisitData() {
		return visitData;
	}
	public void setVisitData(VisitData visitData) {
		this.visitData = visitData;
	}
	
}
