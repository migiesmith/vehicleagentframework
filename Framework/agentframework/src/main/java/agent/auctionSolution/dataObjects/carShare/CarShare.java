package agent.auctionSolution.dataObjects.carShare;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import agent.auctionSolution.dataObjects.VisitData;
import jade.core.AID;

/**
 * A visit used to represent a CarShare 
 * (consists of a list of VisitData listing what visits are included in the car share)
 * 
 * @author Grant
 */
public class CarShare extends VisitData{
	// The agent who is driving during the car share
	public AID carShareDriver = null;
	// The visits passed during the car share
	public List<VisitData> visits = new ArrayList<VisitData>();
	// The location of the last visit in the list
	public String endLocation;

	
	
	// Getters and Setters required by a Concept
	
	public String getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(String endLocation) {
		this.endLocation = endLocation;
	}
	public AID getCarShareDriver() {
		return carShareDriver;
	}
	public void setCarShareDriver(AID carShareDriver) {
		this.carShareDriver = carShareDriver;
	}
	public List<VisitData> getVisits() {
		return visits;
	}
	public void setVisits(List<VisitData> visits) {
		this.visits = visits;
		if(visits.size() > 0){
			location = visits.get(0).location;
			endLocation = visits.get(visits.size()-1).location;
		}
	}
	
}
