package agent.auctionSolution.dataObjects.carShare;

import agent.auctionSolution.dataObjects.Route;

/**
 * Stores the information about a CarShare request
 * 
 * @author Grant
 */
public class CarShareRequest {

	// The locations the requester wishes to travel between
	public String from, to;
	// The route of the requester
	public Route route;
	// The travel mode used to get to the CarShare
	public String transportMode;

	
	// Getters and Setters required by a Concept


	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public String getTransportMode() {
		return transportMode;
	}

	public void setTransportMode(String transportMode) {
		this.transportMode = transportMode;
	}
	
}
