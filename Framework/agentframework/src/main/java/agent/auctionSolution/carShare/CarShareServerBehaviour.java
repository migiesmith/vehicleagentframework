package agent.auctionSolution.carShare;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import agent.auctionSolution.JourneyInfoHelper;
import agent.auctionSolution.dataObjects.Depot;
import agent.auctionSolution.dataObjects.JourneyData;
import agent.auctionSolution.dataObjects.Route;
import agent.auctionSolution.dataObjects.VisitData;
import agent.auctionSolution.dataObjects.carShare.CarShare;
import agent.auctionSolution.dataObjects.carShare.CarShareOffer;
import agent.auctionSolution.dataObjects.carShare.CarShareRequest;
import agent.auctionSolution.dataObjects.carShare.CarShareResult;
import agent.auctionSolution.dataObjects.carShare.ClosestVals;
import agent.auctionSolution.ontologies.GiveObjectPredicate;
import agent.auctionSolution.ontologies.GiveOntology;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Handles requests and offers relating to car sharing.
 * All offers are stored in a list until a request is 
 * fulfilled using the offer. 
 * 
 * @author Grant
 */
public abstract class CarShareServerBehaviour extends Behaviour{

	// Initialise the language for message contents
	private Codec codec = new SLCodec();
	private Ontology ontologyGive = GiveOntology.getInstance();

	// Enum for the steps this behaviour can be in
	private enum STEP{
		AddLanguages,
		Initialise,
		WaitForRequests
	}
	
	// The depot of the current problem
	protected Depot depot;
	
	// Used to check travel times and to check if a route is valid
	public JourneyInfoHelper journeyInfo;
	
	// The current step this behaviour is on
	private STEP currStep = STEP.AddLanguages;
	
	protected List<CarShareOffer> offers = new ArrayList<CarShareOffer>();

	MessageTemplate messageFilter,
					offerTemplate,
					requestTemplate,
					acceptTemplate,
					removeReqTemplate,
					depotInfoTemplate;
	
	
	
	CarShareServerBehaviour(){
		offerTemplate = MessageTemplate.MatchConversationId("car-share-offer");
		requestTemplate = MessageTemplate.MatchConversationId("car-share-request");
		acceptTemplate = MessageTemplate.MatchConversationId("car-share-proposal");
		
		removeReqTemplate = MessageTemplate.MatchConversationId("car-share-remove");
		
		depotInfoTemplate = MessageTemplate.MatchConversationId("depot-setup");
		
		messageFilter = MessageTemplate.or(
				MessageTemplate.or(
						MessageTemplate.or(offerTemplate, acceptTemplate), 
						depotInfoTemplate
						),
				MessageTemplate.or(requestTemplate, removeReqTemplate)
				);
	}
	
	

	// abstract methods that must be implemented //	
	/**
	 * Execute any inital code that is needed
	 * @return
	 */
	protected abstract boolean initialise();
	
	/**
	 * Adds the offer to the list if it is valid
	 * @param newOffer
	 */
	private void addOffer(CarShareOffer newOffer){
		if(newOffer.route.visits.size() <= 1){
			return;
		}
		
		for(VisitData v : newOffer.route.visits){
			if(v instanceof CarShare)
				return;
		}

		removeOffersFrom(newOffer.sender);
		offers.add(newOffer);
		
	}
	
	/**
	 * Removes the offers from the passed in AID
	 * @param carSharer
	 */
	private void removeOffersFrom(AID carSharer){
		Iterator<CarShareOffer> it = offers.iterator();
		while(it.hasNext()){
			CarShareOffer offer = it.next();
			if(offer.sender.equals(carSharer)){
				it.remove();
			}
		}
	}
	
	/**
	 * Reads the offer from the passed in message 
	 * and then adds it to the list of offers.
	 * @param offer
	 */
	private void handleOffer(ACLMessage offer){
		// Read the offer from the msg content
		try {
			ContentElement d = myAgent.getContentManager().extractContent(offer);
			if (d instanceof GiveObjectPredicate) {
				CarShareOffer newOffer = (CarShareOffer) ((GiveObjectPredicate) d).getData();
				newOffer.setSender(offer.getSender());
				addOffer(newOffer);
			}
		} catch (UngroundedException e) {
			e.printStackTrace();
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the CarShareRequest contained in the 
	 * passed in message.
	 * @param request Message containing a CarShareRequest as the content
	 * @return Message contents as CarShareRequest
	 */
	private CarShareRequest getRequest(ACLMessage request){
		// Read the offer from the msg content
		try {
			ContentElement d = myAgent.getContentManager().extractContent(request);
			if (d instanceof GiveObjectPredicate) {
				return (CarShareRequest) ((GiveObjectPredicate) d).getData();
			}
		} catch (UngroundedException e) {
			e.printStackTrace();
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Finds the optimal Car Share for a request and 
	 * sends it as a response to the sender
	 * @param msg
	 */
	private void handleRequest(ACLMessage msg){
		// Get the request information
		CarShareRequest request = getRequest(msg);
		// Keep track of the best option
		CarShareResult bestShare = null;
		
		// Loop through the available offers
		Iterator<CarShareOffer> it = offers.iterator();
		while(it.hasNext()){
			CarShareOffer offer = it.next();
			// If the offer is from the requester then skip
			if(offer.sender.equals(msg.getSender())){
				continue;
			}
			
			// Get the results of matching the current offer with the request
			CarShareResult result = getResultsOfCarSharing(request, offer);
			// If the new share is better than the current best share
			if(bestShare == null || (result != null && result.compareTo(bestShare) < 0)){
				// Set best share to the new share
				bestShare = result;
				if(bestShare != null){
					bestShare.setShareWith(msg.getSender());
					bestShare.setCarSharer(offer.sender);
				}
			}
		}

		
		
		// Send the best option for car sharing
		ACLMessage proposal = msg.createReply();
		proposal.setPerformative(ACLMessage.PROPOSE);
		proposal.setConversationId("car-share-proposal");
		proposal.setLanguage(codec.getName());
		proposal.setOntology(ontologyGive.getName());
		try {
			GiveObjectPredicate give = new GiveObjectPredicate();
			give.setData(bestShare);
			myAgent.getContentManager().fillContent(proposal, give);
			myAgent.send(proposal);
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Inform the sender of the offer stored in the 
	 * results that their offer has been accepted.
	 * @param result
	 */
	private void informCarShareAcceptance(CarShareResult result){
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.addReceiver(new AID(result.carSharer.getLocalName(), AID.ISLOCALNAME));
		msg.setConversationId("car-share-accepted");
		msg.setLanguage(codec.getName());
		msg.setOntology(ontologyGive.getName());
		try {
			GiveObjectPredicate give = new GiveObjectPredicate();
			give.setData(result);
			myAgent.getContentManager().fillContent(msg, give);
			myAgent.send(msg);			
			removeOffersFrom(result.carSharer);
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Check if a CarShare can be made without breaking any time constraints
	 * @param results
	 * @param request
	 * @param offer
	 * @param timeFrom
	 * @param closestFrom
	 * @param closestTo
	 * @return Whether or not a valid CarShare can be made
	 */
	private boolean validTime(CarShareResult results, CarShareRequest request, CarShareOffer offer, double timeFrom, VisitData closestFrom, VisitData closestTo){
		// Calculate the times required for checking if the car share is valid (offer)
		double timeTillFromStarts = 0.0d;
		double timeTillAfterCarShare = 0.0d;
		double totalTime = 0.0d;
		int i = 0;
		Iterator<VisitData> it = offer.route.visits.iterator();
		while(it.hasNext()){
			VisitData v = it.next();
			if (!(v instanceof CarShare)) {
				if (i == 0) {
					totalTime += journeyInfo.getTravelTime(myAgent, depot.location, v.location, "Car");
				} else {
					totalTime += journeyInfo.getTravelTime(myAgent, offer.route.visits.get(i - 1).location, v.location, "Car");
				}
				if (i == offer.route.visits.size()) {
					totalTime += journeyInfo.getTravelTime(myAgent, v.location, depot.location, "Car");
				}
			}
			if(v == closestFrom){
				timeTillFromStarts = totalTime;
			}else if(v == closestTo){
				timeTillAfterCarShare = totalTime;
				break;
			}
			
			totalTime += v.visitLength;

			i++;
		}
		
		// Calculate the time taken till the request's from ends
		double fromTime = 0.0d;
		it = request.route.visits.iterator();
		int k = 0;
		while(it.hasNext()){
			VisitData v = it.next();
			if (k == 0) {
				fromTime += journeyInfo.getTravelTime(myAgent, depot.location, v.location, request.getTransportMode());

			} else {
				VisitData prev = request.route.visits.get(k - 1);
				fromTime += journeyInfo.getTravelTime(myAgent, (prev instanceof CarShare ? ((CarShare)prev).endLocation : prev.location), v.location, request.getTransportMode());

			}
			if (k == request.route.visits.size()) {
				if(v instanceof CarShare){
					fromTime += journeyInfo.getTravelTime(myAgent, ((CarShare)v).endLocation, depot.location, request.getTransportMode());
				}else{
					fromTime += journeyInfo.getTravelTime(myAgent, v.location, depot.location, request.getTransportMode());						
				}
			}
			fromTime += v.visitLength;
			
			if(v.location.equals(request.from)){
				break;
			}
			k++;
		}
		
		// Create calendars using the calculated times
		Calendar requestCalendar = Calendar.getInstance();
		requestCalendar.setTime((request.route.visits.size() > 0? request.route.visits.get(0).getWindowStart() : depot.commenceTime));
		requestCalendar.add(Calendar.MINUTE, (int)fromTime);
				
		Calendar offerCalendar = Calendar.getInstance();
		offerCalendar.setTime(offer.route.visits.get(0).windowStart);
		offerCalendar.add(Calendar.MINUTE, (int)timeTillFromStarts);
		
		// Check if the requester can arrive during the first visit of the car share route
		if(requestCalendar.compareTo(offerCalendar) >= 0){
			offerCalendar.add(Calendar.MINUTE, (int)closestFrom.visitLength);
			if(requestCalendar.compareTo(offerCalendar) <= 0){

				/* The requester can arrive in time, check if adding the
				 * Car Share to their route will break time constraints
				 */
				
				offerCalendar.setTime(offer.route.visits.get(0).windowStart);
				offerCalendar.add(Calendar.MINUTE, (int)timeTillFromStarts);
				results.carShare.setWindowStart(offerCalendar.getTime());
				
				
				offerCalendar.setTime(offer.route.visits.get(0).windowStart);
				offerCalendar.add(Calendar.MINUTE, (int)timeTillAfterCarShare);
				results.carShare.setWindowEnd(offerCalendar.getTime());

				// Check if this will fit within the requester's route
				
				Route tempRoute = new Route();
				it = request.route.visits.iterator();
				while (it.hasNext()) {
					VisitData v = it.next();
					tempRoute.visits.add(v);
					if(v.location.equals(request.from)){
						tempRoute.visits.add(results.carShare);
					}
				}
				if (tempRoute.visits.size() == 0) {
					tempRoute.visits.add(results.carShare);
				}

				return journeyInfo.validTime(myAgent, tempRoute, depot);
			}
		}
		return false;
	}
	
	/**
	 * Finds the optimal car share if there is one
	 * @param request The request to be fulfilled
	 * @param offer The offer that is being checked
	 * @return
	 */
	private CarShareResult getResultsOfCarSharing(CarShareRequest request, CarShareOffer offer){
		CarShareResult results = new CarShareResult();
		
		// Create a list of distances from each visit in the offer list  
		// to find the ones closest to the requester's to and from
		List<ClosestVals> closestFromList = new ArrayList<ClosestVals>();
		List<ClosestVals> closestToList = new ArrayList<ClosestVals>();
		
		for(VisitData v : offer.route.visits){
			double time = journeyInfo.getTravelTime(myAgent, request.from, v.location, request.transportMode);
			closestFromList.add(new ClosestVals(v, time));
			
			time = journeyInfo.getTravelTime(myAgent, v.location, request.to, "Public Transport");
			closestToList.add(new ClosestVals(v, time));
		}
		
		// Sort the list into ascending order
		Collections.sort(closestFromList);
		Collections.sort(closestToList);
		
		
		for(ClosestVals closestFrom : closestFromList){
			for(ClosestVals closestTo : closestToList){
				// Check that from comes before to and that they are not the same
				int fromIndex = offer.route.visits.indexOf(closestFrom.visit);
				int toIndex = offer.route.visits.indexOf(closestTo.visit);				
				if(toIndex - fromIndex < 1){
					continue;
				}
				if (!closestFrom.visit.transport.equals("Car")) {
					continue;
				}
				if (!closestTo.visit.transport.equals("Car")) {
					continue;
				}
				
				// If this makes the requester's route cost more then skip
				if(getJourneyCost(request.from, closestFrom.visit.location, request.transportMode) + getJourneyCost(closestFrom.visit.location, request.to, request.transportMode) > getJourneyCost(request.from, request.to, "Car"))
					continue;
					
				// Make a list containing the Car Share visits
				List<VisitData> visits = new ArrayList<VisitData>();
				for (int i = fromIndex; i <= toIndex; i++) {
					visits.add(offer.route.visits.get(i));
				}

				results.carShare.setVisits(visits);
				
				// Calculate the length of the car share
				double duration = 0.0d;
				String prevLoc = request.from;
				for (int i = 0; i < results.carShare.visits.size(); i++) {
					VisitData v2 = results.carShare.visits.get(i);
										
					duration += journeyInfo.getTravelTime(myAgent, prevLoc, v2.location, "Car") + v2.visitLength;
					
					if (i == results.carShare.visits.size() - 1) {
						duration += journeyInfo.getTravelTime(myAgent, v2.location, request.to, "Public Transport");
					}
					
					prevLoc = v2.location;
				}
				
				results.carShare.visitLength = duration;

				// If adding this Car Share to the requester's route doesn't break any time constraints
				if (validTime(results, request, offer, closestFrom.cost, closestFrom.visit, closestTo.visit)) {
					// Set the results values
					results.setEmFromEnd(getJourneyCost(closestTo.visit.location, request.to, request.transportMode));
					results.setEmToStart(
							getJourneyCost(request.from, closestFrom.visit.location, request.transportMode));
					results.carShare.setTransport("Car Share");
					return results;
				}

			}	
		}
		
		return null;
	}
	
	/**
	 * @param from The location the journey starts at
	 * @param to The location that the journey ends at
	 * @return The cost of travelling between from and to
	 */
	protected double getJourneyCost(String from, String to, String transportMode) {
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.addReceiver(new AID("Map-Server", AID.ISLOCALNAME));
		msg.setConversationId("journey-request");
		msg.setContent(from + "," + to);
		myAgent.send(msg);

		ACLMessage response = myAgent.blockingReceive(MessageTemplate.MatchConversationId("journey-request"));
		JourneyData journeyData = null;
		try {
			ContentElement d = myAgent.getContentManager().extractContent(response);
			if (d instanceof GiveObjectPredicate) {
				journeyData = (JourneyData) ((GiveObjectPredicate) d).getData();
			}
		} catch (UngroundedException e) {
			e.printStackTrace();
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
		if (journeyData != null) {
			if(transportMode.equals("Car")){
				return journeyData.carEm;
			}else if(transportMode.equals("Public Transport")){
				return journeyData.ptEm;
			}
		}
		return 0;
	}
		
	// Allows for a custom class for checking time constraints and sending requests to the MapServer
	public void setJourneyInfoHelper(JourneyInfoHelper newHelper){
		this.journeyInfo = newHelper;
	}
	
	
	@Override
	public void action() {

		switch(currStep){
		case AddLanguages:
			// Register the Ontology for message content
			myAgent.getContentManager().registerLanguage(codec);
			myAgent.getContentManager().registerOntology(ontologyGive);
			currStep = STEP.Initialise;
			break;
			
		case Initialise:
			if(initialise()){
				if(journeyInfo == null)
					journeyInfo = new JourneyInfoHelper();
				
				currStep = STEP.WaitForRequests;
			}
			break;
			
		case WaitForRequests:
			ACLMessage msg = myAgent.receive(messageFilter);
			if(msg != null){
				// Handle offer messages
				if(offerTemplate.match(msg)){
					handleOffer(msg);

				// Handle request messages
				}else if(requestTemplate.match(msg)){
					handleRequest(msg);

				// Handle remove inform messages
				}else if(removeReqTemplate.match(msg)){
					removeOffersFrom(msg.getSender());
					
				// Handle depot inform messages from the auctioneer
				}else if(depotInfoTemplate.match(msg)){
					try {
						ContentElement d = myAgent.getContentManager().extractContent(msg);
						if (d instanceof GiveObjectPredicate) {
							depot = (Depot) ((GiveObjectPredicate) d).getData();
							offers.clear();
						}
					} catch (UngroundedException e) {
						e.printStackTrace();
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}

				// Handle car share acceptance messages
				}else if(acceptTemplate.match(msg)){
					if(msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL){
						CarShareResult bestShare = null;
						try {
							ContentElement d = myAgent.getContentManager().extractContent(msg);
							if (d instanceof GiveObjectPredicate) {
								bestShare = (CarShareResult) ((GiveObjectPredicate) d).getData();
							}
						} catch (UngroundedException e) {
							e.printStackTrace();
						} catch (CodecException e) {
							e.printStackTrace();
						} catch (OntologyException e) {
							e.printStackTrace();
						}
						bestShare.shareWith = msg.getSender();
						informCarShareAcceptance(bestShare);
					}
					
				}
				
			}else{
				block();
			}
			break;
		}		
	}

	@Override
	public boolean done() {
		return false;
	}

}
