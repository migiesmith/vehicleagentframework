package agent.auctionSolution.carShare;

import agent.auctionSolution.JourneyInfoHelper;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/**
 * The agent for the CarShareServerBehaviour
 * 
 * @author Grant
 */
public class CarShareServer extends Agent{

	protected void setup() {
		addBehaviour(new MyCarShareServerBehaviour());
		
		registerWithDF();
	}

	protected void registerWithDF() {
		// Register this Agent with the DF Service
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("car-share-server");
		sd.setName(getLocalName());
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}
	
	private class MyCarShareServerBehaviour extends CarShareServerBehaviour{

		@Override
		protected boolean initialise() {
			setJourneyInfoHelper(new JourneyInfoHelper());
			return true;
		}
		
	}
}
