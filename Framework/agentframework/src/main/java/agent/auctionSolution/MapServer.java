package agent.auctionSolution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;

import agent.auctionSolution.dataObjects.CostVariables;
import agent.auctionSolution.dataObjects.JourneyData;
import agent.auctionSolution.gui.MapServerGui;
import agent.auctionSolution.ontologies.GiveObjectPredicate;
import agent.auctionSolution.ontologies.GiveOntology;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Handles the management of the JourneyData. This can
 * receive requests for information such as JourneyData
 * from one visit to another, the costing variables, and 
 * for the x any y coordinates of a visit for rendering.
 * 
 * @author Grant
 */
@SuppressWarnings("serial")
public class MapServer extends Agent {

	// Initialise the language for message contents
	private Codec codec = new SLCodec();
	private Ontology ontologyGive = GiveOntology.getInstance();

	// The journey data required by the Visit agents
	Map<String, ArrayList<JourneyData>> data;
	// The Variables for costing the solutions
	CostVariables costVars;

	// The gui for the DataServer
	MapServerGui gui;
	
	// Initialise this class
	protected void setup() {
		System.out.println(getAID().getLocalName() + " has started.");

		// Register the Ontology for message content
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontologyGive);
		
		gui = new MapServerGui(this);
		
	}

	public void start() {
		// Responds to location requests
		addBehaviour(new JourneyRequestServer());
		gui.setStatus("Started.");
		
		registerWithDF();
	}

	protected void registerWithDF() {
		// Register this Agent with the DF Service
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Map-Server");
		sd.setName(getLocalName());
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (Exception e) {
		}
	}
	
	private class JourneyRequestServer extends CyclicBehaviour {

		// The template used for incoming messages
		private MessageTemplate journeyTemplate, coordinatesTemplate, costVarTemplate;

		JourneyRequestServer() {
			journeyTemplate = MessageTemplate.MatchConversationId("journey-request");
			coordinatesTemplate = MessageTemplate.MatchConversationId("visit-coordinates");
			costVarTemplate = MessageTemplate.MatchConversationId("cost-vars");
		}

		private JourneyData getData(String from, String to){			
			for(JourneyData d : data.get(from)){
				if(d.to.equals(to))
					return d;
			}			
			return null;
		}
		
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive(MessageTemplate.or(journeyTemplate, MessageTemplate.or(coordinatesTemplate, costVarTemplate)));
			if (msg != null) {
				if (journeyTemplate.match(msg)) {
					// Reply with the journey data for the sender
					ACLMessage reply = msg.createReply();
					reply.setLanguage(codec.getName());
					reply.setOntology(ontologyGive.getName());
					try {
						GiveObjectPredicate give = new GiveObjectPredicate();
						String[] fromAndTo = msg.getContent().split(",");
						JourneyData content = getData(fromAndTo[0], fromAndTo[1]);
						if (content == null) {
							System.out.println("ERROR: No journey data for the request: ("
									+ fromAndTo[0] +","+ fromAndTo[1] + "): "+msg.getSender());
						}
						gui.setStatus("journey-request: "+ fromAndTo[0] +","+ fromAndTo[1]);
						give.setData(content);
						getContentManager().fillContent(reply, give);
						send(reply);
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
				}else if(coordinatesTemplate.match(msg)){
					// Respond to the request with the x and y coordinates of the requested postcode
					ACLMessage reply = msg.createReply();
					JourneyData jd = data.get(msg.getContent()).get(0);
					reply.setContent(jd.getFromX()+","+jd.getFromY());				
					myAgent.send(reply);
					
				}else if(costVarTemplate.match(msg)){
					// Reply with the cost variables for the sender
					ACLMessage reply = msg.createReply();
					reply.setLanguage(codec.getName());
					reply.setOntology(ontologyGive.getName());
					try {
						GiveObjectPredicate give = new GiveObjectPredicate();
						gui.setStatus("Cost Variables Requested");
						give.setData(costVars);
						getContentManager().fillContent(reply, give);
						send(reply);
					} catch (CodecException e) {
						e.printStackTrace();
					} catch (OntologyException e) {
						e.printStackTrace();
					}
					
				}
			} else {
				block();
				gui.setStatus("idle.");
			}
		}

	}

	private void dataFromString(String content) {
		gui.setStatus("Parsing data.");
		
		// The regex for a UK postcode
		String postCodeRegex = "((GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2}))";
		String doubleRegex = "[-]{0,1}([0-9]+(\\.[0-9]+)*)";
		String delimeter = ",";

		Pattern p = Pattern.compile(// From
				postCodeRegex // Post Code
						+ delimeter + doubleRegex // X Location
						+ delimeter + doubleRegex // Y Location
		// To
						+ delimeter + postCodeRegex // Post Code
						+ delimeter + doubleRegex // X Location
						+ delimeter + doubleRegex // Y Location
		// Emissions and Time
						+ delimeter + doubleRegex // carEm
						+ delimeter + doubleRegex // carTime
						+ delimeter + doubleRegex // ptEm
						+ delimeter + doubleRegex // ptTime
						+ delimeter + doubleRegex
						+ delimeter + doubleRegex
		);

		// Create a matcher to find the matching lines
		Matcher m = p.matcher(content);
		// Initialise data for storing the journeys
		data = new HashMap<String, ArrayList<JourneyData>>();

		JourneyData journey;
		String[] lineContent;
		int sum = 0; // Sum is used to inform the user how many journies were
						// loaded
		while (m.find()) { // Matching line found
			// Parse the line
			lineContent = m.group().split(delimeter);
			journey = new JourneyData();
			journey.setAll(
				lineContent[0], // from
				Double.parseDouble(lineContent[1]), // from x
				Double.parseDouble(lineContent[2]), // from y
	
				lineContent[3], // to
				Double.parseDouble(lineContent[4]), // to x
				Double.parseDouble(lineContent[5]), // to y
	
				Double.parseDouble(lineContent[6]), // carEm
				Double.parseDouble(lineContent[7]), // carTime
	
				Double.parseDouble(lineContent[8]), // ptEm
				Double.parseDouble(lineContent[9]), // ptTime
	
				Double.parseDouble(lineContent[11]), // carDist
				Double.parseDouble(lineContent[11]) // TODO ptDist THIS IS USING CARDIST FOR NOW
					
			);

			// If there isn't an entry in the map for this postcode then create
			// one
			if (data.get(journey.from) == null)
				data.put(journey.from, new ArrayList<JourneyData>());

			// Store the JourneyData in the entry corresponding to the .from
			// value
			data.get(journey.from).add(journey);

			// Increment sum
			sum++;
		}
		
		// Load the cost variables
		costVars = new CostVariables();
		String[] lines = content.split("\n");
		for(int i = 0; i < lines.length; i++){
			if(lines[i].startsWith("StaffCostHour")){
				costVars.setStaffCostPerHour(Double.parseDouble(lines[i].split(",")[1]));
				
			}else if(lines[i].startsWith("FixedCostsCar")){
				costVars.setFixedCostsCar(Double.parseDouble(lines[i].split(",")[1]));
				
			}else if(lines[i].startsWith("FixedCostsPt")){
				costVars.setFixedCostsPt(Double.parseDouble(lines[i].split(",")[1]));
				
			}else if(lines[i].startsWith("CarCostsP_km")){
				costVars.setCarCostsPerKM(Double.parseDouble(lines[i].split(",")[1]));
				
			}
		}
		
		System.out.println(sum + " journeys loaded and stored into " + data.size() + " map entries.");
		gui.setStatus(sum + " journeys loaded and stored into " + data.size() + " map entries.");

	}

	// Open a file picker and select a data file to be used
	public void loadFromFile() {
		// Create a file chooser
		final JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
		// Set the title of the file chooser
		fc.setDialogTitle("Select the journey data CSV file");
		// In response to a button click:
		int returnVal = fc.showOpenDialog(null);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			System.out.println("Opening: " + file.getAbsolutePath() + ".");

			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();
				while (line != null) {
					sb.append(line);
					sb.append("\n");
					line = br.readLine();
				}
				br.close();
				dataFromString(sb.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			System.out.println("No file chosen.");
		}
	}

}
