package agent.auctionSolution;

import java.util.Calendar;

import agent.auctionSolution.dataObjects.CostVariables;
import agent.auctionSolution.dataObjects.Depot;
import agent.auctionSolution.dataObjects.JourneyData;
import agent.auctionSolution.dataObjects.Route;
import agent.auctionSolution.dataObjects.VisitData;
import agent.auctionSolution.dataObjects.carShare.CarShare;
import agent.auctionSolution.ontologies.GiveObjectPredicate;
import jade.content.ContentElement;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Helps acquire distance and time information about journeys from the Map-Server. 
 * This also allows a route to be checked if it is valid in terms of its time constraints.
 * @author Grant
 */
public class JourneyInfoHelper {
	
	/**
	 * @param agent The agent sending the request
	 * @param from The location the journey starts at
	 * @param to The location that the journey ends at
	 * @return The JourneyData between from and to
	 */
	public JourneyData getJourneyData(Agent agent, String from, String to){
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.addReceiver(getMapServerAID(agent));
		msg.setConversationId("journey-request");
		msg.setContent(from + "," + to);
		agent.send(msg);
		
		ACLMessage response = agent.blockingReceive(MessageTemplate.MatchConversationId("journey-request"));
		JourneyData journeyData = null;
		try {
			ContentElement d = agent.getContentManager().extractContent(response);
			if (d instanceof GiveObjectPredicate) {
				journeyData = (JourneyData) ((GiveObjectPredicate) d).getData();
			}
		} catch (UngroundedException e) {
			e.printStackTrace();
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}
		return journeyData;
	}

	// Request the MapServer for the cost variables
	public CostVariables getCostVariables(Agent agent){
		CostVariables content = null;

		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.addReceiver(getMapServerAID(agent));
		msg.setConversationId("cost-vars");
		agent.send(msg);

		ACLMessage response = agent.blockingReceive(MessageTemplate.MatchConversationId("cost-vars"));
		try {
			ContentElement d = agent.getContentManager().extractContent(response);
			if (d instanceof GiveObjectPredicate) {
				content = (CostVariables) ((GiveObjectPredicate) d).getData();
			}
		} catch (UngroundedException e) {
			e.printStackTrace();
		} catch (CodecException e) {
			e.printStackTrace();
		} catch (OntologyException e) {
			e.printStackTrace();
		}

		return content;
	}
	
	/**
	 * @param agent The agent sending the request
	 * @param from The location the journey starts at
	 * @param to The location that the journey ends at
	 * @param transportMode The mode of transport used between from and to
	 * @return The distance between from and to
	 */
	public double getDistance(Agent agent, String from, String to, String transportMode) {
		JourneyData journeyData = getJourneyData(agent, from, to);
		if (journeyData != null) {
			if(transportMode.equals("Car") || transportMode.equals("Car Share")){
				return journeyData.carDist;
			}else if(transportMode.equals("Public Transport")){
				return journeyData.ptDist;
			}
		}
		return 0;
	}	
	
	/**
	 * @param agent The agent sending the request
	 * @param from The location the journey starts at
	 * @param to The location that the journey ends at
	 * @param transportMode The mode of transport used between from and to
	 * @return The time to travel between from and to
	 */
	public double getTravelTime(Agent agent, String from, String to, String transportMode) {
		JourneyData journeyData = getJourneyData(agent, from, to);
		if (journeyData != null) {
			if(transportMode.equals("Car") || transportMode.equals("Car Share")){
				return journeyData.carTime;
			}else if(transportMode.equals("Public Transport")){
				return journeyData.ptTime;
			}
		}
		return 0;
	}
	
	/**
	 * @param agent The agent sending the request
	 * @param route The route being checked
	 * @param depot The depot for the problem
	 * @return If the route is valid true otherwise false.
	 */
	public boolean validTime(Agent agent, Route route, Depot depot){
		// Create a calendar and set the time to track the time that it would be
		// relative to the visits
		Calendar cal = Calendar.getInstance();
		cal.setTime(depot.commenceTime);//cal.setTime(tempRoute.visits.firstElement().windowStart);
		cal.add(Calendar.MINUTE, (int) getTravelTime(agent, depot.location, route.visits.get(0).location, route.visits.get(0).transport));
		for(int i = 0; i < route.visits.size(); i++){
			VisitData vd = route.visits.get(i);

			if (cal.getTime().before(vd.windowStart)) {
				// If this is the first visit then set the calendar to the start window
				if(i == 0){
					cal.setTime(route.visits.get(0).windowStart);
				}else{
					return false;
				}
			}
						
			// If the windowEnd is before the time that the route will be at
			// then this is not a valid merge
			if (cal.getTime().after(vd.windowEnd)) {
				return false;
			}
			
			// Add the time this visit takes
			cal.add(Calendar.MINUTE, (int) vd.visitLength);
			if(vd instanceof CarShare){
				cal.add(Calendar.MINUTE, (int) getTravelTime(agent, ((CarShare) vd).endLocation, (i+1 == route.visits.size() ? depot.location : route.visits.get(i+1).location), "Car"));	
			}else{
				cal.add(Calendar.MINUTE, (int) getTravelTime(agent, vd.location, (i+1 == route.visits.size() ? depot.location : route.visits.get(i+1).location), vd.transport));	
			}		
		}

		return true;
	}
	
	// Returns the AID for the map server registered with the DF Service using a service type of "Map-Server"
	public AID getMapServerAID(Agent agent){
		// Setup the search parameters
		DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd  = new ServiceDescription();
        sd.setType("Map-Server");
        dfd.addServices(sd);
        
		try {
			// Search for the Map-Server and return its AID
	        DFAgentDescription[] searchResult = DFService.search(agent, dfd);
	        while(searchResult.length < 1){
	        	searchResult = DFService.search(agent, dfd);
	        	try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	        }
	        return searchResult[0].getName();
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		// If we couldn't find it, return null
		return null;
	}
	
}



