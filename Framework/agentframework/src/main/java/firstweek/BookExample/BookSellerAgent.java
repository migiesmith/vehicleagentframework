package firstweek.BookExample;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class BookSellerAgent extends Agent {

	private Map<String, PriceManager> catalogue;
	
	private BookSellerGui myGui;
	
	protected void setup(){
		catalogue = new Hashtable<String, PriceManager>();
		
		myGui = new BookSellerGuiImpl();
		myGui.setAgent(this);
		myGui.show();
		
		addBehaviour(new OfferRequestsServer());

	    // Register the book-selling service in the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Book-selling");
		sd.setName(getLocalName() + "-Book-selling");
		dfd.addServices(sd);
		try{
			DFService.register(this, dfd);
		}catch(FIPAException fe){
			fe.printStackTrace();
		}
	}
	
	protected void takeDown(){
		if(myGui != null)
			myGui.dispose();
		
		System.out.println("Seller-agent "+getAID().getName()+" terminating.");
	
		// Deregister from the yellow pages
		try{
			DFService.deregister(this);
		}catch(FIPAException fe){
			fe.printStackTrace();
		}
	}
	
	public void putForSale(String title, int initPrice, int minPrice, Date deadline) {
		addBehaviour(new PriceManager(this, title, initPrice, minPrice, deadline));
	}
		
	private class PriceManager extends TickerBehaviour{
		private String title;
		private int minPrice, currentPrice, initPrice, deltaPrice;
		private long initTime, deadline, deltaTime;
		
		private PriceManager(Agent a, String title, int initPrice, int minPrice, Date deadline){
			super(a, 3000);
			this.title = title;
			this.initPrice = initPrice;
			this.currentPrice = initPrice;
			this.deltaPrice = initPrice - minPrice;
			this.deadline = deadline.getTime();
			this.initTime = System.currentTimeMillis();
			this.deltaTime = ((this.deadline - this.initTime) > 0 ? (this.deadline - this.initTime) : 3000);
		}
		
		public void onStart(){
			catalogue.put(title, this);
			super.onStart();
		}
		
		public void onTick(){
			long currentTime = System.currentTimeMillis();
			if(currentTime > deadline){
				myGui.notifyUser("Cannot sell book " + title);
				catalogue.remove(title);
				stop();
			}else{
				// Compute current price
				long elapsedTime = currentTime - initTime;
				currentPrice = Math.round(initPrice - 1.0f * deltaPrice * (1.0f * elapsedTime / deltaTime));
			}
		}

	    public int getCurrentPrice() {
	      return currentPrice;
	    }
	}

	private class OfferRequestsServer extends CyclicBehaviour {
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive();
			if(msg != null){
				String title = msg.getContent();
				ACLMessage reply = msg.createReply();
				
				PriceManager pm = catalogue.get(title);
				if(pm != null){
					reply.setPerformative(ACLMessage.PROPOSE);
					reply.setContent(String.valueOf(pm.getCurrentPrice()));
				}else{
					reply.setPerformative(ACLMessage.REFUSE);
					reply.setContent("not-available");
				}
				myAgent.send(reply);
		        myGui.notifyUser(pm != null ? "Sent Proposal to sell at "+reply.getContent() : "Refused Proposal as the book is not for sale");
			}else{
				block();
			}
		}
	}
	
}
