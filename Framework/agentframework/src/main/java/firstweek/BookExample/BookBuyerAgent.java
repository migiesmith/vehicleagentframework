package firstweek.BookExample;

import java.util.Date;
import java.util.Vector;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class BookBuyerAgent extends Agent {

	private Vector<AID> sellerAgents = new Vector();
	
	private BookBuyerGui myGui;
	
	protected void setup(){
	    System.out.println("Buyer-agent "+getAID().getName()+" is ready.");
	    
	    Object[] args = getArguments();
	    if(args != null && args.length > 0){
	    	for(int i = 0; i < args.length; i++){
	    		AID seller = new AID((String) args[i], AID.ISLOCALNAME);
	    		sellerAgents.addElement(seller);
	    	}
	    }
	    
	    myGui = new BookBuyerGuiImpl();
	    myGui.setAgent(this);
	    myGui.show();
	    
	    addBehaviour(new TickerBehaviour(this, 3000){
	    	protected void onTick(){
	    		DFAgentDescription template = new DFAgentDescription();
	    		ServiceDescription sd = new ServiceDescription();
	    		sd.setType("Book-selling");
	    		template.addServices(sd);
	    		try{
	    			DFAgentDescription[] results = DFService.search(myAgent, template);
	    			sellerAgents.clear();
	    			for(int i = 0; i < results.length; i++){
	    				sellerAgents.addElement(results[i].getName());
	    			}
	    		}catch(FIPAException fe){
	    			fe.printStackTrace();
	    		}
	    	}
	    });
	    
	}
	
	protected void takeDown(){
		if(myGui != null)
			myGui.dispose();
		
		System.out.println("Buyer-agent " + getAID().getName() + " terminated.");
	}

	public void purchase(String title, int maxPrice, Date deadline) {
		// the following line is in the book at page 62
		addBehaviour(new PurchaseManager(this, title, maxPrice, deadline));
	}
	
	public void setCreditCard(String creditCarNumber) {	}
	
	private class PurchaseManager extends TickerBehaviour{
		private String title;
		private int maxPrice, startPrice;
		private long deadline, initTime, deltaTime;
		
		private PurchaseManager(Agent a, String title, int maxPrice, Date date){
			super(a, 3000);
			this.title = title;
			this.maxPrice = maxPrice;
			this.deadline = date.getTime();
			this.initTime = System.currentTimeMillis();
			this.deltaTime = deadline - initTime;
		}
		
		public void onTick(){
			long currentTime = System.currentTimeMillis();
			if(currentTime > deadline){
				myGui.notifyUser("Cannot buy book " + title);
				stop();
			}else{
				long elapsedTime = currentTime - initTime;
				int acceptablePrice = (int)Math.round(1.0 * maxPrice * (1.0 * elapsedTime / deltaTime));
				myAgent.addBehaviour(new BookNegotiator(title, acceptablePrice, this));
			}
		}
		
	}
	
	private class BookNegotiator extends Behaviour{
		private String title;
		private int maxPrice;
		private PurchaseManager manager;
		private AID bestSeller;
		private int bestPrice;
		private int repliesCnt = 0;
		private MessageTemplate mt;
		private int step = 0;
		
		public BookNegotiator(String title, int maxPrice, PurchaseManager manager){
			super(null);
			this.title = title;
			this.maxPrice = maxPrice;
			this.manager = manager;
		}
		
		public void action(){
			switch(step){
			case 0:{ // Request proposals
				ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
				for(int i = 0; i < sellerAgents.size(); i++){
					cfp.addReceiver(sellerAgents.get(i));
				}
				cfp.setContent(title);
				cfp.setConversationId("book-trade");
				cfp.setReplyWith("cfp" + System.currentTimeMillis());
				myAgent.send(cfp);
				myGui.notifyUser("Sent Call for Proposal");
				
				mt = MessageTemplate.and(
						MessageTemplate.MatchConversationId("book-trade"),
						MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
				step = 1;
				break;
				}
			case 1:{ // Receive Proposals
				ACLMessage reply = myAgent.receive(mt);
				if(reply != null){
					if(reply.getPerformative() == ACLMessage.PROPOSE){
						int price = Integer.parseInt(reply.getContent());
						myGui.notifyUser("Received Proposal at "+price+" when maximum acceptable price was "+maxPrice);
						if(bestSeller == null || price < bestPrice){
							bestPrice = price;
							bestSeller = reply.getSender();
						}
					}
					repliesCnt++;
					if(repliesCnt >= sellerAgents.size()){
						step = 2;
					}
				}else{
					block();
				}
				break;
				}
			case 2:{ // Pick the best offer
				if(bestSeller != null && bestPrice <= maxPrice){
					ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
					order.addReceiver(bestSeller);
					order.setContent(title);
					order.setConversationId("book-trade");
					order.setReplyWith("order"+System.currentTimeMillis());
					myAgent.send(order);
					myGui.notifyUser("Sent Accept Proposal");
					
					mt = MessageTemplate.and(
							MessageTemplate.MatchConversationId("book-trade"),
							MessageTemplate.MatchReplyWith(order.getReplyWith()));
					step = 3;
				}else{
					// No acceptable proposal
					step = 4;
				}
				break;
				}
			case 3:{ // Receive the purchase order
				ACLMessage reply = myAgent.receive(mt);
				if(reply != null){
					if(reply.getPerformative() == ACLMessage.INFORM){
						myGui.notifyUser("Book "+title+" successfully purchased. Price = "+bestPrice);
					}
					step = 4;
				}else{
					block();
				}
				break;
				}
			}
		}

	    public boolean done() {
	      return step == 4;
	    }
	    
	}
	
}
