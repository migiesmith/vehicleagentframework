package firstweek;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Vector;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public abstract class MapData extends Agent{

	/**
	 * The Vector containing all of the map's nodes
	 */
	protected Vector nodes;
	private MessageTemplate template;

	protected void setup(){
		nodes = new Vector();
		template = MessageTemplate.or(
				MessageTemplate.MatchConversationId("request-data-all"),
				MessageTemplate.MatchConversationId("request-data-range")			
				);
		
		addBehaviour(new RequestResponder());

	}
		
	private class RequestResponder extends CyclicBehaviour {
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive(template);
			if (msg != null) {
				ACLMessage reply = msg.createReply();
				try {
					if (msg.getConversationId().equals("request-data-all")) {
						reply.setContentObject(nodes);
					} else {
						String[] content = msg.getContent().split(" ");

						if (content.length >= 2) {
							System.out.println(content[0] + " " + content[1]);
							reply.setContentObject((Serializable) new ArrayList(nodes.subList(Integer.parseInt(content[0]),
									Integer.parseInt(content[1]))));
						} else if (content.length >= 1) {
							reply.setContentObject((Serializable) nodes.get(Integer.parseInt(content[0])));
						} else {
							reply.setPerformative(ACLMessage.FAILURE);
						}
					}
					myAgent.send(reply);
					
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				block();
			}
		}
	}
	
}
