package firstweek.clarkeWright2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import firstweek.ClarkeWright.CWVerifier;
import firstweek.ClarkeWright.Node;
import firstweek.ClarkeWright.NodeOrderingImprover;
import firstweek.ClarkeWright.SavingsNode;
import firstweek.ClarkeWright.VRProblem;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;



public class Controller extends Agent implements HasRoutes{

	// The nodes to distribute amongst the routing agents
	protected Vector<Node> nodes;
	// Stores for the problem loaded from a file
	public VRProblem prob;
	
	// The final routes
	private Vector<Vector<Node>> routes = new Vector<Vector<Node>>();
	
	private int NODE_COUNT = 0;
	
	private CWGui myGui;
	
	public Vector<Vector<Node>> getRoutes(){
		return new Vector<Vector<Node>>(routes);
	}
	public Node getDepot(){
		return prob.depot;
	}
	
	protected void setup(){
		// Create empty vectors
		nodes = new Vector<Node>();


		// Load the problem
		try {
			//Create a file chooser
			final JFileChooser fc = new JFileChooser("Test Data\\");
			fc.setFileFilter(new FileFilter(){

				@Override
				public boolean accept(File f) {
					if(f.getName().toUpperCase().endsWith(".CSV"))
						return true;
					
					return false;
				}

				@Override
				public String getDescription() {
					return null;
				}
				
			});

			//In response to a button click:
			int returnVal = fc.showOpenDialog(null);
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				System.out.println("Opening: " + file.getAbsolutePath() + ".");
				prob = new VRProblem(file.getAbsolutePath());
			} else {
				System.out.println("No file chosen, closing..");
				System.exit(0);
			}
			
			nodes = prob.customers;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Add the behaviours
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Routing-Agent");
		dfd.addServices(sd);
		
		addBehaviour(new AccumulateResultsServer()); // Distributes the nodes amongst the routing agents
	
		
		// Add the agents
		for(int i = 0; i < prob.size(); i++){
			addAgent();
		}
		
		myGui = new CWGui("One agent per node (each agent works out its own savings)");
		myGui.setAgent(this);
		
	}
	
	// Dynamically adds agents as required
	private void addAgent(){
		try {
		System.out.println("Creating new agent..");
			getContainerController().createNewAgent("Routing-Node"+NODE_COUNT, CW2RoutingAgent.class.getCanonicalName(), new Object[] { prob.depot, prob.size() })
					.start();
			NODE_COUNT++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	// Distributes nodes amongst the registered routing agents
	private class AccumulateResultsServer extends CyclicBehaviour {
		
		private int step = 0; // 0 = init agents, 1 = get responses, 2 = combine reponses

		private DFAgentDescription agentDesc;
		private MessageTemplate responseTemplate;
		private Vector<AID> agents = new Vector<AID>(NODE_COUNT);
		
		private int responses = 0;
		
		private Vector<SavingsNode>[] savingsArray;
		private Vector<SavingsNode> savings = new Vector<SavingsNode>();

		AccumulateResultsServer(){
			agentDesc = new DFAgentDescription();
    		ServiceDescription sd = new ServiceDescription();
    		sd.setType("Routing-Agent");
    		agentDesc.addServices(sd);
			
    		responseTemplate = MessageTemplate.MatchConversationId("Controller-Report-Back");
    		
    		
		}

		private void getAgents(){
    		try{
    			// Search for the agents using the template
    			SearchConstraints s = new SearchConstraints();
    			s.setMaxResults((long) -1);
    			    			
    			DFAgentDescription[] results = DFService.search(myAgent, agentDesc, s);
    			// Clear the Vector of routing agents and refill it with the results
    			agents.clear();
    			if(results.length > 0){
	    			for(int i = 0; i < results.length; i++){
	    				if(results[i].getName() != getAID())agents.addElement(results[i].getName());
	    			}
    			}
    		}catch(FIPAException fe){
    			fe.printStackTrace();
    		}
		}
		
		// returns a route where n is the last node in a vector
		private Vector<Node> routeWhereCustomerIsLast(Node n){
			for(Vector<Node> r : routes){
				if(r.firstElement().equals(n)) return r;
			}
			return null;
		}

		// returns a route where n is the first element in a vector
		private Vector<Node> routeWhereCustomerIsFirst(Node n){
			for(Vector<Node> r : routes){
				if(r.firstElement().equals(n)){
					return r;
				}
			}
			return null;
		}

		// Returns the capacity of the Agent
		private double getCurrentCapacity(Vector<Node> nodes){
			double total = 0.0d;
			Iterator<Node> it = nodes.iterator();
			while(it.hasNext()){
				total += it.next().cost;
			}
			return total;
		}
		
		private void mergeRoutes(Vector<Node> route0, Vector<Node> route1){
			route0.addAll(route1);
		}
		
		// Clarke Wright solver function
		public void solve(){
			for(Node n : nodes){
				Vector<Node> route = new Vector<Node>();
				route.addElement(n);
				routes.addElement(route);
			}

			// Loop through each saving
			for (SavingsNode savingsNode : savings) {
				// Get the route where ci is the first customer
				Vector<Node> route0 = routeWhereCustomerIsLast(savingsNode.ci);
				if(route0 != null){ // check if we found a route for route0
					// Get the route where cj is the first customer
					Vector<Node> route1 = routeWhereCustomerIsFirst(savingsNode.cj);
					if(route1 != null){ // check if we found a route for route1
						if(route0 == route1){ continue;} // if route0 and route1 are the same, do nothing
						if (getCurrentCapacity(route0) + getCurrentCapacity(route1) <= prob.depot.cost) { // if merge is feasible
							// Merge the two routes
							routes.remove(route1);
							mergeRoutes(route0, route1);
						}
					}
				}
			}
			for(Vector<Node> route : routes){
				if(route.size() > 1)
					NodeOrderingImprover.improve(route, prob.depot);
			}
		}
		
		@Override
		public void action() {
			switch(step){
				case 0:{
					getAgents();
					//System.out.println(agents.size());
					if(agents.size() < NODE_COUNT)
						return;
					System.out.println("There are " + agents.size() + " agents.");
					for(int i = 0; i < agents.size(); i++){
						// Send the agent their node
						ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
						msg.addReceiver(agents.get(i));
						msg.setConversationId("Start-Saving-Calc");
						try {
							msg.setContentObject(nodes.get(i));
							msg.addUserDefinedParameter("agent-id", String.valueOf(i));
						} catch (IOException e) {
							e.printStackTrace();
						}
						myAgent.send(msg);
						
						// Send message to respond once done
						msg = new ACLMessage(ACLMessage.REQUEST);
						msg.addReceiver(agents.get(i));
						msg.setConversationId("Controller-Report-Back");
						myAgent.send(msg);
					}
					System.out.println("Sent all messages to agents");
					savingsArray = (Vector<SavingsNode>[]) new Vector[NODE_COUNT];
					step = 1;
					break;
				}
				case 1:{
					// Accept all messages that match the template
					ACLMessage response = myAgent.receive(responseTemplate);
					if (response != null) {
						try {
							int myID = Integer.parseInt(response.getUserDefinedParameter("agent-id"));
							savingsArray[myID] = (Vector<SavingsNode>)response.getContentObject();
						} catch (UnreadableException e) {
							e.printStackTrace();
						}
						// Increment the replies counter until we have then all
						responses++;
						if(responses >= agents.size()){
							step = 2; // Move to the next step
						}
					}else{
						block();
					}
					break;
				}
				case 2:{
					for(int i = 0; i < savingsArray.length; i++){
						savings.addAll(savingsArray[i]);
					}
					savingsArray = null;
					Collections.sort(savings);
					solve();
					
					// Print out the accumulated results in SVG format
					try {
						writeSVG(routes, "results/prob.svg", "results/solu.svg");
					} catch (Exception e) {
						e.printStackTrace();
					}
	
					double cost = 0;
					for (Vector<Node> route : routes) {
						Node prev = prob.depot;
						for (Node n : route) {
							cost += prev.distance(n);
							prev = n;
						}
						// Add the cost of returning to the depot
						cost += prev.distance(prob.depot);
					}
					System.out.println("This Solution Cost " + cost + ".");
					System.out.println(CWVerifier.verify(getRoutes(), prob.depot, nodes) ? "Valid solution" : "Invalid solution");
					
					doDelete();
					break;
				}
			}
		}
				
	}
	
    // Output the routes in SVG format to show a visual representation of the results
    public void writeSVG(Vector<Vector<Node>> routes, String probFilename,String solnFilename) throws Exception{
		String[] colors = "chocolate cornflowerblue crimson cyan darkblue darkcyan darkgoldenrod".split(" ");
		int colIndex = 0;
		String hdr = 
				"<?xml version='1.0'?>\n"+
						"<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' '../../svg11-flat.dtd'>\n"+
						"<svg width='8cm' height='8cm' viewBox='0 0 500 500' xmlns='http://www.w3.org/2000/svg' version='1.1'>\n";
		String ftr = "</svg>";
		StringBuffer psb = new StringBuffer();
		StringBuffer ssb = new StringBuffer();
		psb.append(hdr);
		ssb.append(hdr);
		for(Vector<Node> route : routes){
			ssb.append(String.format("<path d='M%s %s ",prob.depot.x,prob.depot.y));
			for(Node c : route)
				ssb.append(String.format("L%s %s",c.x,c.y));
			ssb.append(String.format("z' stroke='%s' fill='none' stroke-width='2'/>\n",
					colors[colIndex++ % colors.length]));
		}
		
		for(Node c:nodes){
			String disk = String.format(
					"<g transform='translate(%.0f,%.0f)'>"+
							"<circle cx='0' cy='0' r='%d' fill='pink' stroke='black' stroke-width='1'/>" +
							"<text text-anchor='middle' y='5'>%.0f</text>"+
							"</g>\n", 
							c.x,c.y,10,c.cost);
			psb.append(disk);
			ssb.append(disk);
		}
		String disk = String.format("<g transform='translate(%.0f,%.0f)'>"+
				"<circle cx='0' cy='0' r='%d' fill='pink' stroke='black' stroke-width='1'/>" +
				"<text text-anchor='middle' y='5'>%s</text>"+
				"</g>\n", prob.depot.x, prob.depot.y,20,"D");
		psb.append(disk);
		ssb.append(disk);
		psb.append(ftr);
		ssb.append(ftr);
		PrintStream ppw = new PrintStream(new FileOutputStream(probFilename));
		PrintStream spw = new PrintStream(new FileOutputStream(solnFilename));
		ppw.append(psb);
		spw.append(ssb);
		ppw.close();
		spw.close();
	}
    
}
