package firstweek.clarkeWright2;

import java.util.Vector;

import firstweek.ClarkeWright.Node;

public interface HasRoutes {
	
	public void doDelete();
	public Vector<Vector<Node>> getRoutes();
	public Node getDepot();
	
}
