package firstweek.clarkeWright2;

import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import firstweek.RoutingAgent;
import firstweek.ClarkeWright.Node;
import firstweek.ClarkeWright.SavingsNode;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class CW2RoutingAgent extends RoutingAgent {

	private Node depot;
	private Node myNode = null;
	private int myID = -1;
	Vector<SavingsNode> savings = new Vector<SavingsNode>();
	Vector<AID> agents = new Vector<AID>();
	
	private int addAt = 0;
	
	
	@Override
	protected void setup() {

		if(getArguments().length == 0){
			doDelete();
			return;
		}
		
		MAXIMUM_CAPACITY = 50.0d;
		
		// Setup the nodes Vector and the depot
		nodes = new Vector<Node>();
		depot = (Node) getArguments()[0];
		for(int i = 0; i < (Integer)getArguments()[1]; i++){
			agents.addElement(new AID("Routing-Node"+i, AID.ISLOCALNAME));
		}
		
		// Add the bidding behaviour
		addBehaviour(new SavingEstimator());
		
		// Register the RoutingAgent in the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Routing-Agent");
		sd.setName(getLocalName() + "-Routing-Agent");
		dfd.addServices(sd);
		try{
			DFService.register(this, dfd);
		}catch(FIPAException fe){
			fe.printStackTrace();
		}

		System.out.println("TestMyAgent "+getAID().getName()+" ready.");
	}
	
	protected void takeDown(){
		try {
			DFService.deregister(this);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
	}
	
	// Calculate the savings of adding Node n into this agent's list of nodes
	private double calcSaving(Node n){
		// Calculated using Clarke Wright
		return (depot.distance(myNode) + depot.distance(n)) - myNode.distance(n);
	}
	
	private class SavingEstimator extends CyclicBehaviour{

		private int step = 0; // 0 = waiting, 1 = sending/responding, 2 = return results
		private int evaluationsMade = 0;
		private int agentToSendTo = 0;
		
		DFAgentDescription agentDesc;
		MessageTemplate startTemplate, agentMsgTemplate, reportToControllerTemplate;
		
		SavingEstimator(){
			agentDesc = new DFAgentDescription();
    		ServiceDescription sd = new ServiceDescription();
    		sd.setType("Routing-Agent");
    		agentDesc.addServices(sd);
    		
    		
    		// Message templates
    		startTemplate = MessageTemplate.MatchConversationId("Start-Saving-Calc");
    		agentMsgTemplate = MessageTemplate.MatchConversationId("Node-Eval");
    		reportToControllerTemplate = MessageTemplate.MatchConversationId("Controller-Report-Back");
		}
		
		private void getAgents(){
    		try{
    			// Search for the agents using the template
    			DFAgentDescription[] results = DFService.search(myAgent, agentDesc);
    			// Clear the Vector of routing agents and refill it with the results
    			agents.clear();
    			if(results.length > 0){
	    			for(int i = 0; i < results.length; i++){
	    				if(results[i].getName() != getAID())agents.addElement(results[i].getName());
	    			}
    			}
    		}catch(FIPAException fe){
    			fe.printStackTrace();
    		}
		}
		
		@Override
		public void action() {

			switch(step){
				case 0:{
					ACLMessage msg = myAgent.receive(startTemplate);
					if (msg != null) {
						try {
							myNode = (Node)msg.getContentObject();
							myID = Integer.parseInt(msg.getUserDefinedParameter("agent-id"));
							step = 1;
						} catch (UnreadableException e) {
							e.printStackTrace();
						}
					} else {
						block();
					}
					break;
					}
				case 1:{
					//getAgents();


					if(agentToSendTo < agents.size()){
						ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
						request.addReceiver(agents.get(agentToSendTo));
						request.setConversationId("Node-Eval");
						try {
							request.setContentObject(myNode);
						} catch (IOException e) {
							e.printStackTrace();
						}
						myAgent.send(request);
						agentToSendTo++;
					}
					
					ACLMessage msg = myAgent.receive(agentMsgTemplate);
					if(msg != null){
						try {
							Node n = (Node)msg.getContentObject();
							double s = calcSaving(n);
							SavingsNode saving = new SavingsNode(myNode, n, s);
							if(saving.saving > 0)
								savings.addElement(saving);
						} catch (UnreadableException e) {
							e.printStackTrace();
						}												
						evaluationsMade++;
					}else{
						block();
					}
					

					if(evaluationsMade == agents.size() && agentToSendTo == agents.size())
						step = 2;
					
					break;
				}
				case 2:{
					ACLMessage msg = myAgent.receive(reportToControllerTemplate);
					if (msg != null) {
						ACLMessage reply = msg.createReply();
						try {
							reply.setContentObject(savings);
							reply.addUserDefinedParameter("agent-id", String.valueOf(myID));
						} catch (IOException e) {
							e.printStackTrace();
						}
						myAgent.send(reply);
						doDelete();
					} else {
						block();
					}
					break;
				}
			}
			
		}
		
		
	}

	@Override
	protected double getCurrentCapacity() {
		return 0;
	}

}