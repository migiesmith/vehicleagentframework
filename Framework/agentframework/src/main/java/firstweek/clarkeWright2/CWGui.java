package firstweek.clarkeWright2;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import firstweek.ClarkeWright.Node;

public class CWGui extends JFrame {

	private HasRoutes myAgent;
    private Timer timer = null;
	
	public CWGui(String title){
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				myAgent.doDelete();
			}
		} );

		JPanel rootPanel = new JPanel();
		rootPanel.setLayout(new GridBagLayout());
        JPanel panel=new JPanel();
        getContentPane().add(panel);
        setSize(550, 550);
        setVisible(true);
        setTitle(title);
        
        
        timer = new Timer(1000, new ActionListener(){      // Timer 4 seconds
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        });
        timer.start();
	}
	

    public void paint(Graphics g) {
    	//super.paint(g);

    	
    	BufferedImage b = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);

    	
        Graphics2D g2 = (Graphics2D) b.getGraphics();
    	FontMetrics fm = g2.getFontMetrics();
        Insets insets = this.getInsets();
        g2.translate(insets.left, insets.top);
        g2.drawRect(0, 0, 640, 480);
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getWidth(), getHeight());
    	
    	g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    	g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    	g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
    	g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    	g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    	g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    	g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);    	
        
        // Depot
        Node depot = myAgent.getDepot();

        Vector<Vector<Node>> routes = myAgent.getRoutes();
        Iterator<Vector<Node>> rit = routes.iterator();
        Node prev;
        while(rit.hasNext()){
        	Vector<Node> route = rit.next();
            Iterator<Node> nit = route.iterator();
            prev = depot;
        	g2.setColor(new Color(Math.min(255, (int)route.firstElement().x), Math.min(255,(int)route.firstElement().y), Math.min(255,(int)route.firstElement().cost)));
            while(nit.hasNext()){
            	Node n = nit.next();
            	g2.drawLine((int)n.x, (int)n.y, (int)prev.x, (int)prev.y);
            	prev = n;
            }
        	g2.drawLine((int)depot.x, (int)depot.y, (int)prev.x, (int)prev.y);
        }

    	int radius = 10;
    	g2.setColor(Color.ORANGE);
    	g2.fillOval((int)depot.x - radius, (int)depot.y - radius, radius*2, radius*2);
    	g2.setColor(Color.DARK_GRAY);
    	g2.drawOval((int)depot.x - radius, (int)depot.y - radius, radius*2, radius*2);
    	g2.setColor(Color.BLACK);
    	Rectangle2D depotRect = fm.getStringBounds(String.valueOf((int)depot.cost), g2);
    	g2.drawString(String.valueOf((int)depot.cost), (int)(depot.x - depotRect.getWidth()/2), (int)(depot.y+depotRect.getHeight()/2));
        

        routes = myAgent.getRoutes();
        rit = routes.iterator();
        while(rit.hasNext()){
        	Vector<Node> route = rit.next();
            Iterator<Node> nit = route.iterator();
            while(nit.hasNext()){
            	Node n = nit.next();
            	g2.setColor(Color.ORANGE);
            	g2.fillOval((int)n.x - radius, (int)n.y - radius, radius*2, radius*2);
            	g2.setColor(Color.DARK_GRAY);
            	g2.drawOval((int)n.x - radius, (int)n.y - radius, radius*2, radius*2);
            	g2.setColor(Color.BLACK);
            	String val = String.valueOf((int)n.cost);
            	Rectangle2D r = fm.getStringBounds(val, g2);
            	g2.drawString(val, (int)(n.x - r.getWidth()/2), (int)(n.y+r.getHeight()/2));
            }
        }

		double cost = 0;
		for (Vector<Node> route : routes) {
			prev = depot;
			for (Node n : route) {
				cost += prev.distance(n);
				prev = n;
			}
			// Add the cost of returning to the depot
			cost += prev.distance(depot);
		}
		String val = "Cost: "+cost;
    	Rectangle2D r = fm.getStringBounds(val, g2);
		g2.drawString(val, (int)(getWidth() - r.getWidth() - insets.left), (int)(getHeight() - r.getHeight() - insets.top));

        g.drawImage(b, 0, 0, null);
    }
	
	public void setAgent(HasRoutes a){
		this.myAgent = a;
	}

	void notifyUser(String message){
		
	}
}
