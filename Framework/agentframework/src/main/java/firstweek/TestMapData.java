package firstweek;

import java.io.BufferedReader;
import java.io.FileReader;

import firstweek.ClarkeWright.Node;

public class TestMapData extends MapData {

	@Override
	protected void setup() {
		super.setup();
		
		try {
			readIn("E://Users//Graham//Desktop//uni//Internship//Framework//agentframework//Test Data//rand00010prob.csv");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("TestMapData "+getAID().getName()+" ready.");

	}
	
	public void readIn(String filename) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String s;
		this.nodes.clear();
		while((s=br.readLine())!=null){
			String [] xycTriple = s.split(",");
			for(int i=0;i<xycTriple.length;i+=3)
				nodes.addElement(new Node(
						(int)Double.parseDouble(xycTriple[i]),
						(int)Double.parseDouble(xycTriple[i+1]),
						(int)Double.parseDouble(xycTriple[i+2])));
		}
		br.close();
		System.out.println(nodes);
	}
	

}
