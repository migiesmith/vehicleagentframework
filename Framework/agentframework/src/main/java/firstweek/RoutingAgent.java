package firstweek;


import java.util.Vector;

import firstweek.ClarkeWright.Node;
import jade.core.AID;
import jade.core.Agent;

public abstract class RoutingAgent extends Agent{

	protected Vector<Node> nodes;
	protected double MAXIMUM_CAPACITY;
	
	@Override
	protected abstract void setup();
	protected abstract double getCurrentCapacity();
	
}
