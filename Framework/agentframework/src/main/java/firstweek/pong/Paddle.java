package firstweek.pong;

import java.awt.Rectangle;
import java.awt.geom.Point2D;

import firstweek.pong.Board.STATE;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public abstract class Paddle extends Agent {

	protected Point2D.Double position = new Point2D.Double(0,0);
	protected Point2D.Double velocity = new Point2D.Double(0,0);

	protected int WIDTH, HEIGHT;
	
	private AID board;
	
	private long lastFrame;
	
	@Override
	protected void setup() {
		System.out.println("Started Paddle.");
		
		board = new AID("board", AID.ISLOCALNAME);

		addBehaviour(new PaddleUpdater());
		
	}
	
	public abstract void movementLogic(double x, double y);

	private class PaddleUpdater extends CyclicBehaviour {

		private STATE state = STATE.SETUP;

		private MessageTemplate setupTemplate, updateTemplate, scoreTemplate, collisionTemplate;
		
		
		PaddleUpdater(){
			setupTemplate = MessageTemplate.MatchConversationId("Game-Setup");
			updateTemplate = MessageTemplate.MatchConversationId("Game-Update");
			scoreTemplate = MessageTemplate.MatchConversationId("Game-Score");
			collisionTemplate = MessageTemplate.MatchConversationId("Collision-Check");
		}
		
		private void handleSetup() {
			ACLMessage msg = myAgent.receive(setupTemplate);
			if(msg != null){
				String[] content = msg.getContent().split(" ");
				int x = Integer.parseInt(content[0]);
				int y = Integer.parseInt(content[1]);
				WIDTH = Integer.parseInt(content[2]);
				HEIGHT = Integer.parseInt(content[3]);
				position.setLocation(x, y);
				velocity.setLocation(0,0);
				
				lastFrame = System.currentTimeMillis();
				state = STATE.PLAY;
			}else{
				block();
			}
		}
		
		private void move(double deltaTime){
			position.x += velocity.x * deltaTime;
			position.y += velocity.y * deltaTime;
		}

		Point2D.Double LineIntersection(Point2D.Double ps0, Point2D.Double pe0, Point2D.Double ps1, Point2D.Double pe1){
				  // Get A,B,C of first line - points : ps1 to pe1
				  double A1 = pe0.y-ps0.y;
				  double B1 = ps0.x-pe0.x;
				  double C1 = A1*ps0.x+B1*ps0.y;
				 
				  // Get A,B,C of second line - points : ps2 to pe2
				  double A2 = pe1.y-ps1.y;
				  double B2 = ps1.x-pe1.x;
				  double C2 = A2*ps1.x+B2*ps1.y;
				 
				  // Get delta and check if the lines are parallel
				  double delta = A1*B2 - A2*B1;
				  if(delta == 0)
				     return new Point2D.Double(Double.MIN_VALUE, Double.MIN_VALUE);
				 
				  // Intersection
				  return new Point2D.Double((B2*C1 - B1*C2)/delta, (A1*C2 - A2*C1)/delta);
				}
		
		private Point2D.Double checkCollision(double x, double y, double vx, double vy, double deltaTime){
			Point2D.Double p = new Point2D.Double(0,0);
			
			Point2D.Double paddleStart = new Point2D.Double(position.x, position.y - HEIGHT*0.7d - (velocity.y));
			Point2D.Double paddleEnd = new Point2D.Double(position.x, position.y + HEIGHT*0.7d + ( velocity.y));
			
			Point2D.Double ballStart = new Point2D.Double(x, y);
			Point2D.Double ballEnd = new Point2D.Double(x + vx*deltaTime, y + vy*deltaTime);
			
			Point2D.Double intersection = LineIntersection(paddleStart, paddleEnd, ballStart, ballEnd);
			
			double ballD = ballStart.distance(ballEnd);
			double paddleD = paddleStart.distance(paddleEnd);			
			
			if(ballStart.distance(intersection) <= ballD && ballEnd.distance(intersection) <= ballD
					&& paddleStart.distance(intersection) <= paddleD && paddleEnd.distance(intersection) <= paddleD){
				p.setLocation(vx > 0 ? -1 : 1, vy*1.5f + velocity.y*0.2d );
			}
			return p;
		}
		
		private void handlePlay() {
			ACLMessage msg = myAgent.receive(MessageTemplate.or(updateTemplate, MessageTemplate.or(collisionTemplate, scoreTemplate)));
			if(msg != null){
				if(updateTemplate.match(msg)){
					if(lastFrame == -1)
						lastFrame = System.nanoTime();

					double deltaTime = (System.nanoTime() - lastFrame)*0.0000001d;

					move(deltaTime);
					
					ACLMessage reply = msg.createReply();
					reply.setContent("PADDLE "+position.getX()+" "+position.getY()+" "+velocity.getX()+" "+velocity.getY());
					myAgent.send(reply);

					lastFrame = System.nanoTime();

				}else if(collisionTemplate.match(msg)){
					String[] content = msg.getContent().split(" ");
					String type = content[0];
					double x = Double.parseDouble(content[1]);
					double y = Double.parseDouble(content[2]);
					double vx = Double.parseDouble(content[3]);
					double vy = Double.parseDouble(content[4]);
					double deltaTime = Double.parseDouble(content[5]);

					ACLMessage reply = msg.createReply();
					Point2D.Double collision = checkCollision(x, y, vx, vy, deltaTime);
					movementLogic(x, y);
					reply.setContent(collision.x+" "+collision.y);
					myAgent.send(reply);
				}else{
					// SCORE
					String[] content = msg.getContent().split(" ");
					int x = Integer.parseInt(content[0]);
					int y = Integer.parseInt(content[1]);
					int vx = Integer.parseInt(content[2]);
					int vy = Integer.parseInt(content[3]);
					position.setLocation(x, y);
					velocity.setLocation(vx,vy);
					
				}
			}else{
				block();
			}
		}


		@Override
		public void action() {
			switch (state) {
			case SETUP:
				handleSetup();
				break;
			case PLAY:
				handlePlay();
				break;
			}
		}
				
	}
}
