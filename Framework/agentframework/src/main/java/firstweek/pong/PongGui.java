package firstweek.pong;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class PongGui extends JFrame {

	private Board myAgent;
    private Timer timer = null;
	
    private boolean rendering = false;
    
    PongGui(int width, int height, Board agent){
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				myAgent.doDelete();
			}
		} );

		setAgent(agent);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		JPanel rootPanel = new JPanel();
		rootPanel.setLayout(new GridBagLayout());
        JPanel panel=new JPanel();
        getContentPane().add(panel);
        setSize(width, height);
        this.pack();
        Insets insets = this.getInsets();
        this.setBounds(0,0, insets.left + width + insets.right, insets.top + height + insets.bottom);
        setVisible(true);
        setIgnoreRepaint(true);
	}
	

    public void paint(Graphics g) {
    	//super.paint(g);

    	if(rendering)
    		return;
    	rendering = true;
    	
    	BufferedImage b = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
    	
    	
        Graphics2D g2 = (Graphics2D) b.getGraphics();
        Insets insets = this.getInsets();
        g2.translate(insets.left, insets.top);
        g2.drawRect(0, 0, 640, 480);
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getWidth(), getHeight());
        
        
    	g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    	g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    	g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
    	g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    	g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    	g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    	g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);    	


        g2.setColor(Color.BLACK);
        Vector<RenderObject> objects = myAgent.getRenderObjects();
        Iterator<RenderObject> it = objects.iterator();
        while(it.hasNext()){
        	RenderObject obj = it.next();
        	if(obj.getType().equals("BALL")){
        		int radius = 2;
        		g2.fillOval((int)obj.getPosition().getX() - radius, (int)obj.getPosition().getY() - radius, radius * 2, radius * 2);
        	}else{
        		g2.fillRect((int)obj.getPosition().getX() - 2, (int)obj.getPosition().getY() - 20, 4, 40);
        	}
        }
        
        
        int[] score = myAgent.getScore();
        
        g2.setFont(new Font("Arial", Font.PLAIN, 20));
        FontMetrics fm = g2.getFontMetrics();
        g2.drawString("Score", getWidth()/2 - fm.stringWidth("Score")/2, fm.getHeight());
        g2.drawString(score[0]+" : "+score[1], getWidth()/2 - fm.stringWidth(score[0]+" : "+score[1])/2, fm.getHeight()*2);

        g2.setColor(Color.BLACK);
        g2.drawLine(getWidth()/2, fm.getHeight()*4, getWidth()/2, (int)(getHeight()*0.9));
        g2.drawRect(0, 0, getWidth() - insets.left - insets.right, getHeight() - insets.top - insets.bottom);
        
        g.drawImage(b, 0, 0, null);
        rendering = false;
    }
	
	void setAgent(Board a){
		this.myAgent = a;
	}

	void notifyUser(String message){
		
	}
}
