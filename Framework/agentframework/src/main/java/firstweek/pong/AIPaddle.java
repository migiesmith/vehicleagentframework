package firstweek.pong;

import java.awt.geom.Point2D;
import java.util.Random;

public class AIPaddle extends Paddle {

	Random r = new Random();
	
	@Override
	public void movementLogic(double x, double y) {
		velocity.setLocation(velocity.x, Math.abs(y - position.y) < 10 ? 0 : (y > position.y ? 3.5d + r.nextDouble() : -3.5d));

		if((velocity.y < 0 && position.y - HEIGHT*0.5d <= 0) || (velocity.y > 0 && position.y + HEIGHT*0.5d >= 480))
			velocity.y = 0;
		
		if(position.distance(new Point2D.Double(x, y)) < HEIGHT*0.5d)
			velocity.setLocation(velocity.getX(), 2.0d);
		
	}

}
