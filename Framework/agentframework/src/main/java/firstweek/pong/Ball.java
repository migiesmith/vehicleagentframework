package firstweek.pong;


import java.awt.geom.Point2D;

import firstweek.pong.Board.STATE;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class Ball extends Agent {

	private Point2D.Double position = new Point2D.Double(0,0);
	private Point2D.Double velocity = new Point2D.Double(0,0);

	private AID board;
	
	private AID[] paddles;
	
	private long lastFrame = -1;
	
	@Override
	protected void setup() {
		System.out.println("Started Ball.");

		board = new AID("board", AID.ISLOCALNAME);
		paddles = new AID[]{ new AID("paddle0", AID.ISLOCALNAME), new AID("paddle1", AID.ISLOCALNAME)};
		
		addBehaviour(new BallUpdater());
	}

	private class BallUpdater extends CyclicBehaviour {

		private STATE state = STATE.SETUP;

		private MessageTemplate setupTemplate, updateTemplate, scoreTemplate, collisionTemplate;
		
		
		BallUpdater(){
			setupTemplate = MessageTemplate.MatchConversationId("Game-Setup");
			updateTemplate = MessageTemplate.MatchConversationId("Game-Update");
			scoreTemplate = MessageTemplate.MatchConversationId("Game-Score");
			collisionTemplate = MessageTemplate.MatchConversationId("Collision-Check");
		}
		
		private void handleSetup() {
			ACLMessage msg = myAgent.receive(setupTemplate);
			if(msg != null){
				String[] content = msg.getContent().split(" ");
				int x = Integer.parseInt(content[0]);
				int y = Integer.parseInt(content[1]);
				int vx = Integer.parseInt(content[2]);
				int vy = Integer.parseInt(content[3]);
				position.setLocation(x, y);
				velocity.setLocation(vx,vy);
				
				state = STATE.PLAY;
			}else{
				block();
			}
		}
		
		private void move(double deltaTime){
			position.x += velocity.x * deltaTime;
			position.y += velocity.y * deltaTime;
		}
		
		private void collisionResponse(ACLMessage msg){
			String[] content = msg.getContent().split(" ");
			double x = Double.parseDouble(content[0]);
			double y = Double.parseDouble(content[1]);

			
			if((y < 0 && velocity.y > 0) || (y > 0 && velocity.y < 0)){
				velocity.y *= -1;
			}
			if((x < 0 && velocity.x > 0) || (x > 0 && velocity.x < 0)){
				velocity.x *= -1;
			}
			
			if(msg.getSender().getName().equals(paddles[0].getName()) || msg.getSender().getName().equals(paddles[1].getName())){
				velocity.x = velocity.getX()*1.3d;
				velocity.y = velocity.getY()*1.3d + y;
			}
			
			velocity.x = (velocity.getX() >= 0 ? 1 : -1) * Math.min(Math.abs(velocity.getX()), 3.5);
			velocity.y = (velocity.getY() >= 0 ? 1 : -1) * Math.min(Math.abs(velocity.getY()), 3.5);
			if(Math.abs(velocity.x) > 2)
				velocity.x *= 0.8;
			if(Math.abs(velocity.x) > 2)
				velocity.y *= 0.8;
			
			System.out.println(velocity);
		}

		private void handlePlay() {
			ACLMessage msg = myAgent.receive(MessageTemplate.or(updateTemplate, MessageTemplate.or(collisionTemplate, scoreTemplate)));
			if(msg != null){
				if(updateTemplate.match(msg)){
					if(lastFrame == -1)
						lastFrame = System.nanoTime();

					double deltaTime = (System.nanoTime() - lastFrame)*0.0000001d;
					
					move(deltaTime);
					
					ACLMessage reply = msg.createReply();
					reply.setContent("BALL "+position.getX()+" "+position.getY()+" "+velocity.getX()+" "+velocity.getY() + " " + deltaTime);
					myAgent.send(reply);
					
					ACLMessage collisionMsg = new ACLMessage(ACLMessage.REQUEST);
					collisionMsg.addReceiver(board);
					collisionMsg.addReceiver(paddles[0]);
					collisionMsg.addReceiver(paddles[1]);
					collisionMsg.setConversationId("Collision-Check");
					collisionMsg.setContent(reply.getContent());
					myAgent.send(collisionMsg);

					
					lastFrame = System.nanoTime();
										
				}else if(collisionTemplate.match(msg)){
						collisionResponse(msg);

				}else{
					// SCORE
					String[] content = msg.getContent().split(" ");
					int x = Integer.parseInt(content[0]);
					int y = Integer.parseInt(content[1]);
					int vx = Integer.parseInt(content[2]);
					int vy = Integer.parseInt(content[3]);
					position.setLocation(x, y);
					velocity.setLocation(vx,vy);
				}
				
				
			}else{
				block();
			}
		}

		@Override
		public void action() {
			switch (state) {
			case SETUP:
				handleSetup();
				break;
			case PLAY:
				handlePlay();
				break;
			}
		}
		
	}
	
}
