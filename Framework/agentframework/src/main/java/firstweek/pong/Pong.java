package firstweek.pong;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public class Pong {

	public static void main(String[] args) {
		ContainerController myContainer;
		
		Runtime myRuntime = Runtime.instance();

		// prepare the settings for the platform that we're going to start
		Profile myProfile = new ProfileImpl();

		// create the main container
		myContainer = myRuntime.createMainContainer(myProfile);
		
		try {
		    AgentController rma = myContainer.createNewAgent("rma", "jade.tools.rma.rma", null);
		    rma.start();
		    AgentController board = myContainer.createNewAgent("board", Board.class.getCanonicalName(), null);
		    board.start();
		} catch(StaleProxyException e) {
		    e.printStackTrace();
		}
	}

}
