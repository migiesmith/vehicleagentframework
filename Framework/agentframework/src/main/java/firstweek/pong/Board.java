package firstweek.pong;

import java.awt.geom.Point2D;
import java.util.Vector;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.ShutdownPlatform;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.StaleProxyException;

public class Board extends Agent{

	protected int[] score = {0,0};
	private final int _WIDTH = 640, _HEIGHT = 480;

	private AID ball, paddle0, paddle1;
	
	private PongGui myGui;
	
	
	private Vector<RenderObject> renderObjects = new Vector<RenderObject>();
	
	enum STATE{
		SETUP,
		PLAY,
		SCORE
	}
	
	@Override
	protected void setup() {
		System.out.println("Started Board.");
		
		
		try {
			ball = new AID("ball", AID.ISLOCALNAME);
			getContainerController().createNewAgent("ball", Ball.class.getCanonicalName(), null).start();
			System.out.println("Ball made.");
			paddle0 = new AID("paddle0", AID.ISLOCALNAME);
			getContainerController().createNewAgent("paddle0", PlayerPaddle.class.getCanonicalName(), null).start();
			paddle1 = new AID("paddle1", AID.ISLOCALNAME);
			getContainerController().createNewAgent("paddle1", AIPaddle.class.getCanonicalName(), null).start();
			System.out.println("Paddles made.");
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
		
		addBehaviour(new BoardUpdater(this));
		
		myGui = new PongGui(_WIDTH, _HEIGHT, this);
		
	}
	
	
	public Vector<RenderObject> getRenderObjects(){
		return new Vector<RenderObject>(renderObjects);
	}
	
	public int[] getScore(){
		return score;
	}

	private class BoardUpdater extends TickerBehaviour {

		private STATE state = STATE.SETUP;

		private MessageTemplate updateTemplate, collisionTemplate;
		
		private int roundWinner = 0;
		
		BoardUpdater(Agent a){
			super(a, 20);
			updateTemplate = MessageTemplate.MatchConversationId("Game-Update");
			collisionTemplate = MessageTemplate.MatchConversationId("Collision-Check");		
		}
		
		private void handleSetup() {
			// Send setup to ball
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(ball);
			msg.setConversationId("Game-Setup");
			int x = _WIDTH/2;
			int y = _HEIGHT/2;
			int vx = -1;
			int vy = 0;
			msg.setContent(x+" "+y+" "+vx+" "+vy);
			myAgent.send(msg);

			// Send setup to paddles
			msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(paddle0);
			msg.setConversationId("Game-Setup");
			x = 20;
			y = _HEIGHT/2;
			int w = 4;
			int h = 40;
			msg.setContent(x+" "+y+" "+w+" "+h);
			myAgent.send(msg);

			msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(paddle1);
			msg.setConversationId("Game-Setup");
			x = _WIDTH-20;
			y = _HEIGHT/2;
			w = 4;
			h = 40;
			msg.setContent(x+" "+y+" "+w+" "+h);
			myAgent.send(msg);

			for(int i = 0; i < 3; i++)
				renderObjects.addElement(new RenderObject("", new Point2D.Double(0,0), new Point2D.Double(0,0)));
			
			state = STATE.PLAY;
		}

		private void handlePlay() {

			ACLMessage inMsg = myAgent.receive(updateTemplate);
			while(inMsg != null){
				String[] content = inMsg.getContent().split(" ");
				String type = content[0];
				int x = (int)Double.parseDouble(content[1]);
				int y = (int)Double.parseDouble(content[2]);
				int vx = (int)Double.parseDouble(content[3]);
				int vy = (int)Double.parseDouble(content[4]);
				
				renderObjects.addElement(new RenderObject(type, new Point2D.Double(x, y), new Point2D.Double(vx, vy)));
				
				if(type == "BALL")
					System.out.println(type+":"+x+","+y+","+vx+","+vy);
				
				inMsg = myAgent.receive(updateTemplate);
			}
			if(renderObjects.size() >= 3){
				myGui.paint(myGui.getGraphics());
				ACLMessage outMsg = new ACLMessage(ACLMessage.REQUEST);
				outMsg.addReceiver(ball);
				outMsg.addReceiver(paddle0);
				outMsg.addReceiver(paddle1);
				outMsg.setConversationId("Game-Update");
				myAgent.send(outMsg);
				renderObjects.clear();
			}

			ACLMessage collisionMsg = myAgent.receive(collisionTemplate);
			while(collisionMsg != null){
				String[] content = collisionMsg.getContent().split(" ");
				String type = content[0];
				double x = Double.parseDouble(content[1]);
				double y = Double.parseDouble(content[2]);
				double vx = Double.parseDouble(content[3]);
				double vy = Double.parseDouble(content[4]);
				double deltaTime = Double.parseDouble(content[5]);

				ACLMessage reply = collisionMsg.createReply();
				Point2D.Double collision = checkCollision(x, y, vx, vy, deltaTime);
				reply.setContent(collision.x+" "+collision.y);
				myAgent.send(reply);
				
				collisionMsg = myAgent.receive(collisionTemplate);
				if(state == STATE.SCORE){
					while(collisionMsg != null){
						collisionMsg = myAgent.receive(collisionTemplate);
					}
				}
			}
		}
		
		private Point2D.Double checkCollision(double x, double y, double vx, double vy, double deltaTime){
			Point2D.Double p = new Point2D.Double(0,0);
			// Y collision
			if(y + vy*deltaTime >= _HEIGHT && vy > 0){
				p.y = -1;
			}else if(y + vy*deltaTime <= 0 && vy < 0){
				p.y = 1;
			}
			// X collision
			if(x + vx*deltaTime >= _WIDTH && vx > 0){
				p.x = -1;
				roundWinner = 0;
				state = STATE.SCORE;
			}else if(x + vx*deltaTime <= 0 && vx < 0){
				p.x = 1;
				roundWinner = 1;
				state = STATE.SCORE;
			}
			return p;
		}
		
		private void handleScore() {
			score[roundWinner]++;
			
			// Send score to ball
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(ball);
			msg.setConversationId("Game-Score");
			int x = _WIDTH/2;
			int y = _HEIGHT/2;
			int vx = score[0] > score[1] ? 1 : -1;
			int vy = 0;
			msg.setContent(x+" "+y+" "+vx+" "+vy);
			myAgent.send(msg);

			// Send score to paddles
			msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(paddle0);
			msg.setConversationId("Game-Score");
			x = 20;
			y = _HEIGHT/2;
			vx = 0;
			vy = 0;
			msg.setContent(x+" "+y+" "+vx+" "+vy);
			myAgent.send(msg);

			msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(paddle1);
			msg.setConversationId("Game-Score");
			x = _WIDTH-20;
			y = _HEIGHT/2;
			vx = 0;
			vy = 0;
			msg.setContent(x+" "+y+" "+vx+" "+vy);
			myAgent.send(msg);

			state = STATE.PLAY;			
		}

		@Override
		protected void onTick() {
			switch (state) {
			case SETUP:
				handleSetup();
				break;
			case PLAY:
				handlePlay();
				break;
			case SCORE:
				handleScore();
				break;
			}			
		}
	}
	
}
