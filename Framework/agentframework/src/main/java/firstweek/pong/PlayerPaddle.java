package firstweek.pong;

import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;

public class PlayerPaddle extends Paddle{

	private boolean KEY_UP = false;
	private boolean KEY_DOWN = false;
	
	@Override
	protected void setup(){
		super.setup();
		AWTEventListener listener = new AWTEventListener() {
			  public void eventDispatched(AWTEvent event) {
			    try {
			      KeyEvent evt = (KeyEvent)event;
			      if(evt.getID() == KeyEvent.KEY_PRESSED) {
			    	  switch(evt.getKeyCode()){
			    	  case KeyEvent.VK_UP:
			    		  KEY_UP = true;
			    		  break;
			    	  case KeyEvent.VK_W:
			    		  KEY_UP = true;
			    		  break;
			    	  case KeyEvent.VK_DOWN:
			    		  KEY_DOWN = true;
			    		  break;
			    	  case KeyEvent.VK_S:
			    		  KEY_DOWN = true;
			    		  break;
			    	  }
			      }else if(evt.getID() == KeyEvent.KEY_RELEASED) {
			    	  switch(evt.getKeyCode()){
			    	  case KeyEvent.VK_UP:
			    		  KEY_UP = false;
			    		  break;
			    	  case KeyEvent.VK_W:
			    		  KEY_UP = false;
			    		  break;
			    	  case KeyEvent.VK_DOWN:
			    		  KEY_DOWN = false;
			    		  break;
			    	  case KeyEvent.VK_S:
			    		  KEY_DOWN = false;
			    		  break;
			    	  }
			      }
			    }
			    catch(Exception e) {
			      e.printStackTrace();
			    }
			  }
			};

			Toolkit.getDefaultToolkit().addAWTEventListener(listener, AWTEvent.KEY_EVENT_MASK);
	}
	
	@Override
	public void movementLogic(double x, double y) {
		velocity.y = 0;
		if(KEY_UP){
			velocity.setLocation(velocity.getX(), -4.0d);
		}else if(KEY_DOWN){
			velocity.setLocation(velocity.getX(), 4.0d);
		}
		
		if((velocity.y < 0 && position.y - HEIGHT*0.5d <= 0) || (velocity.y > 0 && position.y + HEIGHT*0.5d >= 480))
			velocity.y = 0;
	}

}
