package firstweek.pong;

import java.awt.geom.Point2D;

public class RenderObject {

	private String type;
	private Point2D.Double position;
	private Point2D.Double velocity;
	
	RenderObject(String type, Point2D.Double position, Point2D.Double velocity){
		setType(type);
		setPosition(position);
		setVelocity(velocity);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Point2D.Double getPosition() {
		return position;
	}

	public void setPosition(Point2D.Double position) {
		this.position = position;
	}

	public Point2D.Double getVelocity() {
		return velocity;
	}

	public void setVelocity(Point2D.Double velocity) {
		this.velocity = velocity;
	}
}
