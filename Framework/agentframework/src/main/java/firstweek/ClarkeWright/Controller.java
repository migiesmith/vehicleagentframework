package firstweek.ClarkeWright;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import firstweek.clarkeWright2.CWGui;
import firstweek.clarkeWright2.HasRoutes;
import jade.core.AID;
import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.proto.SubscriptionInitiator;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public class Controller extends Agent implements HasRoutes{

	// States for the NodeDistributionServer
	private enum STATE{
		SENDING,
		WAITING,
		AWARDING,
		COMPLETE,
	}
	// The state that the NodeDistributionServer is currently in
	private STATE currentState = STATE.SENDING;
	
	// The routing agents found on the yellow-pages
	private Vector<AID> routingAgents;
	// The nodes to distribute amongst the routing agents
	protected Vector<Node> nodes;
	// Stores for the problem loaded from a file
	private VRProblem prob;

	private CWGui myGui;
	Vector<Vector<Node>> routes = new Vector<Vector<Node>>();
	
	
	private int NODE_COUNT = 0;
	private boolean awaitingAgentUpdate = true;
	
	protected void setup(){
		// Create empty vectors
		routingAgents = new Vector<AID>();
		nodes = new Vector<Node>();

		// Load the problem
		try {
			//Create a file chooser
			final JFileChooser fc = new JFileChooser("Test Data\\");
			fc.setFileFilter(new FileFilter(){

				@Override
				public boolean accept(File f) {
					if(f.getName().toUpperCase().endsWith(".CSV"))
						return true;
					
					return false;
				}

				@Override
				public String getDescription() {
					return null;
				}
				
			});

			//In response to a button click:
			int returnVal = fc.showOpenDialog(null);
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				System.out.println("Opening: " + file.getAbsolutePath() + ".");
				prob = new VRProblem(file.getAbsolutePath());
			} else {
				System.out.println("No file chosen, closing..");
				System.exit(0);
			}
			
			nodes = prob.customers;
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Add the behaviours
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Routing-Agent");
		dfd.addServices(sd);
		
		addBehaviour(new RoutingAgentSubscription(this, dfd)); // Keeps the routingAgents Vector up-to-date
		addBehaviour(new NodeDistributionServer()); // Distributes the nodes amongst the routing agents
	
		
		// Add the first agent
		addAgent();

		myGui = new CWGui("Agents bid for nodes (Agents created as needed)");
		myGui.setAgent(this);
		
	}
	
	// Dynamically adds agents as required
	private void addAgent(){
		awaitingAgentUpdate = true;
		try {
		System.out.println("Creating new agent..");
			getContainerController().createNewAgent("Routing-Node"+NODE_COUNT, TestRoutingAgent.class.getCanonicalName(), new Object[] { prob.depot })
					.start();
			NODE_COUNT++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Vector<Vector<Node>> getRoutes(){
		return new Vector<Vector<Node>>(routes);
	}
	public Node getDepot(){
		return prob.depot;
	}
	
	// Distributes nodes amongst the registered routing agents
	private class NodeDistributionServer extends CyclicBehaviour {

		// Template for proposals during the waiting state
		private MessageTemplate waitingTemplate;
		
		// Index of the node to be sent to an agent
		private int index = 0;
		// The number of replies received for the current CFP
		private int replies = 0;
		
		// The nodes that have been sent
		private Vector<Node> sent;
		
		// The best saving and the corresponding agent for the current node to be sent
		private double bestSaving = Double.MIN_VALUE;
		private AID bestAgent;
		
		
		NodeDistributionServer(){
			// Setup the waitingTemplate and the sent Vector
			waitingTemplate = MessageTemplate.MatchConversationId("node-bidding");
			sent = new Vector<Node>();
		}
		
		private void sendCFPToAgentsForBid(){
			// Send out a CFP to each routingAgent
			if(index >= nodes.size() || sent.size() == nodes.size()){
				currentState = STATE.COMPLETE;
				return;
			}
			if(awaitingAgentUpdate){
				return;
			}
			System.out.println("Sending to agents.");
			
			// Get a node
			Node node = nodes.get(index);					
			// Create and send a CFP for the node
			ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
			for(AID id : routingAgents){
				cfp.addReceiver(id);
			}
			cfp.setContent(node.x+","+node.y+","+node.cost);
			cfp.setConversationId("node-bidding");
			cfp.setReplyWith("cfp" + System.currentTimeMillis());
			myAgent.send(cfp);
			
			// Change to the waiting state
			currentState = STATE.WAITING;
		}
		
		private void handleCFPResponses(){
			// Accept Responses to the CFP from the routingAgents
			ACLMessage response = myAgent.receive(waitingTemplate);
			if (response != null) {
				// Only process Proposes
				if(response.getPerformative() == ACLMessage.PROPOSE){
					double saving = Double.parseDouble(response.getContent());
					if(bestAgent == null || saving > bestSaving){
						bestSaving = saving;
						bestAgent = response.getSender();
					}
				}
				// Increment the reply counter until we have them all
				replies++;
				if(replies >= routingAgents.size()){
					currentState = STATE.AWARDING;
				}
				
			}else{
				block();
			}
		}
		
		private void handleWinningBid(){
			// If we have a best agent then send them the node
			if(bestAgent != null){
				// Create a proposal acceptance message and send it
				ACLMessage msg = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
				msg.addReceiver(bestAgent);
				try {
					msg.setContentObject((Serializable) nodes.get(index));
				} catch (IOException e) {
					e.printStackTrace();
				}
				msg.setConversationId("node-bid-won");
				msg.setReplyWith("node-won"+System.currentTimeMillis());
				myAgent.send(msg);
				
				sent.addElement(nodes.get(index));
				index++;
			}else{
				/* All of the agents would go over capacity, we need another agent.
				 * Inform every agent to clear their nodes Vector
				*/
				ACLMessage clearRequest = new ACLMessage(ACLMessage.REQUEST);
				for(AID id : routingAgents){
					clearRequest.addReceiver(id);
				}
				clearRequest.setConversationId("node-clear-all");
				myAgent.send(clearRequest);
				
				// Reset values so that it is as if no nodes have been sent
				index = 0;
				sent.clear();
				/* Clear this Vector so that the system waits until RoutingAgentFinder 
				 * has updated the routingAgents Vector
				 */						
				//routingAgents.clear();
				// Create another agent
				addAgent();

			}

			// Reset values for the next node to send
			bestSaving = Double.MIN_VALUE;
			bestAgent = null;
			replies = 0;
			currentState = STATE.SENDING;	
		}
		
		@Override
		public void action() {
			// Switch on the currentState
			switch(currentState){
				case SENDING:{
					// Send out a CFP to each routingAgent
					sendCFPToAgentsForBid();
					break;
					}
				case WAITING:{
					// Accept Responses to the CFP from the routingAgents
					handleCFPResponses();						
					break;
					}
				case AWARDING:{
					// Send the node to the best agent or create a new one if all refused
					handleWinningBid();
					break;
					}
				case COMPLETE:{
					System.out.println("Complete.");
					// Remove this behaviour as it has finished
					myAgent.removeBehaviour(this);
					// Add a behaviour to accumulate the nodes held by each routing agent
					addBehaviour(new ResultAccumulatingServer());
					break;
					}
			}
		}
				
	}

	private class ResultAccumulatingServer extends CyclicBehaviour {

		private int replies = 0;
		private int step = 0;
		

		private MessageTemplate waitingTemplate;
		
		
		ResultAccumulatingServer(){
			waitingTemplate = MessageTemplate.MatchConversationId("route-accum");
		}
		
		@Override
		public void action() {
			switch (step) {
				case 0: {
					// Request each user for their node Vector
					ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
					for(AID id : routingAgents){
						request.addReceiver(id);
					}
					request.setConversationId("route-accum");
					request.setReplyWith("request" + System.currentTimeMillis());
					myAgent.send(request);
					
					// Change to the next step
					step = 1;
					break;
				}
				case 1: {
					// Accept all messages that match the template
					ACLMessage response = myAgent.receive(waitingTemplate);
					if (response != null) {
						// Add the node Vector from the agent to the Vector of node Vectors
						try {
							routes.addElement((Vector<Node>)response.getContentObject());
						} catch (UnreadableException e) {
							e.printStackTrace();
						}
						// Increment the replies counter until we have then all
						replies++;
						if(replies >= routingAgents.size()){
							step = 2; // Move to the next step
						}
					}else{
						block();
					}
					break;
				}
				case 2: {					
					// Print out the accumulated results in SVG format
					try {
						writeSVG(routes, "results/prob.svg", "results/solu.svg");
					} catch (Exception e) {
						e.printStackTrace();
					}
	
					double cost = 0;
					for (Vector<Node> route : routes) {
						Node prev = prob.depot;
						for (Node n : route) {
							cost += prev.distance(n);
							prev = n;
						}
						// Add the cost of returning to the depot
						cost += prev.distance(prob.depot);
					}
					System.out.println("This Solution Cost " + cost + ".");
					System.out.println(CWVerifier.verify(getRoutes(), prob.depot, nodes) ? "Valid solution" : "Invalid solution");
					
					doDelete();
					break;
				}
			}
		}
	}
	
	// Finds all RoutingAgents on the yellow-pages (DF)
	private class RoutingAgentSubscription extends SubscriptionInitiator{

		/**
		 * @param a The Agent to link to
		 * @param period The time interval, in milliseconds, between each tick
		 */
		public RoutingAgentSubscription(Agent a, DFAgentDescription dfd) {
			super(a, DFService.createSubscriptionMessage(a,getDefaultDF(),dfd,null));
		}
		

		protected void handleInform(ACLMessage inform) {
			//System.out.println("Agent " + getLocalName() + ": Notification received from DF");
			try {
				DFAgentDescription[] results = DFService.decodeNotification(inform.getContent());
				// Clear the Vector of routing agents and refill it with the results
    			if(results.length > 0){
	    			for(int i = 0; i < results.length; i++){
	    				if(results[i].getAllServices().hasNext()){
	    					// Add the new agent to the list
	    					routingAgents.addElement(results[i].getName());
	    				}else{
	    					// Remove the agent from the list
		    				routingAgents.removeElement(results[i].getName());
	    				}
	    			}
    			}
    			awaitingAgentUpdate = false;
			} catch (FIPAException fe) {
				fe.printStackTrace();
			}

		}
		
	}
	
    // Output the routes in SVG format to show a visual representation of the results
    public void writeSVG(Vector<Vector<Node>> routes, String probFilename,String solnFilename) throws Exception{
		String[] colors = "chocolate cornflowerblue crimson cyan darkblue darkcyan darkgoldenrod".split(" ");
		int colIndex = 0;
		String hdr = 
				"<?xml version='1.0'?>\n"+
						"<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' '../../svg11-flat.dtd'>\n"+
						"<svg width='8cm' height='8cm' viewBox='0 0 500 500' xmlns='http://www.w3.org/2000/svg' version='1.1'>\n";
		String ftr = "</svg>";
		StringBuffer psb = new StringBuffer();
		StringBuffer ssb = new StringBuffer();
		psb.append(hdr);
		ssb.append(hdr);
		for(Vector<Node> route : routes){
			ssb.append(String.format("<path d='M%s %s ",prob.depot.x,prob.depot.y));
			for(Node c : route)
				ssb.append(String.format("L%s %s",c.x,c.y));
			ssb.append(String.format("z' stroke='%s' fill='none' stroke-width='2'/>\n",
					colors[colIndex++ % colors.length]));
		}
		
		for(Node c:nodes){
			String disk = String.format(
					"<g transform='translate(%.0f,%.0f)'>"+
							"<circle cx='0' cy='0' r='%d' fill='pink' stroke='black' stroke-width='1'/>" +
							"<text text-anchor='middle' y='5'>%.0f</text>"+
							"</g>\n", 
							c.x,c.y,10,c.cost);
			psb.append(disk);
			ssb.append(disk);
		}
		String disk = String.format("<g transform='translate(%.0f,%.0f)'>"+
				"<circle cx='0' cy='0' r='%d' fill='pink' stroke='black' stroke-width='1'/>" +
				"<text text-anchor='middle' y='5'>%s</text>"+
				"</g>\n", prob.depot.x, prob.depot.y,20,"D");
		psb.append(disk);
		ssb.append(disk);
		psb.append(ftr);
		ssb.append(ftr);
		PrintStream ppw = new PrintStream(new FileOutputStream(probFilename));
		PrintStream spw = new PrintStream(new FileOutputStream(solnFilename));
		ppw.append(psb);
		spw.append(ssb);
		ppw.close();
		spw.close();
	}
    
}
