package firstweek.ClarkeWright;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public abstract class NodeOrderingImprover {

	// Improves the ordering of the nodes
	public static void improve(Vector<Node> nodes, Node depot){		
		// Create a list of savings
		List<SavingsNode> savings = new ArrayList<SavingsNode>();
		// Loop through each node and add the relative savings to the savings list if there is a saving to be made
		for(int i = 0; i < nodes.size(); i++){
			for(int j = 0; j < nodes.size(); j++){
				if(i == j) continue;
				Node ci = nodes.get(i);
				Node cj = nodes.get(j);
				double saving = depot.distance(ci) + depot.distance(cj) - ci.distance(cj);
				if(saving > 0)
					savings.add(new SavingsNode(ci,cj,saving));
			}
		}
		
		// Sort the list into descending order
		Collections.sort(savings);

		// Create an ordered list
		List<Node> ordered = new LinkedList<Node>();
		// Add the highest saving nodes and remove them from the savings list
		ordered.add(savings.get(0).ci);
		ordered.add(savings.remove(0).cj);

		// Loop through the savings list
		for(SavingsNode s : savings){
			// if the first ordered node is ci and ordered doesn't contain cj then add cj
			if(ordered.get(0) == s.ci && !ordered.contains(s.cj)){
				ordered.add(0,s.cj);
			// else if the lat ordered node is ci and ordered doesn't contain cj then add cj
			}else if(ordered.get(ordered.size()-1) == s.ci && !ordered.contains(s.cj)){
				ordered.add(s.cj);
			// else add cj to the side that has the largest saving
			}else{
				Node first = ordered.get(0);
				Node last = ordered.get(ordered.size()-1);
				
				double savingF = (depot.distance(first) + depot.distance(s.cj)) - first.distance(s.cj);
				double savingL = (depot.distance(last) + depot.distance(s.cj)) - last.distance(s.cj);
				
				if(!ordered.contains(s.cj)){
					if(savingF >= savingL)
						ordered.add(0,s.cj);
					else
						ordered.add(s.cj);
				}
			}
		}
		// clear this list
		nodes.clear();
		// add all of the ordered nodes to this list
		nodes.addAll(ordered);
	}
	
}
