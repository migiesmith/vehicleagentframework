package firstweek.ClarkeWright;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import firstweek.RoutingAgent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class TestRoutingAgent extends RoutingAgent {


	private Node depot;
	
	private int addAt = 0;
	
	@Override
	protected void setup() {

		if(getArguments().length == 0){
			doDelete();
			return;
		}
		
		MAXIMUM_CAPACITY = 50.0d;
		
		// Setup the nodes Vector and the depot
		nodes = new Vector<Node>();
		depot = (Node) getArguments()[0];
		
		// Add the bidding behaviour
		addBehaviour(new NodeBidder());
		
		// Register the RoutingAgent in the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Routing-Agent");
		sd.setName(getLocalName() + "-Routing-Agent");
		dfd.addServices(sd);
		try{
			DFService.register(this, dfd);
		}catch(FIPAException fe){
			fe.printStackTrace();
		}

		System.out.println("TestMyAgent "+getAID().getName()+" ready.");
	}
	
	protected void takeDown(){
		try {
			DFService.deregister(this);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
	}
	
	// Returns the capacity of the Agent
	protected double getCurrentCapacity(){
		double total = 0.0d;
		Iterator<Node> it = nodes.iterator();
		while(it.hasNext()){
			total += it.next().cost;
		}
		return total;
	}
	
	// Calculate the savings of adding Node n into this agent's list of nodes
	private double getSaving(Node n){
		if(nodes.size() == 0) // If this has no nodes, give it priority
			return Double.MAX_VALUE;
		// Calculated using Clarke Wright

		Node n0 = nodes.firstElement();
		double bestSaving = (depot.distance(n0) + depot.distance(n)) - n0.distance(n);
		addAt = 0;
		
		if(nodes.size() > 1){
			n0 = nodes.lastElement();
			double saving = (depot.distance(n0) + depot.distance(n)) - n0.distance(n);
			if (saving > bestSaving) {
				bestSaving = saving;
				addAt = nodes.size();
			}
			for(int i = 0; i < nodes.size(); i++){
				for(int j = 0; j < nodes.size(); j++){
					if(i == j) continue;
					Node ci = nodes.get(i);
					Node cj = nodes.get(j);
					saving = (ci.distance(cj) + cj.distance(ci)) - (ci.distance(n) + cj.distance(n))*0.5d;
					if (saving > bestSaving) {
						bestSaving = saving;
						addAt = i;
					}
				}
			}
		}
		// Return the highest saving value
		return bestSaving;
	}
	
	private class NodeBidder extends CyclicBehaviour{

		// The templates of the acceptable messages
		private MessageTemplate cfpTemplate,
								proposalTemplate,
								completionTemplate,
								clearTemplate;
		
		NodeBidder(){
			// Setup the templates
			cfpTemplate = MessageTemplate.and(
					MessageTemplate.MatchConversationId("node-bidding"),
					MessageTemplate.MatchPerformative(ACLMessage.CFP)
					);
			proposalTemplate = MessageTemplate.and(
					MessageTemplate.MatchConversationId("node-bid-won"),
					MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL)
					);
			completionTemplate = MessageTemplate.and(
					MessageTemplate.MatchConversationId("route-accum"),
					MessageTemplate.MatchPerformative(ACLMessage.REQUEST)
					);
			clearTemplate = MessageTemplate.and(MessageTemplate.MatchConversationId("node-clear-all"),
					MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
		}
		
		private void respondToCFP(ACLMessage msg){
			// Create a response to the CFP from the Controller
			ACLMessage reply = msg.createReply();
			
			// Read the values of the node to bid for
			String[] msgContent = msg.getContent().split(",");
			double x = Double.parseDouble(msgContent[0]);
			double y = Double.parseDouble(msgContent[1]);
			double cost = Double.parseDouble(msgContent[2]);

			// Calculate the highest saving that could be made by adding the node to this agent
			double nodeSaving = getSaving(new Node(x, y, cost));

			// If the capacity will be under the MAX then make a proposal
			if(getCurrentCapacity() + cost < MAXIMUM_CAPACITY){
				reply.setPerformative(ACLMessage.PROPOSE);
				reply.setContent(String.valueOf(nodeSaving));
				//System.out.println(getAID().getName()+"| Sent my Proposal.");
			}else{ // This will go over capacity, refuse the proposal
				reply.setPerformative(ACLMessage.REFUSE);
				//System.out.println(getAID().getName()+"| Sent my Refusal.");
			}
			// Send the reply
			myAgent.send(reply);
		}
		
		@Override
		public void action() {
			// Accept only 3 types of messages
			ACLMessage msg = myAgent.receive(MessageTemplate.or(
					MessageTemplate.or(cfpTemplate, proposalTemplate),
					MessageTemplate.or(completionTemplate, clearTemplate)					
					));
			if (msg != null) {
				if(cfpTemplate.match(msg)){
					respondToCFP(msg);

				}else if(proposalTemplate.match(msg)){
					// The Controller has accepted the proposal, read the node and store it in the list
					System.out.println(getAID().getName()+"| I got the Node!");
					try {
						Node n = (Node) msg.getContentObject();
						nodes.add(addAt, n);
					} catch (UnreadableException e) {
						e.printStackTrace();
					} 
					
				}else if(completionTemplate.match(msg)){
					NodeOrderingImprover.improve(nodes, depot);
					// The Controller has finished sending nodes, return this node's list
					ACLMessage reply = msg.createReply();
					try {
						reply.setContentObject(nodes);
					} catch (IOException e) {
						e.printStackTrace();
					}
					myAgent.send(reply);
					System.out.println(getAID().getLocalName() + " has stopped.");
					doDelete();
					
				}else if(clearTemplate.match(msg)){
					nodes.clear();
					addAt = 0;
					
				}
			}else{
				block();
			}
		}
		
		
	}

}
