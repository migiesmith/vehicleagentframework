package firstweek.ClarkeWright;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class CWVerifier {

	public static Boolean verify(Vector<Vector<Node>> soln, Node depot, Vector<Node> nodes){
		//Check that no route exceeds capacity
		Boolean okSoFar = true;
		for(Vector<Node> route : soln){
			//Start the spare capacity at
			int total = 0;
			for(Node n:route)
				total += n.cost;
			if (total>depot.cost){
				System.out.printf("********FAIL Route starting %s is over capacity %d\n",
						route.get(0),
						total
						);
				okSoFar = false;
			}
		}
		//Check that we keep the customer satisfied
		//Check that every customer is visited and the correct amount is picked up
		Map<String,Double> reqd = new HashMap<String,Double>();
		for(Node n : nodes){
			String address = String.format("%fx%f", n.x,n.y);
			reqd.put(address, n.cost);
		}
		for(Vector<Node> route:soln){
			for(Node n:route){
				String address = String.format("%fx%f", n.x,n.y);
				if (reqd.containsKey(address))
					reqd.put(address, reqd.get(address)-n.cost);
				else
					System.out.printf("********FAIL no customer at %s\n",address);
			}
		}
		for(String address:reqd.keySet())
			if (reqd.get(address)!=0){
				System.out.printf("********FAIL Customer at %s has %f left over\n",address,reqd.get(address));
				okSoFar = false;
			}
		return okSoFar;
	}
	
}
