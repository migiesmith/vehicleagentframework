package firstweek.ClarkeWright;

import java.awt.geom.Point2D;
import java.io.Serializable;

public class Node extends Point2D.Double implements Serializable{

	// Cost to the capacity
	public double cost;
	
	// Constructor taking the location and cost of the node
	public Node(double x, double y, double cost){
		this.x = x;
		this.y = y;
		this.cost = cost;
	}

	public boolean equals(Node n){
		return (x == n.x && y == n.y && cost == n.cost);
	}
	
	// Converts the node to a printable String
	public String toString(){
		return "["+x+","+y+","+cost+"]";
	}
}
