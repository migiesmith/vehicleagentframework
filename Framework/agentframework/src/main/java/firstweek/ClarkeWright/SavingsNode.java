package firstweek.ClarkeWright;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.Serializable;

public class SavingsNode implements Comparable<SavingsNode>, Serializable {

	// Saving to be made
	public final double saving;
	// Customers that make the saving
	public final Node ci, cj;
	
	public SavingsNode(Node ci, Node cj, double saving){
		this.ci = ci;
		this.cj = cj;
		this.saving = saving;
	}

	// Comparator for sorting
	public int compareTo(SavingsNode sj) {
		return saving < sj.saving ? 1 : saving == sj.saving ? 0 : -1;
	}
	
	public String toString(){
		return "["+ci+","+cj+","+saving+"]";
	}
	
}
